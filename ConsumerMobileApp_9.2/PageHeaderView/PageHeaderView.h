//
//  PageHeaderView.h
//  Consumer Client
//
//  Created by android on 6/18/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PageHeaderViewButtonDelegate <NSObject>
/**
 * This method is  used for Custom delegate Method for PageHeader.
 */
// PageHeader Required methods Declaration.
@required
-(void)menuBtn_Action;
@end
/**
 * This method is  used for Custom delegate Method for PageHeader.
 */
@protocol PageHeaderViewRightButtonDelegate <NSObject>
// PageHeader Required methods Declaration.
@required
-(void)checkMarkBtn_Action;
-(void)editBtn_Action;
-(void)infoBtn_Action;
-(void)deleteBtn_Action;
@end

// Class Parent template1
/**
 * This class used to handle functionality and View of  PageHeader.
 *
 * @author Integra
 *
 */

@interface PageHeaderView : UIView{
    int viewTags;
}
@property (nonatomic, assign) NSObject <PageHeaderViewButtonDelegate> *delegate;
@property (nonatomic, assign) NSObject <PageHeaderViewRightButtonDelegate> *rightButtondelegate;

/**
 * declarations are used to set the UIConstraints Of PageHeader.
 *Label,Value label,Border label and Buttons.
 */
@property (nonatomic, strong) UIImageView *logoIcon;
@property (nonatomic, strong) UIImageView *leftMenu_Icon;
@property (nonatomic, strong) UILabel *header_titleLabel;

@property (nonatomic, strong) UIButton *leftmenuButton1;
@property (nonatomic, strong) UIButton *leftmenuButton2;
@property (nonatomic, strong) UIButton *rightmenu_Button1;
@property (nonatomic, strong) UIButton *rightmenu_Button2;
@property (nonatomic, strong) UIButton *rightmenu_Button3;

/*
 * This method is used to set PageHeader Method For initialization.
 */
// Method For initialization.

-(id)initWithFrame:(CGRect)frame withHeaderTitle:(NSString *)title withLeftbarBtn1Image_IconName:(NSString *)leftBtn1_Icon withLeftbarBtn2Image_IconName:(NSString *)leftBtn2_Icon  withHeaderButtonImage:(NSString *)headerBtn_Icon withRightbarBtn1Image_IconName:(NSString *)rightBtn1_Icon withRightbarBtn2Image_IconName:(NSString *)rightBtn2_Icon withRightbarBtn3Image_IconName:(NSString *)rightBtn3_Icon withTag:(int)viewTag;

@end