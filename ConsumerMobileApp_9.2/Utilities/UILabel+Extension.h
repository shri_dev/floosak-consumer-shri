//
//  UILabel+Extension.h
//  Consumer Client
//
//  Created by test on 11/02/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum _UILabelResizeResult{
    UILabelResizeFailed = -1,
    UILabelResizedNoChange = 0,
    UILabelResized = 1,
} UILabelResizeResult;


@interface UILabel (UILabelExtension)

//align text to top
- (UILabelResizeResult)alignToTop;

//enlarge height of this label to keep current text and font size
- (UILabelResizeResult)enlargeHeightToKeepFontSize;

//resize font size to keep current text and label size
- (UILabelResizeResult)resizeFontSizeToKeepCurrentRect:(CGFloat)initialFontSize;

@end