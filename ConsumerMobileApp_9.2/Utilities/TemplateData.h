//
//  TemplateData.h
//  Consumer Client
//
//  Created by android on 1/8/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TemplateData : NSObject

@property(nonatomic,assign) SEL selector;
@property(nonatomic,assign) id target;

+ (id)sharedManager;

@end
