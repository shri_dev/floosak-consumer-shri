//
//  TemplateData.m
//  Consumer Client
//
//  Created by android on 1/8/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import "TemplateData.h"

@implementation TemplateData

+ (id)sharedManager {
    static TemplateData *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

@end
