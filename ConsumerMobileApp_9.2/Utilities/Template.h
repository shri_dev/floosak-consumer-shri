//
//  Template.h
//  Consumer Client
//
//  Created by android on 7/24/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Constants used to Load Feature menu view Based on keyTemplates(Navigation Type1).
 */
//ForType1
#define KEY_TEMPLATES @"mainTemplates"
#define IMAGES @"images"
#define SUB_TEMPLATES @"templates"
#define SUB_TEMPLATES_PROPERTIES @"templateProperties"

/**
 * Constants used to Load Feature menu view Based on Ctaegory(Navigation Type2).
 */
//ForType2
#define CATEGORIES_NAMES_STRING    @"categoryNamesStrings"
#define CATEGORIES_NAMES           @"categoryNames"
#define CATEGORIES_IMAGES          @"categoryImages"
#define KEY_CATEGORY_FEATURES      @"keyCategoryFeatures"
#define KEY_CATEGORY_TEMPLATES     @"keyCategoryTemplates"
#define KEY_CATEGORY_TEMPLATE_PROPERTY_FILES @"keyCategorytemplatePropertyFiles"

@interface Template : NSObject
/**
 * Constants used to define Template(Feature - Actiontype,Datarray....etc).
 */
@property(nonatomic,retain) NSString *actionType;
@property(nonatomic,retain) NSString *nextTemplateName;
@property(nonatomic,retain) NSString *nextTemplatepropertyFile;
@property(nonatomic,retain) NSString *currentTemplatepropertyFile;
@property(nonatomic,retain) NSString *processorCode;
@property(nonatomic,retain) NSString *selectedLocalProcessorCode;
@property(nonatomic,retain) NSMutableDictionary *dictionary;
@property(nonatomic,retain) NSMutableArray *contentArray;
@property(nonatomic,retain) NSMutableArray *labelValidationArray;
@property(nonatomic,retain) NSString *alertType;
@property(nonatomic,retain) NSString *validationType;
@property(nonatomic,retain) id classType;
@property(nonatomic,retain) NSString *apiParameterType;
@property(nonatomic,retain) NSString *transactionType;
@property(nonatomic,assign) int selectedIndex;
@property(nonatomic,assign) NSString *currentClass;
@property(nonatomic,assign) SEL selector;
@property(nonatomic,assign) id target;

/**
 * This method is used to define Template.
 */
+(Template *)sharedTemplate;

/**
 * This method is used to define Template Realated Properties (Feature - NextTemplate and NextTemplate Property file).
 */
+(Template *) initWithactionType:(NSString *)actionType nextTemplate:(NSString *)nextTemplate nextTemplatePropertyFile:(NSString *)nextTemplatePropertyFile currentTemplatePropertyFile:(NSString *)currentTemplatePropertyFile  ProcessorCode:(NSString *)ProcessorCode Dictionary:(NSMutableDictionary *)Dictionary contentArray:(NSMutableArray *)contentArray alertType:(NSString *)alertType ValidationType:(NSString *)ValidationType inClass :(id)classType withApiParameter:(NSString *)apiParameterType withLableValidation :(NSMutableArray *)labelValidationArray withSelectedLocalProcessorCode:(NSString *)selectedLocalProcessorCode withSelectedIndex:(int)selectedIndex withTransactionType:(NSString *)transactionType withCurrentClass:(NSString *)currentClass;
/**
 * This method is used to get nextTemplate feature menu Index
 */
+(NSDictionary *) getNextFeatureTemplateWithIndex:(int) tag;
/**
 * This method is used to get processor code of the Property file.
 */
+(NSString *)getProcessorCodeWithData :(NSString *)data;
/**
 * This method is used to get transaction code of the Property file.
 */
+(NSString *)getTransactionCodeWithData :(NSString *)data;
/**
 * This method is used to get Reference Parameter of Template
 */
+(NSString *)getReferenceParameter :(NSString *)data;
/**
 * This method is used to get SMS mode Template Data.
 */
+(NSString *)getSMSFormatTextWithDictionary:(NSDictionary *)valueDictionary;
/**
 * This method is used to get Application mode (SMS or GPRS).
 */
+(BOOL) checkMode;
/**
 * This method is used to get Formated key values dictionary.
 */
+(NSString *)getFormatedKey:(NSMutableDictionary *)valueDictionary;
/**
 * This method is used to get Fee for amount based Transaction Type.
 */
+(NSString *) getFeeForAmount:(NSString *)feeAmount andTransactionType:(NSString *)transactionType;
/**
 * This method is used to Format the message.
 */
+(NSString *)formatMessageWithMessage:(NSString *)error withReferenceValues:(NSString *)reference;

@end
