//
//  SpeechManager.h
//  TTSDemo
//
//  Created by test on 08/05/17.
//  Copyright (c) 2017 Obopay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface SpeechManager : NSObject
@property (nonatomic,strong)AVSpeechSynthesizer *synthesizer;
@property (nonatomic,strong)NSString *language;
+(instancetype)sharedManager;
-(void)speakWithString:(NSString *)voiceStr;
-(void)stopSpeaking;
@end
