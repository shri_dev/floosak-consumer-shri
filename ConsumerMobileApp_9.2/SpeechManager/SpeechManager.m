//
//  SpeechManager.m
//  TTSDemo
//
//  Created by test on 08/05/17.
//  Copyright (c) 2017 Obopay. All rights reserved.
//

#import "SpeechManager.h"
@implementation SpeechManager
@synthesize synthesizer,language;

+(instancetype)sharedManager
{
    static SpeechManager *manager=nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        if (manager==nil)
        {
            manager=[[super alloc] init];
            [manager createSpeechSynthesizer];
        }
    });
    return manager;
}

-(void)createSpeechSynthesizer
{
    
    /*
    
     Supporting Language
    
    ********************************************
    Language: pt-BR
    Language: fr-CA
    Language: sk-SK
    Language: th-TH
    Language: ro-RO
    Language: no-NO
    Language: fi-FI
    Language: pl-PL
    Language: de-DE
    Language: nl-NL
    Language: id-ID
    Language: tr-TR
    Language: it-IT
    Language: pt-PT
    Language: fr-FR
    Language: ru-RU
    Language: es-MX
    Language: zh-HK
    Language: sv-SE
    Language: hu-HU
    Language: zh-TW
    Language: es-ES
    Language: zh-CN
    Language: nl-BE
    Language: en-GB
    Language: ar-SA
    Language: ko-KR
    Language: cs-CZ
    Language: en-ZA
    Language: en-AU
    Language: da-DK
    Language: en-US
    Language: en-IE
    Language: he-IL
    Language: hi-IN
    Language: el-GR
    Language: ja-JP
     
   **********************************************
     */
    
     language=@"hi-IN";
    synthesizer=[[AVSpeechSynthesizer alloc] init];
    
}

-(void)speakWithString:(NSString *)voiceStr
{
    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:voiceStr];
    [utterance setRate:AVSpeechUtteranceMinimumSpeechRate];
    [utterance setPitchMultiplier:1.0];
    [utterance setVolume:1.0];
    [utterance setVoice:[AVSpeechSynthesisVoice voiceWithLanguage:((language==nil)?nil:language)]];
    [synthesizer speakUtterance:utterance];
}

-(void)stopSpeaking
{
    [synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
}

@end
