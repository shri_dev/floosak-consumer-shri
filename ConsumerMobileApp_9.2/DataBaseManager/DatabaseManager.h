//
//  DatabaseManager.h
//  GenericDatabase
//
//  Created by PRABAKAR MP on 27/07/12.
//  Copyright 2012 CORPUS MOBILE LABS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DatabaseManager : NSObject{
    NSString *dbName;
}
@property(nonatomic,retain) NSString *dbName;

- (id)initWithDatabaseName:(NSString *)databaseName;
- (BOOL)findDBPresent;
- (NSString *)getDBPath;
- (NSError *)createDatabaseIfNoDBPresent;

- (void)deleteRecordFromTable:(NSString *) deleteStmt;
- (void)updateTableData:(NSString *)updateStmt;
- (void)showAlert :(NSString *)aTitle description:(NSString *)aDescription;


// New

// Logined user Details
- (NSArray *)getAllLoginColumns;
- (NSArray *)getAllLoginInformation;
- (NSError *)recordInsertionIntoLoginUserDetailsWithData:(NSArray *)dataArray;
- (void)deleteAllDetailsFromLoginDetails;
- (void)updateTableDataInLoginDetails :(NSString *) imagePath;


// New
//Launch API Deatails
- (NSArray *)getLaunchAPIColumns;
- (NSArray *)getAllLaunchAPIInformation;
- (NSError *)recordInsertionIntoLaunchAPIUserDetailsWithData:(NSArray *)dataArray;
- (void)updateTableDataInLaunchAPIDetails:(NSString *)localImageUrl forServerURL : (NSString *)marketImageURL;
- (void)deleteAllDetailsFromLaunchAPIDetails;




// Transaction Details
- (NSArray *)getAllTransactionHistory;
-(NSArray *)getAllTransactionHistoryWithTransactionCode:(NSString*)transactionType;
- (NSError *)recordInsertionIntoTransactionDetailsWithData:(NSArray *)dataArray;
- (NSError *)deleteFirstRecordInsertionFromTransactionDetails;
- (void)deleteAllDetailsFromTransactionHistory;

// Drop Down Time Stamp Details
- (NSArray *)getAllDropDownTimeStapDetails;
- (NSError *)recordInsertionIntoDropDownTimeStampWithData:(NSArray *)dataArray;
- (void)deleteAllDetailsFromDropDownTimeStampDetails;

// Drop Down Details
- (NSArray *)getAllDropDownDetails;
- (NSArray *)getAllDropDownDetailsForKey:(NSString *)keyValue;
- (NSError *)recordInsertionIntoDropDownDetailsWithData:(NSArray *)dataArray;
- (void)deleteAllDetailsFromDropDownDetails;

// Ministatements Details
-(NSArray *)getAllMiniStatementColumns;
-(NSArray *)getAllMiniStatementDetails;
- (void)deleteAllDetailsFromMiniStatement;
- (NSError *)recordInsertionIntoMiniStatementDetailsWithData:(NSArray *)dataArray;


// PayBill Transaction Details
-(NSArray *)getAllPayBillTransactionColumns;
-(NSArray *)getAllPayBillTransactionDetails;
//- (void)deleteAllDetailsFromPayBillTransaction;
- (NSError *)recordInsertionIntoPayBillTransactionDetailsWithData:(NSArray *)dataArray;


// Pay Bills Details
- (void)deleteAllDetailsPayBills;
- (void)deleteAllPayBillsTransactionHistory;
-(void)deleteFromDropDownValueWithType :(NSString *)type;


// Common Error method
- (NSError *)recordInsertionErrorWithResult:(int)result;

//Update Table
- (void)updateTableDataInDropDownTimeStamp:(NSString *)dropDownType withStatus: (NSString *)status;
- (void)updateTableDataInLaunchAPIDetailsWithServerURL:(NSString *)localImageUrl forServerURL: (NSString *)marketImageURL;

@end
