//
//  XmlParserHandler.h
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceDataObject.h"
#import "WebServiceConstants.h"

@protocol XMLParserHandlerDelegate <NSObject>

@required

-(void)errorCodeHandlingInXML:(NSString *)errorCode andMessgae:(NSString *)message withProcessorCode:(NSString *)processorCode;

@end


@interface XmlParserHandler : NSObject<NSXMLParserDelegate>
{
    BOOL isFalutResponse;
    NSString *currentElement;
    NSMutableString *foundValue;
    WebServiceDataObject *webServiceDataObject;
    id senderDelegate;
}

@property (nonatomic, strong) NSXMLParser *xmlParser;
@property (nonatomic, assign) NSObject <XMLParserHandlerDelegate> *delegate;

-(void)parseResponseData:(NSData *)pResponse withReqDictionary:(NSMutableDictionary *)reqDict withDelegate:(id)sendDelegate;

@end
