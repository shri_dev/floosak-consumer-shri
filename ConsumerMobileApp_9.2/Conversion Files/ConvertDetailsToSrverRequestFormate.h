//
//  ConvertDetailsToSrverRequestFormate.h
//  Consumer Client
//
//  Created by android on 6/16/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConvertDetailsToSrverRequestFormate : NSObject


-(NSDictionary *)convertTheFetchFeeValues:(NSDictionary *)inputDictionary;
-(NSDictionary *)convertTheP2PValues:(NSDictionary *)inputDictionary;
-(NSDictionary *)convertThePayBillValues:(NSDictionary *)inputDictionary;
-(NSDictionary *)convertTheAddBillerValues:(NSDictionary *)inputDictionary;
-(NSDictionary *)convertTheCASHINValues:(NSDictionary *)inputDictionary;
@end
