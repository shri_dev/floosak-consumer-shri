//
//  UITextField+RTL.m
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/17/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#import "UITextField+RTL.h"

@implementation UITextField (RTL)
-(instancetype)init
{
    self=[super init];
    if (self)
    {
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            self.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
            self.textAlignment=NSTextAlignmentRight;
        }
        else
        {
            self.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
            self.textAlignment=NSTextAlignmentLeft;
        }
    }
    return self;
}

- (void)drawPlaceholderInRect:(CGRect)rect
{
    [self.textColor setFill];
    CGRect placeholderRect = CGRectMake(rect.origin.x, (rect.size.height- self.font.pointSize)/2, rect.size.width, self.font.pointSize);
    [[self placeholder] drawInRect:placeholderRect withFont:self.font lineBreakMode:NSLineBreakByWordWrapping alignment:self.textAlignment];
}

-(NSTextAlignment)textAlignment
{
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    
    if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
    {
        return NSTextAlignmentRight;
    }
    return NSTextAlignmentLeft;
}

- (UITextInputMode *)textInputMode
{
     NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    if (self.keyboardType == UIKeyboardTypePhonePad || self.keyboardType == UIKeyboardTypeNumberPad ||self.keyboardType==UIKeyboardTypeDecimalPad)
    {
            for (UITextInputMode *tim in [UITextInputMode activeInputModes])
            {
                if ([[self langFromLocale:@"en"] isEqualToString:[self langFromLocale:tim.primaryLanguage]])
                    return tim;
            }
    }
    else
    {
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            for (UITextInputMode *tim in [UITextInputMode activeInputModes])
            {
                if ([[self langFromLocale:@"ar"] isEqualToString:[self langFromLocale:tim.primaryLanguage]])
                    return tim;
            }
        }
        else
        {
            for (UITextInputMode *tim in [UITextInputMode activeInputModes])
            {
                if ([[self langFromLocale:@"en"] isEqualToString:[self langFromLocale:tim.primaryLanguage]])
                    return tim;
            }
        }
    }
     return [super textInputMode];
}

-(NSString *)langFromLocale:(NSString *)locale
{
    NSRange r = [locale rangeOfString:@"_"];
    if (r.length == 0) r.location = locale.length;
    NSRange r2 = [locale rangeOfString:@"-"];
    if (r2.length == 0) r2.location = locale.length;
    return [[locale substringToIndex:MIN(r.location, r2.location)] lowercaseString];
}

@end
