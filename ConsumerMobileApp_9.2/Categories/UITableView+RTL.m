//
//  UITableView+RTL.m
//  ConsumerMobileApp_9.2
//
//  Created by Administrator on 03/11/17.
//  Copyright © 2017 Soumya. All rights reserved.
//
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"
#import "UITableView+RTL.h"

@implementation UITableView (RTL)
-(id)init
{
    self=[super init];
    if (self)
    {
        self.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
        self.tableHeaderView=[[UIView alloc] initWithFrame:CGRectZero];
    }
    return self;
}
@end
