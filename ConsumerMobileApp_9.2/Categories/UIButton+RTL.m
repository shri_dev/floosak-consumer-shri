//
//  UIButton+RTL.m
//  ConsumerMobileApp_9.2
//
//  Created by test on 8/17/17.
//  Copyright © 2017 Soumya. All rights reserved.
//

#import "UIButton+RTL.h"
@implementation UIButton (RTL)
-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self)
    {
        
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
            self.titleLabel.layer.affineTransform=CGAffineTransformMakeScale(-1.0, 1.0);
        }
        else
        {
            self.titleLabel.layer.affineTransform=CGAffineTransformMakeScale(1.0, 1.0);
        }
        
    }
    return self;
}

@end
