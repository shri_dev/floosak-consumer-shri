
//  WebSericeUtils.m
//  Consumer Client
//
//  Created by android on 6/4/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "DatabaseConstatants.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "ValidationsClass.h"
@implementation WebSericeUtils

/*
 * This method is used to set all web service api Methods.
 */
-(NSMutableDictionary *)getWebServiceBundleObjectWithProccessorCode:(NSString *)proccessorCode withInputDataModel:(NSDictionary *)dataDictionary
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] init];
    if([proccessorCode isEqualToString:PROCESSOR_CODE_FETCH_AREA_CODES]){
        valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        NSMutableDictionary *localWebServiceValues = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
        [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
        if (![localWebServiceValues objectForKey:PARAMETER14]) {
            [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER14];
        }
        if (![localWebServiceValues objectForKey:PARAMETER16]) {
            [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
        }
        if (![localWebServiceValues objectForKey:PARAMETER21]) {
            [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER21];
        }
        if (![localWebServiceValues objectForKey:PARAMETER22]) {
            [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
        }
        if (![localWebServiceValues objectForKey:PARAMETER23]) {
            [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
        }
        if (![localWebServiceValues objectForKey:PARAMETER24]) {
            [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
        }
        if (![localWebServiceValues objectForKey:PARAMETER25]) {
            [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
        }
        
        //if user lang is arabic, set pd4 = loggedin mobile number..
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
        {
            [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomerPhoneNumber"] forKey:PARAMETER20];
        }
        [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
        [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
//        <java1:Reference1>AreaCode</java1:Reference1>
//        <java1:Reference2>Billpay</java1:Reference2>
        [valuesDictionary setObject:@"AreaCode" forKey:PARAMETER34];
        [valuesDictionary setObject:@"Billpay" forKey:PARAMETER35];
    }else if([proccessorCode isEqualToString:PROCESSOR_CODE_GET_GOVERNORATES]){
        valuesDictionary = [self getGovernoratesBundle:dataDictionary];
    }else
    if ([proccessorCode isEqualToString:PROCESSOR_CODE_FETCH_MY_PAYEES])
    {
        valuesDictionary = [self getMyPayeeBundle:dataDictionary];
    }
    else
    if ([proccessorCode isEqualToString:PROCESSOR_CODE_AGGREGATOR_LINKED_BANK_LIST]) {
        valuesDictionary = [self getAggregatorLinkedBankList:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_CASHOUT_BANK_AGGREGATOR]) {
        valuesDictionary = [self cashoutBank:dataDictionary];
    }
    else
    if ([proccessorCode isEqualToString:PROCESSOR_CODE_CASHOUT_BANK_LIST]) {
        valuesDictionary = [self fetchCashOutBankList:dataDictionary];
    }
    else
    if ([proccessorCode isEqualToString:PROCESSOR_CODE_FETCH_BENEFICIARY_BANK])
    {
        valuesDictionary = [self fetchBankBeneficiary:dataDictionary];
    }
    else
    if ([proccessorCode isEqualToString:PROCESSOR_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR])
    {
        valuesDictionary = [self topUpMobilePrepaid:dataDictionary];
    }
    else
    if ([proccessorCode isEqualToString:PROCESSOR_CODE_BANK])
    {
        valuesDictionary = [self addBankIMPSIFSCBenificiary:dataDictionary];
    }
    else
    if ([proccessorCode isEqualToString:PROCESSOR_CODE_FETCH_IFSC])
    {
        valuesDictionary = [self getCityList:dataDictionary];
    }
    else
    if ([proccessorCode isEqualToString:PROCESSOR_CODE_LOGIN])
    {
        valuesDictionary = [self getLoginWebServiceParameterFinalList:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_MINI_STATEMENT])
    {
        valuesDictionary = [self getMiniStatementParameterFinalList:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_BALANCE_ENQUIRY])
    {
        valuesDictionary = [self getBalanceEnquiryBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_FETCH_FEE])
    {
        valuesDictionary = [self getFetchFeeBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_P2P_REGISTERED_BENEFICIARY_MOBILE_NUMBER])
    {
        valuesDictionary = [self getP2PUsingMobileNumberBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_SELECT_LIST_DROPDOWN_LIST])
    {
        valuesDictionary = [self getDropdownList:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_CAHNGE_PIN])
    {
        valuesDictionary = [self getChangeMPIN:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_RESET_PIN_FORGOT_MPIN])
    {
        valuesDictionary = [self getForgotMPIN:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_ACTIVATION])
    {
        valuesDictionary = [self getActivationPin:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_CHANGE_LANG])
    {
        valuesDictionary = [self getChangeLaunguage:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_CASH_OUT_WITHOUT_OTP])
    {
        valuesDictionary = [self getCashOutWithOutOTP:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_AGENT_DETAILS])
    {
        valuesDictionary = [self getAgentDetails:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_SELF_REG])
    {
        valuesDictionary = [self getSignUpDetails:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_PENDING_BILLS])
    {
        valuesDictionary = [self getPayBillPendingDetails:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLS])
    {
        valuesDictionary = [self getPayFetchBillsDetails:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_PAY_BILLER_NICK_NAME])
    {
        valuesDictionary = [self getPayBillerByNickNameBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_FETCH_MY_BILLERS])
    {
        valuesDictionary = [self getFetchMyBillersBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_FETCH_BILLER_ALL])
    {
        valuesDictionary = [self getFetchAllBillersBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_ADD_BILLER])
    {
        valuesDictionary = [self getAddBillersBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_CHECK_MY_VELOCITY])
    {
        valuesDictionary = [self getVelocityLimitBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_PAY_MERCHANT])
    {
        valuesDictionary = [self getPayMerchantBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_ADD_PAYEE])
    {
        valuesDictionary = [self getAddPayeeBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_VIEW_PAYEE])
    {
        valuesDictionary = [self getViewPayeeBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_LAUNCH])
    {
        valuesDictionary = [self getLaunchAPIBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_PAYU_LOAD_AUTHENTICATE])
    {
        valuesDictionary = [self getPayUSeamlessAPIBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC])
    {
        valuesDictionary = [self getSignUpDKYCAPIBundle:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_PRODUCT_DETAILS])
    {
        valuesDictionary = [self getProductDetailsAPI:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_UPDATEDEVICE_ID])
    {
        valuesDictionary = [self getUpdateDeviceIDAPI:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_VIEW_ALL_ACTIVE_BUNDLES])
    {
        valuesDictionary = [self getViewAllBundlesAPI:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_BUY_BUNDLE])
    {
        valuesDictionary = [self getBuyBundleBundlesAPI:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_VIEW_SUBSCRIPTIONS])
    {
        valuesDictionary = [self getViewSubScriptionBundleAPI:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_UPGRADE_BUNDLE])
    {
        valuesDictionary = [self getUpGradeBundleAPI:dataDictionary];
    }
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_GET_LOCATION])
    {
        valuesDictionary = [self getLocateAgentBundleAPI:dataDictionary];
    }
//
    else if ([proccessorCode isEqualToString:PROCESSOR_CODE_CONTACT_CENTRE])
    {
        valuesDictionary = [self getContactCentreInfo:dataDictionary];
    }
      else
    {
        valuesDictionary = [self getDefaultAPIBundle:dataDictionary];
    }

     NSLog(@"Final Data Dictionary For Web Request : %@",valuesDictionary);

    if([[valuesDictionary objectForKey:@"ProcessorCode"] isEqualToString:@"0051"]){
        [valuesDictionary setObject:@"" forKey:@"SenderFirstName"];
        [valuesDictionary setObject:@"" forKey:@"SenderLastName"];
    }

    return valuesDictionary;
}

#pragma mark - ConsumerClient Application.
/*
 * This method is used to set all non input and input parameters for  Cash out bank aggregators web service api.
 */
- (NSMutableDictionary *)getAggregatorLinkedBankList:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    if ([localWebServiceValues objectForKey:@"SUPPORTED_AGGREGATOR_LIST_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"SUPPORTED_AGGREGATOR_LIST_typeName"] forKey:PARAMETER18];
    }
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for  Cash out bank web service api.
 */
- (NSMutableDictionary *)cashoutBank:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    if ([localWebServiceValues objectForKey:@"cashoutBankName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"cashoutBankName"] forKey:PARAMETER18];
    }
    
    
    [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER18] forKey:PARAMETER19];
   
    
    if ([localWebServiceValues objectForKey:@"SUPPORTED_AGGREGATOR_LIST_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"SUPPORTED_AGGREGATOR_LIST_typeDesc"] forKey:PARAMETER20];
    }
    
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:[valuesDictionary objectForKey:PARAMETER9] forKey:PARAMETER36];
    NSLog(@"Value Dictionary : %@",valuesDictionary);
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for Fetch Cash out bank list web service api.
 */
- (NSMutableDictionary *)fetchCashOutBankList:(NSDictionary *)localWebServiceValues
 {
     NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
     NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
     [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
     [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
     [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
     [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
     [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
     [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
     return valuesDictionary;
 }

/*
 * This method is used to set all non input and input parameters for Fetch bank beneficiary web service api.
 */
- (NSMutableDictionary *)fetchBankBeneficiary:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    
    NSString *str=[userDetails objectForKey:PARAMETER11];
    if (str.length>0)
        [valuesDictionary setObject:[userDetails objectForKey:PARAMETER11] forKey:PARAMETER11];
    
    NSString *str1=[userDetails objectForKey:PARAMETER12];
    if (str1)
        [valuesDictionary setObject:[userDetails objectForKey:PARAMETER12] forKey:PARAMETER12];
    
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for Topup Mobile web service api.
 */
- (NSMutableDictionary *)topUpMobilePrepaid:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    
    NSLog(@"Dictionary : %@",localWebServiceValues);
    
    if ([localWebServiceValues objectForKey:@"TOPUP_OPERATOR_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"TOPUP_OPERATOR_typeName"] forKey:PARAMETER18];
    }
    else
    if ([localWebServiceValues objectForKey:@"DTH_OPERATOR_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"DTH_OPERATOR_typeName"] forKey:PARAMETER18];
    }
    else
    {
        if ([[localWebServiceValues objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_TOP_UP_MOBILE]) {
            [valuesDictionary setObject:default_operator_id forKey:@"TOPUP_OPERATOR_typeName"];
            [valuesDictionary setObject:default_dth_operator_name forKey:@"TOPUP_OPERATOR_typeDesc"];
            [valuesDictionary setObject:default_operator_id forKey:PARAMETER18];
        }
        else
        if ([[localWebServiceValues objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_TOP_UP_PREPAID_MOBILE_OPERATOR]) {
            [valuesDictionary setObject:default_dth_operator_id forKey:@"DTH_TYPE_typeName"];
            [valuesDictionary setObject:default_dth_operator_name forKey:@"DTH_TYPE_typeDesc"];
            [valuesDictionary setObject:default_dth_operator_id forKey:PARAMETER18];
        }
    }

    if ([localWebServiceValues objectForKey:@"DTH_TYPE_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"DTH_TYPE_typeName"] forKey:PARAMETER35];
    }
    
    if ([localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACTION_OPERATOR_ID] forKey:PARAMETER18];
    }

    if ([localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_ID]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACTION_DTH_TYPE_ID] forKey:PARAMETER35];
    }


    
    if ([localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] forKey:PARAMETER36];
        [valuesDictionary setObject:[localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] forKey:PARAMETER19];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"staticListId"]) {
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListId"] isEqualToString:@"Mobile"]) {
            [valuesDictionary setObject:@"VOICE" forKey:PARAMETER20];
        }
        else
            [valuesDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"staticListId"] forKey:PARAMETER20];
    }
    
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER36];
    NSLog(@"TopUP Mobile Value Dictionary : %@",valuesDictionary);
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for Bank IFSC web service api.
 */
- (NSMutableDictionary *)addBankIMPSIFSCBenificiary:(NSDictionary *)localWebServiceValues
{
    NSDictionary *userDetails = [[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"];
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    if ([userDetails objectForKey:PARAMETER11]) {
        [valuesDictionary setObject:[userDetails objectForKey:PARAMETER11] forKey:PARAMETER11];
    }
    if ([userDetails objectForKey:PARAMETER12]) {
        [valuesDictionary setObject:[userDetails objectForKey:PARAMETER12] forKey:PARAMETER12];

    }
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];

    if ([[localWebServiceValues objectForKey:PARAMETER13] isEqualToString:TRANSACTION_CODE_BANK_MMID]) {
        if ([localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER]) {
            [valuesDictionary setObject:[localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACTION_BENIFICARY_PHONE_NUMBER] forKey:PARAMETER34];
        }
    }
    
    if ([localWebServiceValues objectForKey:@"bank"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"bank"] forKey:PARAMETER19];
    }
    //@"UTBI0BKM481" for testing
    
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];

    NSLog(@"BankIMPSIFSC Value Dictionary : %@",valuesDictionary);
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for Login web service api.
 */
- (NSMutableDictionary *)getLoginWebServiceParameterFinalList:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:PARAMETER6 forKey:PARAMETER6];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    
    //Using Simulator testing
  if (!([[AppDelegate getDeviceIdentifier] compare:@"(null)"]==NSOrderedSame)) {
        [valuesDictionary setObject:[AppDelegate getDeviceIdentifier] forKey:PARAMETER19];
    }
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];

    NSLog(@"valuesDictionary : %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for get city list web service api.
 */
- (NSMutableDictionary *)getCityList:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSLog(@"Local Web Service Values...%@",localWebServiceValues);
    
    if ([localWebServiceValues objectForKey:@"BANK_LIST_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"BANK_LIST_typeName"] forKey:PARAMETER18];
    }
    if ([localWebServiceValues objectForKey:@"STATE_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"STATE_typeName"] forKey:PARAMETER34];
    }
    if ([localWebServiceValues objectForKey:@"CITY_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"CITY_typeName"] forKey:PARAMETER19];
    }
    if ([localWebServiceValues objectForKey:@"Branch_LIST_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"Branch_LIST_typeName"] forKey:PARAMETER20];
    }
    
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    NSLog(@"valuesDictionary : %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for mini
 * statement web service api.
 */
- (NSMutableDictionary *)getMiniStatementParameterFinalList:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
        
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:NSLocalizedStringFromTableInBundle(@"application_maximum_no_of_records_history_transactions",@"GeneralSettings",[NSBundle mainBundle], nil) forKey:PARAMETER18];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // Saved Phone number we have to pass
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER36];
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for balance
 * enquiry web service api.
 */

- (NSMutableDictionary *)getBalanceEnquiryBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    // Saved Phone number we have to pass
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // Saved Phone number we have to pass
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER36];
    return valuesDictionary;
}


/*
 * This method is used to set all non input and input parameters for FetchFee web service api.
 */
- (NSMutableDictionary *)getFetchFeeBundle:(NSDictionary *)localWebServiceValues
{//Bank
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    /*
     * From ui elements the following things are required
     * payment details 1---------mpin   parameter position is : 17
     * payment details 2---------benficiary phone number parameter position is :18
     * payment details 3--------constants value for each feature parameter position is :19
     *
     *
     * TxnAmount---------------amount parameter position is :  25
     *
     */
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    
    if ([localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACITON_AMOUNT]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:TRANSACTION_DATA_TRANSACITON_AMOUNT] forKey:PARAMETER21];
    }
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"bankValCheck"]) {
        
        if ([localWebServiceValues objectForKey:PARAMETER19])
        {
            [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER19] forKey:PARAMETER18];
        }
      
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"bankValCheck"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    

    if ([localWebServiceValues objectForKey:PARAMETER9]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER9] forKey:PARAMETER19];
    }
    else
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER19];
  
  
    if ([[localWebServiceValues objectForKey:PARAMETER19] isEqualToString:@"UNLOAD"])
    {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER19] forKey:PARAMETER18];
    }
    
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    if ([[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9])
    {
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    }
     NSLog(@"FEE valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
    
}

/*
 * This method is used to set all non input and input parameters for P2PUsingMobileNumber
 *  web service api.
 */
- (NSMutableDictionary *)getP2PUsingMobileNumberBundle:(NSDictionary *)localWebServiceValues
{
    /*
     * From ui elements the following things are required
     * payment details 1---------mpin parameter position is : 17
     * payment details 2--------benficiary phone numberparameter position is :  18
     * TxnAmount---------------amount parameter position is :  21
     *
     */
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];

    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:[localWebServiceValues objectForKey:@"CustomerPhoneNumber"] forKey:PARAMETER18];  // Benificiary Ph Number
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    if ([[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9]) {
        // Saved Phone number we have to pass
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER36];
    }
    if ([[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] ) {
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    }
    return valuesDictionary;
}

/*
 * This method is used to set all drop down list
 *  web service api.
 */

- (NSMutableDictionary *)getDropdownList:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:TRANSACTION_CODE_BALANCE_ENQUIRY forKey:PARAMETER13];
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@",PROCESSOR_CODE_SELECT_LIST_DROPDOWN_LIST] forKey:PARAMETER15];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    NSUserDefaults *dropDownTypes = [NSUserDefaults standardUserDefaults];
    NSString *dropDownString = [[dropDownTypes objectForKey:@"dropDownTypes"] componentsJoinedByString:@","];
    if ([localWebServiceValues objectForKey:@"dropDownString"]) {
        NSLog(@"DropdownString : %@",[localWebServiceValues objectForKey:@"dropDownString"]);
        dropDownString = [localWebServiceValues objectForKey:@"dropDownString"];
    }
    [valuesDictionary setObject:dropDownString forKey:PARAMETER17];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27]; 
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:[dropDownTypes valueForKey:userLanguage] forKey:PARAMETER34];
    
    NSString *p34 = [valuesDictionary objectForKey:PARAMETER34];
    if(![p34 isEqualToString:@"Arabic"]){
        if(![p34 isEqualToString:@"English"]){
            [valuesDictionary setObject:@"Arabic" forKey:PARAMETER34];
        }
    }
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for Change MPIN web service api.
 */
- (NSMutableDictionary *)getChangeMPIN:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:TRANSACTION_CODE_CAHNGE_PIN forKey:PARAMETER13];
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@",PROCESSOR_CODE_CAHNGE_PIN] forKey:PARAMETER15];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER36];
    
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for Forgot MPIN web service api.
 */
- (NSMutableDictionary *)getForgotMPIN:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER9] forKey:PARAMETER36];
    
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
}


/*
 * This method is used to set all non input and input parameters for Activation  web service api.
 */
- (NSMutableDictionary *)getActivationPin:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for ChangeLanguage web service api.
 */
- (NSMutableDictionary *)getChangeLaunguage:(NSDictionary *)localWebServiceValues
{
    NSLog(@"localWebServiceValues ChangeLanguage: %@",localWebServiceValues);
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    if ([localWebServiceValues objectForKey:DROP_DOWN_TYPE_NAME]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:DROP_DOWN_TYPE_NAME] forKey:PARAMETER18];
    }
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for CashOutWithOut OTP web service api.
 */
- (NSMutableDictionary *)getAgentDetails:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];

    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    NSLog(@"User Phone Number : %@",[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9]);
    if ([[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9]) {
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    }
    [valuesDictionary setObject:TRANSACTION_CODE_AGENT_DETAILS forKey:PARAMETER13];
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@",PROCESSOR_CODE_AGENT_DETAILS] forKey:PARAMETER15];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
      [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
}


/*
 * This method is used to set all non input and input parameters for CashOutWithOut OTP web service api.
 */
- (NSMutableDictionary *)getCashOutWithOutOTP:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    
    if ([[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9]) {
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    }
    
    if ([[sta objectForKey:@"userDetails"]objectForKey:PARAMETER11]) {
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"]objectForKey:PARAMETER11] forKey:PARAMETER11];
    }
    
    if ([[sta objectForKey:@"userDetails"]objectForKey:PARAMETER12]) {
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"]objectForKey:PARAMETER12] forKey:PARAMETER12];
    }
    
    if ([localWebServiceValues objectForKey:PARAMETER6]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER6] forKey:PARAMETER18];
    }
    
    [valuesDictionary setObject:TRANSACTION_CODE_CASH_OUT_WITHOUT_OTP forKey:PARAMETER13];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@",PROCESSOR_CODE_CASH_OUT_WITHOUT_OTP] forKey:PARAMETER15];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    if ([[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9]) {
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER36];
    }
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for SIgnUP web service api.
 */
- (NSMutableDictionary *)getSignUpDetails:(NSDictionary *)localWebServiceValues
{
    NSLog(@"localWebServiceValues SignUp: %@",localWebServiceValues);
    
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];

    NSString *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedDocType"];
    if(temp)
        [valuesDictionary setObject:temp forKey:@"ID Type"];

    
    if ([localWebServiceValues objectForKey:PARAMETER9]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER9] forKey:PARAMETER18];
    }
    // For represents trhat Single User mode of application.
    // To save Mobile Number Locally For further Reference.
    int userLevel = (int)[NSLocalizedStringFromTableInBundle(@"application_default_user_support_mode",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    
    if (userLevel == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:[localWebServiceValues objectForKey:PARAMETER9] forKey:ACTIVATIONMOBILENUMBER];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    NSLog(@"Userdefaults saved number is..%@",[[NSUserDefaults standardUserDefaults]objectForKey:ACTIVATIONMOBILENUMBER]);
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    
     [valuesDictionary setObject:[NSString stringWithFormat:@"%@~%@",[localWebServiceValues objectForKey:PARAMETER10],[localWebServiceValues objectForKey:[NSString stringWithFormat:@"input_%@",PARAMETER10]]] forKey:PARAMETER10];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:SELF_REG_PAYMENTDETAILS2 forKey:PARAMETER18];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:SELF_REG_REFERENCE1 forKey:PARAMETER34];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
    {
        [valuesDictionary setObject:@"Arabic" forKey:PARAMETER34];
    }else{
        [valuesDictionary setObject:@"English" forKey:PARAMETER34];
    }
//    NSString *param17 = [ValidationsClass encryptString:[self getPaymentDetails1:valuesDictionary]];
    NSString *param17 = [self getPaymentDetails1:valuesDictionary];
    [valuesDictionary setObject:param17 forKey:PARAMETER17];
    [valuesDictionary setObject:@"221" forKey:PARAMETER13];
//    [valuesDictionary setObject:@"65123456789" forKey:PARAMETER10];
    
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

-(NSString*)getPaymentDetails1:(NSMutableDictionary*)dict{
    NSLog(@"preparing params : %@",dict);
    

    NSString *str = [dict objectForKey:@"SenderFirstName"];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"SenderFatherName"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"SenderGrandFatherName"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"SenderFamilyName"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"PaymentDetails3"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"SenderPlaceofBirth"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"PaymentDetails4"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"ID Type"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"ID Number"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"ID Expiry Date"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"Governorate"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"District"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"Street"]];
    str = [str stringByAppendingString:@"|"];
    str = [str stringByAppendingString:[dict objectForKey:@"Zone"]];

    NSLog(@"data string : %@", str);
//    @"sample firstname|sample fathersname|sample frame|sample familyname|22/02/1990|placeofbirth|Male|National ID|65678987623|22/02/2029|Al Hudaydah|Al Mighlaf|test street|testzone";
    
//    firstname|sample fathername|sample grandfathername|sample familyname|22/02/1994|Test Birthplace|Male|FOREIGN PASSPORT~(null)|787654321|12/12/2020|Al Maharah|Sarar|Test Street|test zone
    return str;
}

/*
 * This method is used to set all non input and input parameters for pay bill pending web service api.
 */
- (NSMutableDictionary *)getPayBillPendingDetails:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    
    [valuesDictionary setObject:TRANSACTION_CODE_PENDING_BILLS forKey:PARAMETER13];
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@",PROCESSOR_CODE_PENDING_BILLS] forKey:PARAMETER15];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER36];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
}


/*
 * This method is used to set all non input and input parameters for Fetch Billers web service api.
 */
- (NSMutableDictionary *)getPayFetchBillsDetails:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
}


/*
 * This method is used to set all non input and input parameters for pay billers by nickname web service api.
 */
- (NSMutableDictionary *)getPayBillerByNickNameBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    
    if([localWebServiceValues objectForKey:BILLERREFERENCE1] && [localWebServiceValues objectForKey:BILLERREFERENCE2]){
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@|%@",[localWebServiceValues objectForKey:BILLERREFERENCE1],[localWebServiceValues objectForKey:BILLERREFERENCE2]] forKey:PARAMETER19];
    }
    if([localWebServiceValues objectForKey:PARAMETER21]){
        [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER21] forKey:PARAMETER21];
    }
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:VALUE_TWO forKey:PARAMETER35];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER38];
    if (![localWebServiceValues objectForKey:BILLERMASTERID]) {
        if([localWebServiceValues objectForKey:PARAMETER20]){
            [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER20] forKey:PARAMETER20];
        }
        [valuesDictionary setObject:@"" forKey:PARAMETER18];
    }else
    {
        NSString *str=[[sta objectForKey:@"userDetails"]objectForKey:PARAMETER11];
        if (str.length>0)
            [valuesDictionary setObject:[[sta objectForKey:@"userDetails"]objectForKey:PARAMETER11] forKey:PARAMETER11];
        NSString *str1=[[sta objectForKey:@"userDetails"]objectForKey:PARAMETER12];
        if (str1.length>0)
            [valuesDictionary setObject:[[sta objectForKey:@"userDetails"]objectForKey:PARAMETER12] forKey:PARAMETER12];
        
        if ([localWebServiceValues objectForKey:PARAMETER15] && [[localWebServiceValues objectForKey:PARAMETER15] rangeOfString:@"_"].location != NSNotFound) {
            [valuesDictionary setObject:[[[localWebServiceValues objectForKey:PARAMETER15] componentsSeparatedByString:@"_"] objectAtIndex:0] forKey:PARAMETER15];
        }
        if ([localWebServiceValues objectForKey:PARAMETER20])
        {
            [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER20] forKey:PARAMETER20];
        }
        
    }

    
    //by shri
    NSString *areaCode = @"";
    NSArray *temp2;
    NSString *temp = [valuesDictionary objectForKey:BILLERREFERENCE2];
    if(!temp)
        temp = [valuesDictionary objectForKey:@"areaCode"];
    
    if(temp)
        temp2 = [temp componentsSeparatedByString:@"-"];
    
    if(temp2)
        areaCode = [temp2 objectAtIndex:0];
    
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@|%@|||", [valuesDictionary objectForKey:BILLERREFERENCE1], areaCode] forKey:PARAMETER19];
    if([[valuesDictionary objectForKey:PARAMETER18] isEqualToString:@""]){
            NSString *pd2 = [valuesDictionary objectForKey:BILLERID];
            pd2 = [NSString stringWithFormat:@"%@", pd2];
            [valuesDictionary setObject:pd2 forKey:PARAMETER18];
    }
    [valuesDictionary setObject:@"" forKey:PARAMETER20]; //PaymentDetails4
    [valuesDictionary setObject:@"108" forKey:PARAMETER13]; //transaction type
     NSLog(@"valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
    
}

/*
 * This method is used to set all non input and input parameters for PayMerchant web service api.
 */
- (NSMutableDictionary *)getPayMerchantBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSLog(@"Values dictionary is..%@",[localWebServiceValues description]);
    
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];

    if ([localWebServiceValues objectForKey:@"merchantId"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"merchantId"] forKey:PARAMETER18];
    }
    if ([localWebServiceValues objectForKey:@"fee"] ) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"fee"] forKey:PARAMETER24];
    }
//    if ([localWebServiceValues objectForKey:@"amount"] ) {
//        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"amount"] forKey:PARAMETER21];
//    }    
    
    if ([localWebServiceValues objectForKey:@"TxnAmount"] ) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"TxnAmount"] forKey:PARAMETER21];
    }
    
    if ([sta objectForKey:@"staticListId"])
    {
        [valuesDictionary setObject:[sta objectForKey:@"staticListId"] forKey:PARAMETER35];
    }
    
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    
    if ([[valuesDictionary objectForKey:PARAMETER15] rangeOfString:@"_"].location != NSNotFound) {
        [valuesDictionary setObject:[[[localWebServiceValues objectForKey:PARAMETER15]  componentsSeparatedByString:@"_"] objectAtIndex:0] forKey:PARAMETER15];
    }
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:VALUE_TWO forKey:PARAMETER38];
    NSLog(@"Merchant valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for AddPayee web service api.
 */
- (NSMutableDictionary *)getAddPayeeBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    if (![localWebServiceValues objectForKey:PARAMETER13]) {
        [valuesDictionary setObject:TRANSACTION_CODE_ADD_PAYEE forKey:PARAMETER13];
    }
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:PROCESSOR_CODE_ADD_PAYEE forKey:PARAMETER15];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    
    if ([localWebServiceValues objectForKey:PARAMETER13]) {
        NSString *type = [localWebServiceValues objectForKey:PARAMETER13];
        if ([type isEqualToString:TRANSACTION_CODE_FETCH_BENEFICIARY_IFSC] || [type isEqualToString:TRANSACTION_CODE_FETCH_BENEFICIARY_MMID] || [type isEqualToString:TRANSACTION_CODE_FETCH_BENEFICIARY_NEFT]) {
            [valuesDictionary setObject:[localWebServiceValues objectForKey:@"FirstName"] forKey:PARAMETER35];
            [valuesDictionary setObject:[localWebServiceValues objectForKey:@"LastName"] forKey:PARAMETER36];
            [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER37];
        }
    }
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}


/*
 * This method is used to set all non input and input parameters for governorates list web service api.
 */
- (NSMutableDictionary *)getGovernoratesBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    [valuesDictionary setObject:@"TREE|Governorate|NULL" forKey:PARAMETER17];
    [valuesDictionary setObject:@"Governorate;Districts" forKey:PARAMETER34];
    [valuesDictionary setObject:@"Registration" forKey:PARAMETER35];

    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];

    if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
    {
        [valuesDictionary setObject:@"Arabic" forKey:PARAMETER20];
    }
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for MyPayee web service api.
 */
- (NSMutableDictionary *)getMyPayeeBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    NSString *str=[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER11];
    if (str.length>0)
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"]objectForKey:PARAMETER11] forKey:PARAMETER11];
    NSString *str1=[[sta objectForKey:@"userDetails"]objectForKey:PARAMETER12];
    if (str1.length>0)
        [valuesDictionary setObject:[[sta objectForKey:@"userDetails"]objectForKey:PARAMETER12] forKey:PARAMETER12];
    
    if ([[localWebServiceValues objectForKey:PARAMETER15] rangeOfString:@"_"].location != NSNotFound) {
        [valuesDictionary setObject:[[[localWebServiceValues objectForKey:PARAMETER15] componentsSeparatedByString:@"_"] objectAtIndex:0] forKey:PARAMETER15];
    }
    
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    //NSLog(@"valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for View Payee web service api.
 */
- (NSMutableDictionary *)getViewPayeeBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:INSTRUMENT_TYPE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for FetchFee web service api.
 */
- (NSMutableDictionary *)getFetchMyBillersBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for Fetch all billers web service api.
 */
- (NSMutableDictionary *)getFetchAllBillersBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];

    if([[localWebServiceValues objectForKey:PARAMETER_TYPE] isEqualToString:@"BILLER_CATEGORY"])
    {
        [valuesDictionary removeObjectForKey:PARAMETER17];
        [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER17] forKey:PARAMETER19];
    }
    else if([[localWebServiceValues objectForKey:PARAMETER_TYPE] isEqualToString:@"BILLER_LOCATION"])
    {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER17] forKey:PARAMETER17];
    }
    else
    {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER18] forKey:PARAMETER18];
    }
    
    [valuesDictionary setObject:@"117" forKey:PARAMETER13];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for add billers web service api.
 */
- (NSMutableDictionary *)getAddBillersBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@|%@|||", [valuesDictionary objectForKey:BILLERREFERENCE1], [valuesDictionary objectForKey:@"areaCode"]] forKey:PARAMETER19];
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for Check My Velocity web service api.
 */
- (NSMutableDictionary *)getVelocityLimitBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:TRANSACTION_CODE_CHECK_MY_VELOCITY forKey:PARAMETER13];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@", PROCESSOR_CODE_CHECK_MY_VELOCITY] forKey:PARAMETER15];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
//    if([[valuesDictionary objectForKey:@"PaymentDetails3"] isEqualToString:@"BCASHOUT"]){
//        [valuesDictionary setObject:@"CASHOUT" forKey:@"PaymentDetails3"];
//    }
    return valuesDictionary;

}
/*
 * This method is used to set all non input and input parameters for Launch API web service api.
 */

- (NSMutableDictionary *)getLaunchAPIBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:TRANSACTION_CODE_LAUNCH forKey:PARAMETER13];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:[NSString stringWithFormat:@"%@",PROCESSOR_CODE_LAUNCH] forKey:PARAMETER15];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:PARAMETER_RETINA forKey:PARAMETER17];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
     NSLog(@"valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for Payu Seam less API web service api.
 */
-(NSMutableDictionary *)getPayUSeamlessAPIBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:[localWebServiceValues objectForKey:PARAMETER17] forKey:PARAMETER17];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER36];
     // NSLog(@"valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for SignUpDKYC API web service api.
 */
- (NSMutableDictionary *)getSignUpDKYCAPIBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    if ([localWebServiceValues objectForKey:@"DKYC_DOC_TYPE_typeName"]) {
        [valuesDictionary setObject:[localWebServiceValues objectForKey:@"DKYC_DOC_TYPE_typeName"] forKey:PARAMETER18];
    }
    [valuesDictionary setObject:@"1" forKey:PARAMETER14];
    
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    

    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedDocType"];
    if(str)
        [valuesDictionary setObject:str forKey:@"PaymentDetails2"];
    
    NSString *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"UploadImageOne"];
    if(temp)
        [valuesDictionary setObject:temp forKey:PARAMETER19];
    
    temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"UploadImageTwo"];
    if(temp)
        [valuesDictionary setObject:temp forKey:PARAMETER20];
    
    temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"UploadImageThree"];
    if(temp)
        [valuesDictionary setObject:temp forKey:PARAMETER34];
    
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for ProductDetails API web service api.
 */
- (NSMutableDictionary *)getProductDetailsAPI:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    // NSLog(@"valuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
    
}

/*
 * This method is used to set all non input and input parameters for getUpdateDeviceIDAPI  API web service api.
 */
- (NSMutableDictionary *)getUpdateDeviceIDAPI:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSString *deviceID = [AppDelegate getDeviceIdentifier];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:deviceID  forKey:PARAMETER18];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
     NSLog(@"UPDATE_DeviceID_ValuesDictionary: %@",valuesDictionary);
    
    return valuesDictionary;
    
}

/*
 * This method is used to set all non input and input parameters for getUpdateDeviceIDAPI  API web service api.
 */
- (NSMutableDictionary *)getViewAllBundlesAPI:(NSDictionary *)localWebServiceValues
{
     sleep(0.5);
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    NSLog(@"ViewAllBundle valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}
/*
 * This method is used to set all non input and input parameters for getUpdateDeviceIDAPI  API web service api.
 */
- (NSMutableDictionary *)getBuyBundleBundlesAPI:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER6];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:[localWebServiceValues objectForKey:@"transaction_type"] forKey:PARAMETER19];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    NSLog(@"BUYBundle valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for getUpdateDeviceIDAPI  API web service api.
 */
- (NSMutableDictionary *)getViewSubScriptionBundleAPI:(NSDictionary *)localWebServiceValues
{
    sleep(0.5);
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER6];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    NSLog(@"ViewSubScription valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

/*
 * This method is used to set all non input and input parameters for getUpdateDeviceIDAPI  API web service api.
 */
- (NSMutableDictionary *)getUpGradeBundleAPI:(NSDictionary *)localWebServiceValues
{
    sleep(0.5);
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    NSLog(@"UpGrade valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}


- (NSMutableDictionary *)getContactCentreInfo:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:localWebServiceValues[PARAMETER13] forKey:PARAMETER13];
    [valuesDictionary setObject:localWebServiceValues[PARAMETER15] forKey:PARAMETER15];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    if (localWebServiceValues[PARAMETER19] && [localWebServiceValues[PARAMETER19] isEqualToString:localWebServiceValues[@"COMPLAINT_FEEDBACK_TYPE_typeDesc"]])
    {
        [valuesDictionary setObject:localWebServiceValues[@"COMPLAINT_FEEDBACK_TYPE_typeName"] forKey:PARAMETER19];
    }
    
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    NSLog(@"getContactCentreInfo_valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;

}



/*
 * This method is used to set all non input and input parameters for getUpdateDeviceIDAPI  API web service api.
 */
- (NSMutableDictionary *)getLocateAgentBundleAPI:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER6];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    [valuesDictionary setObject:@"032" forKey:PARAMETER13];
    [valuesDictionary setObject:@"0182" forKey:PARAMETER15];
    [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER14];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER21];
    [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];
    NSLog(@"LocateAgent valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;
}

#pragma mark - Common For all Projects

/*
 * This method is used to set all non input and input parameters for Default API web service api.
 */
- (NSMutableDictionary *)getDefaultAPIBundle:(NSDictionary *)localWebServiceValues
{
    NSMutableDictionary *valuesDictionary = [[NSMutableDictionary alloc] initWithDictionary:localWebServiceValues];
    NSUserDefaults *sta = [NSUserDefaults standardUserDefaults];
    [valuesDictionary setObject:APIID_TYPE forKey:PARAMETER2];
    [valuesDictionary setObject:TIER1_AGENT_ID forKey:PARAMETER3];
    [valuesDictionary setObject:[[sta objectForKey:@"userDetails"] objectForKey:PARAMETER9] forKey:PARAMETER9];
    if (![localWebServiceValues objectForKey:PARAMETER14]) {
        [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER14];
    }
    if (![localWebServiceValues objectForKey:PARAMETER16]) {
        [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER16];
    }
    if (![localWebServiceValues objectForKey:PARAMETER21]) {
        [valuesDictionary setObject:VALUE_ONE forKey:PARAMETER21];
    }
    if (![localWebServiceValues objectForKey:PARAMETER22]) {
        [valuesDictionary setObject:CURRENCY_CODE forKey:PARAMETER22];
    }
    if (![localWebServiceValues objectForKey:PARAMETER23]) {
        [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER23];
    }
    if (![localWebServiceValues objectForKey:PARAMETER24]) {
        [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER24];
    }
    if (![localWebServiceValues objectForKey:PARAMETER25]) {
        [valuesDictionary setObject:VALUE_ZERO forKey:PARAMETER25];
    }
    
    [valuesDictionary setObject:[self getRequestId] forKey:PARAMETER27];
    [valuesDictionary setObject:[self getTime] forKey:PARAMETER32];

    NSLog(@"Default valuesDictionary: %@",valuesDictionary);
    return valuesDictionary;

}


/*
 * This method is used to get request Id.
 */

-(NSString *)getRequestId
{
    NSString *requestId = nil;
    long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    requestId = [[[NSString stringWithFormat:@"%ld",currentTime] stringByAppendingString:uniqueIdentifier] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSLog(@"Request ID : %@",requestId);
    requestId=[self getRequestIdWithEncryptedForm:requestId];
//    requestId=[self getRequestIdWithEncryptedForm:requestId];
    return requestId;
}


-(NSString *)getRequestIdWithEncryptedForm:(NSString *)value
{
    NSString *encrypted=nil;
    encrypted=[ValidationsClass encryptStringForRequestID:value];
    NSLog(@"Encrypted Request ID : %@", encrypted);
    return encrypted;
}

/*
 * This method is used to get years between dates.
 */

-(NSString *)yearsBetweenDates:(NSString *)userDOB
{
    NSString *age = nil;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:SETDATEFORMATDATEMONTHYEAR];
    NSDate *date = [dateFormatter dateFromString:userDOB];
    NSDate *today = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:date toDate:today options:0];
    age = [NSString stringWithFormat:@"%ld",(long)ageComponents.year];
    NSLog(@"age: %@",age);

    return age;
}

/*
 * This method is used to get CurrentTime.
 */
-(NSString *)getTime
{
    NSString *myString;
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSString *timeZone =  NSLocalizedStringFromTableInBundle(@"applicaiton_time_zone",@"GeneralSettings",[NSBundle mainBundle], nil);
    [dateFormatter  setTimeZone:[NSTimeZone timeZoneWithAbbreviation:timeZone]];
    [dateFormatter setDateFormat:SERVER_DATE_FORMAT];
     myString = [dateFormatter stringFromDate:now];
    NSLog(@"CurrentTime:%@",myString);
    myString = [myString stringByAppendingString:@"+05:30"];
    return myString;
}

@end
