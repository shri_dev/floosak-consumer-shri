//  Created by Shri on 6/13/18.
//  Copyright © 2018 Addtim. All rights reserved.

#import "CustomTextField.h"

@implementation CustomTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autocorrectionType = UITextAutocorrectionTypeNo;
    }
    return self;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
        return NO;
}

@end
