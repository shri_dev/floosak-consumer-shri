//
//  ActivityIndicator.h
//  Consumer Client
//
//  Created by android on 6/9/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicator : UIView

@property (nonatomic, retain) UIActivityIndicatorView * activityView;
@property (nonatomic, retain) UIView *loadingView;
@property (nonatomic, retain) UILabel *loadingLabel;

-(id)initWithFrame:(CGRect)frame;
-(void)startActivityIndicator;
-(void)stopActivityIndicator;

@end
