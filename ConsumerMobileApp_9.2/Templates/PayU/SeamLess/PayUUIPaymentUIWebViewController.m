//
//  PayUUIPaymentUIWebViewController.m
//  Consumer Client
//
//  Created by android on 12/21/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "PayUUIPaymentUIWebViewController.h"
#import "WebViewJavascriptBridge.h"
#import "Constants.h"
#import "iOSDefaultActivityIndicator.h"
#import "PayUConstants.h"
#import "PayUSAGetHashes.h"
#import "PayUSAGetTransactionID.h"
#import "WebServiceConstants.h"
#import "UIView+Toast.h"

@interface PayUUIPaymentUIWebViewController ()

@property WebViewJavascriptBridge* PayU;
@property (strong,nonatomic) PayUModelPaymentParams *paymentParam;

@property (nonatomic, strong) PayUCreateRequest *createRequest;
@property (nonatomic, strong) PayUValidations *validations;
@property (strong, nonatomic) PayUWebServiceResponse *webServiceResponse;
@property (strong, nonatomic) iOSDefaultActivityIndicator *defaultActivityIndicator;
@property (strong, nonatomic) PayUSAGetHashes *getHashesFromServer;
@property (strong, nonatomic) PayUSAGetTransactionID *getTransactionID;

@end

@implementation PayUUIPaymentUIWebViewController
@synthesize paymentRequest,paymentWebView,payeeDetailsDict,localDatadictvalues;

#pragma mark - PayUUIPaymentUIWebViewController UIViewController.
/**
 * This method is used to set implemention of PayUUIPaymentUIWebViewController.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view  withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
    
    NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        payeeDetailsDict=[[NSDictionary alloc]initWithDictionary:dataDictionary];
    }
        return self;
}
#pragma mark - ViewController Life Cycle Method.
/**
 * This method is used to set Load PayuWebView By using Input Request.
 */
- (void)viewDidLoad {
    
    // Amount
    int result= [[[NSUserDefaults standardUserDefaults] objectForKey:@"TranasctionAmount"] intValue];
    
    resultStr=[NSString stringWithFormat:@"%d",result];
    
    NSArray *payeeDetailsArray=[[NSArray alloc]initWithObjects:@"merchantKey",@"saltKey",@"email",@"product",@"sURL",@"fURL", nil];
    //    PayUAuthDetails
    NSArray *objects = [[[NSUserDefaults standardUserDefaults] objectForKey:@"PayUAuthDetails"] componentsSeparatedByString:@"|"];
    
    payuDetailsdict = [[NSMutableDictionary alloc] init];
    for (int i = 0; i< objects.count;i++)
    {
        if ([payeeDetailsArray[i] isEqualToString:@"merchantKey"]) {
            [payuDetailsdict setObject:@"gtKFFx" forKey:payeeDetailsArray[i]];
        }else if ([payeeDetailsArray[i] isEqualToString:@"saltKey"])
        {
            [payuDetailsdict setObject:@"eCwWELxi" forKey:payeeDetailsArray[i]];
        }else
        [payuDetailsdict setObject:objects[i] forKey:payeeDetailsArray[i]];
    }
        NSLog(@"Payu Details Dictioanry...%@",[payuDetailsdict description]);
    // WebView
    paymentWebView=[[UIWebView alloc]init];
    paymentWebView.frame=CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.getHashesFromServer = [PayUSAGetHashes new];
    paymentWebView.backgroundColor=[UIColor clearColor];
    self.navigationController.navigationBarHidden = NO;
    [self.view addSubview:paymentWebView];
    
    [activityIndicator startActivityIndicator];
    activityIndicator.hidden=NO;
    
    
    // Back Button
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"<Back" style:UIBarButtonItemStyleBordered target:self action:@selector(home:)];
    self.navigationItem.leftBarButtonItem=newBackButton;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self setActivityIndicator];
    [self generatePostData];
}
/**
 * This method is used to set Input Request For sending Payu server.
 * @Paramtypes For creating request- AMOUNT,PRODUCTINFO,FIRSTNAME,EMAIL, PHONE,FURL, SURL, USERCREDENTIAL and
 OFFERKEY
 */

-(void)generatePostData
{
    self.defaultActivityIndicator = [[iOSDefaultActivityIndicator alloc]init];
    self.paymentParam = [PayUModelPaymentParams new];
    self.getTransactionID = [PayUSAGetTransactionID new];
    
    //    saltKey
    
    // Merchant Key
    if ([payuDetailsdict objectForKey:@"saltKey"] == (id)[NSNull null] || [[payuDetailsdict objectForKey:@"saltKey"] length]== 0 || [[payuDetailsdict objectForKey:@"saltKey"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)
        self.paymentParam.key=@"gtKFFx";
    else
        self.paymentParam.key=[NSString stringWithFormat:@"%@",[payuDetailsdict objectForKey:@"saltKey"]];
    
    // Amount
    if (resultStr == (id)[NSNull null] || resultStr.length == 0 || [resultStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)
        self.paymentParam.amount=@"10.0";
    else
        self.paymentParam.amount=resultStr;
    
    // Product Info
    if ([payuDetailsdict objectForKey:@"product"] == (id)[NSNull null] || [[payuDetailsdict objectForKey:@"product"] length]== 0 || [[payuDetailsdict objectForKey:@"product"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)
        self.paymentParam.productInfo=@"Nokia";
    else
        self.paymentParam.productInfo=[NSString stringWithFormat:@"%@",[payuDetailsdict objectForKey:@"product"]];
    
    // Email
    if ([payuDetailsdict objectForKey:@"email"] == (id)[NSNull null] || [[payuDetailsdict objectForKey:@"email"] length]== 0 || [[payuDetailsdict objectForKey:@"email"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)
        self.paymentParam.email=@"email@testsdk1.com";
    else
        self.paymentParam.email=[NSString stringWithFormat:@"%@",[payuDetailsdict objectForKey:@"email"]];
    
    // Transaction Id
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PayuOpsTransactionId"] == (id)[NSNull null] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"PayuOpsTransactionId"] length]== 0 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"PayuOpsTransactionId"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)
    {
        self.paymentParam.transactionID=[self.getTransactionID getTransactionIDWithLength:15];
    }
    else
        self.paymentParam.transactionID=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"PayuOpsTransactionId"]];
    
    // Bank Code
    
    if ([payeeDetailsDict objectForKey:@"typeName"] == (id)[NSNull null] || [[payeeDetailsDict objectForKey:@"typeName"] length]== 0 || [[payeeDetailsDict objectForKey:@"typeName"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)
        self.paymentParam.bankCode=@"UBIBC";
    else
        self.paymentParam.bankCode=[NSString stringWithFormat:@"%@",[payeeDetailsDict objectForKey:@"typeName"]];
    
    
    // Bank payment Type
    [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPPT7_TemplateId"];
    
    // Success URL
//    if ([payuDetailsdict objectForKey:@"sURL"] == (id)[NSNull null] || [payuDetailsdict objectForKey:@"sURL"]== 0 || [[payuDetailsdict objectForKey:@"sURL"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)
//        self.paymentParam.SURL=@"https://payu.herokuapp.com/ios_success";
//    
//    else
        self.paymentParam.SURL=[NSString stringWithFormat:@"%@",[payuDetailsdict objectForKey:@"sURL"]];
    
    // Needed to check PLS un comment
//    NSLog(@"SuRl...%@",[payuDetailsdict objectForKey:@"sURL"]);
    
    // Failure URL
//    if ([payuDetailsdict objectForKey:@"fURL"] == (id)[NSNull null] || [[payuDetailsdict objectForKey:@"fURL"] length]== 0 || [[payuDetailsdict objectForKey:@"fURL"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)
//        self.paymentParam.FURL=@"https://payu.herokuapp.com/ios_failure";
//    else
        self.paymentParam.FURL=[NSString stringWithFormat:@"%@",[payuDetailsdict objectForKey:@"fURL"]];
    
    self.paymentParam.firstName=@"Ram";
    self.paymentParam.userCredentials = @"ra:ra";
    self.paymentParam.phoneNumber =[[[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"] objectForKey:PARAMETER9];
    
    
    //    Application PayuPG environment
    if (NSLocalizedStringFromTableInBundle(@"application_payu_mode",@"GeneralSettings",[NSBundle mainBundle], nil))
         self.paymentParam.Environment = NSLocalizedStringFromTableInBundle(@"application_payu_mode",@"GeneralSettings",[NSBundle mainBundle], nil);
    else
       self.paymentParam.Environment =ENVIRONMENT_MOBILETEST;

    self.paymentParam.offerKey = @"offertest@1411";
    self.paymentParam.expYear = [payeeDetailsDict objectForKey:@"expiryYear"];
    self.paymentParam.expMonth = [payeeDetailsDict objectForKey:@"expiryMonth"];
    self.paymentParam.nameOnCard = [payeeDetailsDict objectForKey:@"nameOnCard"];
    self.paymentParam.cardNumber = [payeeDetailsDict objectForKey:@"cardNumber"];
    self.paymentParam.CVV = [payeeDetailsDict objectForKey:@"CVV"];
    self.paymentParam.udf1 = @"u1";
    self.paymentParam.udf2 = @"u2";
    self.paymentParam.udf3 = @"u3";
    self.paymentParam.udf4 = @"u4";
    self.paymentParam.udf5 = @"u5";
    
    [self.getHashesFromServer generateHashFromServer:self.paymentParam withCompletionBlock:^(PayUHashes *hashes, NSString *errorString) {
        if (errorString) {
            dispatch_async(dispatch_get_main_queue(), ^{
                PAYUALERT(@"Error", errorString);
                [self removeActivityIndicator];
            });
        }
        else{
            self.paymentParam.hashes = hashes;
            PayUWebServiceResponse *respo = [PayUWebServiceResponse new];
            [respo callVASForMobileSDKWithPaymentParam:self.paymentParam];        //FORVAS1
            self.webServiceResponse = [PayUWebServiceResponse new];
            [self.webServiceResponse getPayUPaymentRelatedDetailForMobileSDK:self.paymentParam withCompletionBlock:^(PayUModelPaymentRelatedDetail *paymentRelatedDetails, NSString *errorMessage, id extraParam) {
                if (errorMessage) {
                    PAYUALERT(@"Error", errorString);
                }
                else{
                    
                    self.createRequest = [PayUCreateRequest new];
                    [self.createRequest createRequestWithPaymentParam:self.paymentParam forPaymentType:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedPPT7_TemplateId"] withCompletionBlock:^(NSMutableURLRequest *request, NSString *postParam, NSString *error) {
                        if (error == nil) {
                            paymentRequest=request;
                            [paymentWebView loadRequest:paymentRequest];
                            _PayU = [WebViewJavascriptBridge bridgeForWebView:paymentWebView webViewDelegate:self handler:^(id data, WVJBResponseCallback responseCallback)
                                     {
                                         if(data)
                                         {
                                             [self removeActivityIndicator];
                                             [[NSNotificationCenter defaultCenter] postNotificationName:@"passData" object:[NSMutableData dataWithData:data ]];
                                             responseCallback(@"Response for message from ObjC");
                                         }
                                     }];
                        }
                        else{
                            [self removeActivityIndicator];
                            [self.view makeToast:error duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor blackColor] textColor:[UIColor whiteColor]];
                            
                        }
                    }];
                }
            }];
        }
    }];
}

#pragma mark - WebView Delegate Methods
/**
 * This method is used Web View delegate method related to payu.
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    //Requst start
    return true;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    //Starting Request
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self removeActivityIndicator];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self removeActivityIndicator];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
}


#pragma mark - Navigation Bar back Button Action.
/**
 * This method is used to set Pop to Previous View Once PayMent Succeed or failure.
 */
-(void)home:(UIBarButtonItem *)sender {
    self.navigationController.navigationBarHidden = YES;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark - Default memory warning method.

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

