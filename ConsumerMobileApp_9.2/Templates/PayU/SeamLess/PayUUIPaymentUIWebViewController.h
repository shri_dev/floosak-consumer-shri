//
//  PayUUIPaymentUIWebViewController.h
//  Consumer Client
//
//  Created by android on 12/21/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayU_iOS_CoreSDK.h"
#import "ActivityIndicator.h"
#import "BaseViewController.h"

/**
 * This class used to handle functionality and ViewController of PayUUIPaymentUIWebViewController.
 *
 * @author Integra
 *
 */
@interface PayUUIPaymentUIWebViewController : BaseViewController<UIWebViewDelegate>
{
    NSString *resultStr;
    NSMutableDictionary *payuDetailsdict;
}
/**
 * declarations are used to set the UIConstraints Of PayUUIPaymentUIWebViewController.
 */
@property(nonatomic,strong) NSMutableURLRequest *paymentRequest;
@property (strong, nonatomic)  UIWebView *paymentWebView;
@property(strong,nonatomic) NSDictionary *payeeDetailsDict;
@property(strong,nonatomic) NSDictionary *localDatadictvalues;

/**
 * This method is  used for Method Initialization of PayUUIPaymentUIWebViewController.
 */
// Method For initialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view  withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;

@end
