//
//  PaymentViewController.m
//  Consumer Client
//
//  Created by android on 12/16/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import "PaymentViewController.h"
#import "Constants.h"
#import "ActivityIndicator.h"
#import "WebServiceConstants.h"
#import "PayUConstants.h"
#include <CommonCrypto/CommonDigest.h>


#define KEY             @"gtKFFx"
// define KEY             @"smsplus"
// define KEY             @"0MQaQP"
#define AMOUNT          @"100"
#define PRODUCTINFO     @"Nokia"
#define FIRSTNAME       @"Ram"
#define EMAIL           @"email@testsdk.com"
#define PHONE           @"9876543210"
#define FURL            @"https://dl.dropboxusercontent.com/s/h6m11xr93mxhfvf/Failure_iOS.html"
#define SURL            @"https://dl.dropboxusercontent.com/s/y911hgtgdkkiy0w/success_iOS.html"
#define USERCREDENTIAL  @"ra:ra"
#define OFFERKEY        @"test123@6622"

@interface PaymentViewController ()<UIWebViewDelegate>

//Merchant ID : gtKFFx
//Salt : eCwWELxi
@property (strong, nonatomic) UIWebView *payuWebView;
@property (strong, nonatomic) NSMutableURLRequest *req;
@property (strong, nonatomic) NSString *txnIDStr;
@property (strong, nonatomic) NSString *paymentHashStr;

typedef void (^urlRequestCompletionBlock)(NSURLResponse *response, NSData *data, NSError *connectionError);

@end

@implementation PaymentViewController

@synthesize payuWebView,payeeDetailsDict,req,txnIDStr,paymentHashStr;

#pragma mark - PaymentViewController UIViewController.
/**
 * This method is used to set implemention of PaymentViewController.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view  withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
    
    NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        payeeDetailsDict=[[NSDictionary alloc]initWithDictionary:dataDictionary];
        
        NSString *opsTransactionStr=[[NSUserDefaults standardUserDefaults] objectForKey:@"PayuOpsTransactionId"];
        NSLog(@"Opstransaction Id..%@",opsTransactionStr);
    }
    return self;
}

#pragma mark - VIewController Life Cycle.
/**
 * This method is used to set Load PayuWebView By using Input Request.
 */
- (void)viewDidLoad
{
    self.view.backgroundColor=[UIColor whiteColor];
    
    int result= [[[NSUserDefaults standardUserDefaults] objectForKey:@"TranasctionAmount"] intValue];
    resultStr=[NSString stringWithFormat:@"%d",result];
    
    NSArray *payeeDetailsArray=[[NSArray alloc]initWithObjects:@"merchantKey",@"saltKey",@"email",@"product",@"sURL",@"fURL", nil];
    NSLog(@"PayuDetails Dict....%@",payuDetailsdict);
    NSArray *objects = [[payeeDetailsDict objectForKey:@"PayUAuthDetails"] componentsSeparatedByString:@"|"];
    NSLog(@"Object Values : %@",objects);
    
    payuDetailsdict=[[NSMutableDictionary alloc] init];
    
    for (int i = 0; i< objects.count;i++)
    {
        if ([payeeDetailsArray[i] isEqualToString:@"merchantKey"])
        {
            //[payuDetailsdict setObject:@"eCwWELxi" forKey:payeeDetailsArray[i]];
            [payuDetailsdict setObject:NSLocalizedStringFromTableInBundle(@"application_payu_non_seamless_merchant_key",@"GeneralSettings",[NSBundle mainBundle], nil) forKey:payeeDetailsArray[i]];
            
        }else if([payeeDetailsArray[i] isEqualToString:@"saltKey"])
        {
             [payuDetailsdict setObject:NSLocalizedStringFromTableInBundle(@"application_payu_non_seamless_salt_key",@"GeneralSettings",[NSBundle mainBundle], nil) forKey:payeeDetailsArray[i]];
            
        }else
        [payuDetailsdict setObject:objects[i] forKey:payeeDetailsArray[i]];
        
    }
    NSLog(@"PayuDetails Dict....%@",payuDetailsdict);
    
    // WebView
    payuWebView=[[UIWebView alloc]init];
    payuWebView.frame=CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    payuWebView.backgroundColor=[UIColor clearColor];
    self.navigationController.navigationBarHidden = NO;
    [self.view addSubview:payuWebView];
    
    // Back Button
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"<Back" style:UIBarButtonItemStyleBordered target:self action:@selector(home:)];
    self.navigationItem.leftBarButtonItem=newBackButton;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
        // Transaction Id
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PayuOpsTransactionId"] == (id)[NSNull null] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"PayuOpsTransactionId"] length]== 0 || [[[NSUserDefaults standardUserDefaults] objectForKey:@"PayuOpsTransactionId"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)
        txnIDStr = [self randomStringWithLength:15];
    
    else
        txnIDStr = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"PayuOpsTransactionId"]];
    
    [self setActivityIndicator];
    [self generateHashFromServer:nil withCompletionBlock:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //[self removeActivityIndicator];
            [self performSelectorOnMainThread:@selector(generatePostData) withObject:self waitUntilDone:YES];
        });
    }];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}

#pragma mark - Req Post parameters Data.
/**
 * This method is used to set Input Request For sending Payu server.
 * @Paramtypes For creating request- AMOUNT,PRODUCTINFO,FIRSTNAME,EMAIL, PHONE,FURL, SURL, USERCREDENTIAL and
  OFFERKEY
 */
-(void)generatePostData
{
    NSString *postData = [[NSString alloc]init];
    NSURL *restURL = [NSURL new];
    
    // Application PayuPG environment
    if ([NSLocalizedStringFromTableInBundle(@"application_payu_mode",@"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:ENVIRONMENT_PRODUCTION]) {
        //Application payuPG URL
        restURL=[NSURL URLWithString:PAYU_PAYMENT_PRODUCTION_URL];
    }else{
        //restURL=[NSURL URLWithString:PAYU_PAYMENT_TEST_URL];
        restURL = [NSURL URLWithString:PAYU_PAYMENT_TEST_URL1];
    }
    
    self.req=[NSMutableURLRequest requestWithURL:restURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    self.req.HTTPMethod = @"POST";
    
    NSLog(@"Non-Seamless payuDetailsdict:%@",payuDetailsdict);
    
    NSURL *sURL = [NSURL URLWithString:([payuDetailsdict objectForKey:@"sURL"] == (id)[NSNull null] || [payuDetailsdict objectForKey:@"sURL"]== 0 || [[payuDetailsdict objectForKey:@"sURL"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)?SURL:[payuDetailsdict objectForKey:@"sURL"]];
    
    NSURL *fURL = [NSURL URLWithString:([payuDetailsdict objectForKey:@"fURL"] == (id)[NSNull null] || [[payuDetailsdict objectForKey:@"fURL"] length]== 0 || [[payuDetailsdict objectForKey:@"fURL"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length ==0)?FURL:[payuDetailsdict objectForKey:@"fURL"]];

    //String convert to Hash Value, by using SHA512
   paymentHashStr = [self generateHashValueByUsingSHA512:[NSString stringWithFormat:@"%@|%@|%@|%@|||||||||||||%@",[payuDetailsdict objectForKey:@"merchantKey"],txnIDStr,(resultStr.length>0)?resultStr:AMOUNT,[payuDetailsdict objectForKey:@"product"],[payuDetailsdict objectForKey:@"saltKey"]]];
    NSLog(@"paymentHash Converted Has String :%@",paymentHashStr);
    
    //Create Post Data
    postData = [NSString stringWithFormat:@"pg=&device_type=1&txnid=%@&amount=%@&ccnum=null&ccvv=null&ccexpmon=null&bankcode=&productinfo=Wallet&key=%@&ccexpyr=null&ccname=testuser&surl=%@&hash=%@&furl=%@",txnIDStr,(resultStr.length>0)?resultStr:AMOUNT,[payuDetailsdict objectForKey:@"merchantKey"],sURL, paymentHashStr,fURL];
    NSLog(@"Non-Seamless postData:%@",postData);
    
    [self.req setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [self.req setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    payuWebView.delegate = self;
    [payuWebView loadRequest:self.req];
    //[self removeActivityIndicator];
}

-(NSString *)generateHashValueByUsingSHA512:(NSString *)string
{
    const char *cstr = [string cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:string.length];
    uint8_t digest[CC_SHA512_DIGEST_LENGTH];
    CC_SHA512(data.bytes, data.length, digest);
    NSMutableString* output = [NSMutableString  stringWithCapacity:CC_SHA512_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    return output;
}

-(NSString *) randomStringWithLength:(int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((u_int32_t)[letters length])]];
    }
    return randomString;
}
#pragma mark - Generate Hash Key.
/**
 * This method is used to set generate Hash key for Payu transcaction.
 */
- (void) generateHashFromServer:(NSDictionary *) paramDict withCompletionBlock:(urlRequestCompletionBlock)completionBlock{
    
    void(^serverResponseForHashGenerationCallback)(NSURLResponse *response, NSData *data, NSError *error) = completionBlock;
    
    NSURL *restURL = [NSURL URLWithString:@"https://payu.herokuapp.com/get_hash"];
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:restURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    theRequest.HTTPMethod = @"POST";
    
    NSOperationQueue *networkQueue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:networkQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSError *errorJson = nil;
        NSDictionary *hashDict = [NSDictionary new];
        hashDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
        paymentHashStr = [hashDict valueForKey:@"payment_hash"];
        
        serverResponseForHashGenerationCallback(response, data,connectionError);
    }];
}


#pragma mark - WebView Delegate Methods
/**
 * This method is used Web View delegate method related to payu.
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return true;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self removeActivityIndicator];
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self removeActivityIndicator];
}

#pragma mark - Navigation Bar back Button Action.
/**
 * This method is used to set Pop to Previous View Once PayMent Succeed or failure.
 */
-(void)home:(UIBarButtonItem *)sender {
    self.navigationController.navigationBarHidden = YES;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark - Default memory warning method.

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

