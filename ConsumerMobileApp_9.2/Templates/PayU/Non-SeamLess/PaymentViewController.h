//
//  PaymentViewController.h
//  Consumer Client
//
//  Created by android on 12/16/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
/**
 * This class used to handle functionality and View of PaymentViewController.
 *
 * @author Integra
 *
 */
@interface PaymentViewController : BaseViewController
{
    NSString *resultStr;
    NSMutableDictionary *payuDetailsdict;
}
/**
 * declarations are used to set the UIConstraints Of PaymentViewController.
 */
@property (strong,nonatomic) NSMutableURLRequest *request;
@property (strong,nonatomic) NSDictionary *payeeDetailsDict;
@property (strong,nonatomic) UIActivityIndicatorView *activityindicatorView;

/**
 * This method is  used for Method Initialization of PaymentViewController.
 */
// Method For initialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view  withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;

@end
