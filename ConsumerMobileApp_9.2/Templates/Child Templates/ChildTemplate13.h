//
//  ChildTemplate13.h
//  Consumer Client
//
//  Created by test on 21/10/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 * This class used to handle functionality and View of Childtemplate13
 *
 * @author Integra
 *
 */
@interface ChildTemplate13 : UIView<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *templateDictionary;
    int tagVal;
    id superId;
}
/**
 * declarations are used to set the UIConstraints Of ChildTemplate13.
 * Label and TableView.
 */
@property(nonatomic,strong) UITableView *categoryTableView;
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong) UILabel *headerTitle_Label;
@property(nonatomic,strong) UILabel *headerborder_Label;
/**
 * This method is  used for Method Initialization of ChildTemplate13.
 */
// Method For initialization.
- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView;

@end
