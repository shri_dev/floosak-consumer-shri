//
//  ChildTemplate3.h
//  Consumer Client
//
//  Created by jagadeeshk on 11/05/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PageHeaderView.h"
/**
 * This class used to handle functionality and View of Childtemplate3.
 *
 * @author Integra
 *
 */
@interface ChildTemplate3 : UIViewController<PageHeaderViewButtonDelegate,PageHeaderViewRightButtonDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIGestureRecognizerDelegate>
{
    NSArray *dropdownList_ItemsArray;
    NSString *propertyFileName;
    BOOL hasSelectionOperation;
    
    int selectedValueInteger;
    
    NSString *typeString;
    int selectedCount;
    int nextY_postion;
    int labelX_position;
    int distanceY;
    
    NSString *strTitle;
    PageHeaderView *pageHeader;
    PageHeaderView *pageHeader1;
}

/**
 * declarations are used to set the UIConstraints Of ChildTemplate3.
 * Label and TableView.
 */

@property(nonatomic) SEL baseSelector;
@property(nonatomic) id basetarget;
@property(nonatomic,strong) NSArray *dropdownList_ItemsArray;
@property(nonatomic,retain) UILabel *dropdown_NameLabel;
@property(nonatomic,strong) UITableView *dropDownData_TableView;
@property(nonatomic,strong) UILabel *valueLabel;
@property(nonatomic,strong) UIImageView *content_ImageView;
@property(nonatomic,strong) UILabel *bglabel;
@property(nonatomic,strong) UISearchBar *main_searchBar;
@property(nonatomic,strong) NSMutableArray *filteredArray;
@property (nonatomic, assign) BOOL isFiltered;
/**
 * This method is  used for Method Initialization of ChildTemplate3.
 */
// Method For initialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withPropertyName:(NSString *)propertyFile hasDidSelectFunction:(BOOL)hasDidSelectFunction withDataDictionary:(NSDictionary *)dataDictionary withDataArray:(NSArray *)dataDictsArray withPIN:(NSString *)MPIN withProcessorCode:(NSString *)processorCode withType:(NSString *)type fromView:(int)view insideView :(id)superView;

@end
