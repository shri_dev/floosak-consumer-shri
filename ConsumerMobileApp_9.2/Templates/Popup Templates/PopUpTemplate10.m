//
//  PopUpTemplate10.m
//  ConsumerClient
//
//  Created by android on 20/09/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import "PopUpTemplate10.h"
#import "Constants.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import "Template.h"
#import "WebServiceConstants.h"
#import "Constants.h"
#import "NotificationConstants.h"
#import "UIView+Toast.h"
#import "PopUpTemplate2.h"
#import "DatabaseConstatants.h"
#import "WebSericeUtils.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceDataObject.h"
#import "AppDelegate.h"
#import "Localization.h"
#import "Template.h"
#import "ParentTemplate6.h"


@implementation PopUpTemplate10


@synthesize popupTemplate10Label,propertyFileName,processor_Code;
@synthesize headerLabel1,headerLabel2,headerLabel3,borderLabel,myAccountsTableView,valueLabel1,button1,alertLabel;
#pragma mark - Class Method Initialization.

- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex
{
    
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    
    if (self)
    {
        propertyFileName=propertyFile;
        [self setBackgroundColor:[UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.4f]];
        
        NSLog(@"Property File Name...%@",propertyFileName);
        localDataDictionary = [[NSMutableDictionary alloc] initWithDictionary:dataDictionary];
        NSLog(@"local Data Dictioanry...%@",localDataDictionary);
        NSString *dataValuestr =  NSLocalizedStringFromTableInBundle(@"popup_template_10_list_data_to_load_webservice_api",propertyFileName,[NSBundle mainBundle], nil);
        local_processorCode=[Template getProcessorCodeWithData:dataValuestr];
        transactionType = [Template getTransactionCodeWithData:dataValuestr];
        NSMutableDictionary *webServiceRequestInputDetails = [[NSMutableDictionary alloc] init];
        [webServiceRequestInputDetails setObject:local_processorCode forKey:PARAMETER15];
        [webServiceRequestInputDetails setObject:transactionType forKey:PARAMETER13];
        [self commonWebServiceCallWithData:webServiceRequestInputDetails andProcessorCode:local_processorCode];
        [self addControlsForView];
    }
    return self;
}

#pragma mark - Create UI For Using UI Constraints.

-(void)addControlsForView
{
    
    int x_Position = 0;
    y_Position = 0;
    datavalArray=[[NSMutableArray alloc]init];
    popupTemplate10Label = [[UILabel alloc] init];
    alertLabel=[[UILabel alloc]init];
    NSLog(@"DatAvalue Dictionary..%@",datavalArray);
    popupTemplate10Label.frame = CGRectMake(20, 50, SCREEN_WIDTH-40 , SCREEN_HEIGHT-100);
    //    NSArray *backGroundColors = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template1_background_color",propertyFileName,[NSBundle mainBundle], nil)];
    //    if (backGroundColors)
    //        popupTemplate3Label.backgroundColor = [UIColor colorWithRed:[[backGroundColors objectAtIndex:0] floatValue] green:[[backGroundColors objectAtIndex:1] floatValue] blue:[[backGroundColors objectAtIndex:2] floatValue] alpha:1.0f];
    popupTemplate10Label.backgroundColor=[UIColor whiteColor];
    [self addSubview:popupTemplate10Label];
    
   // header Label1
    if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headerLabel1=[[UILabel alloc]init];
        headerLabel1.frame=CGRectMake(40, 60, SCREEN_WIDTH-80 , 30);
        headerLabel1.textAlignment=NSTextAlignmentCenter;
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text",propertyFileName,[NSBundle mainBundle], nil)];
        if (headerStr)
            headerLabel1.text = headerStr;
  
    
        if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                headerLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text_font",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text_font",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                headerLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        x_Position=headerLabel1.frame.origin.x+headerLabel1.intrinsicContentSize.width+5;
        y_Position=headerLabel1.frame.origin.y+headerLabel1.frame.size.height+5;
        [self addSubview:headerLabel1];
    }
    // header border label
    if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_header_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        borderLabel=[[UILabel alloc]init];
        borderLabel.frame=CGRectMake(40,y_Position,SCREEN_WIDTH-80,1);
        borderLabel.backgroundColor = [UIColor blackColor];
        y_Position=borderLabel.frame.origin.y+borderLabel.frame.size.height;
        [self addSubview:borderLabel];
    }
    // header Label2
    if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        CGRect labelFrame = CGRectMake(40, y_Position, SCREEN_WIDTH-80 , 40);
        headerLabel2 = [[UILabel alloc] init];
        headerLabel2.frame=labelFrame;
        headerLabel2.textAlignment=NSTextAlignmentCenter;
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            [headerLabel2 setText:headerStr];
        
        // Tell the label to use an unlimited number of lines
        [headerLabel2 setNumberOfLines:0];
        [headerLabel2 sizeToFit];

        if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template_10_header_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                headerLabel2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_font",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_font",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                headerLabel2.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        y_Position=headerLabel2.frame.origin.y+headerLabel2.frame.size.height+10;
        [self addSubview:headerLabel2];
    }
    // header Label3
    if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_avilable_bundle_text_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        CGRect labelFrame = CGRectMake(40, y_Position, SCREEN_WIDTH-80 , 40);
        headerLabel3 = [[UILabel alloc] init];
        headerLabel3.frame=labelFrame;
        headerLabel3.textAlignment=NSTextAlignmentLeft;
        
        NSString *headerStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template_10_avilable_bundle_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (headerStr)
            [headerLabel3 setText:headerStr];
        
        // Tell the label to use an unlimited number of lines
        [headerLabel3 setNumberOfLines:0];
        [headerLabel3 sizeToFit];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_avilable_bundle_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template_10_avilable_bundle_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template_10_avilable_bundle_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                headerLabel3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_font",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template_10_current_bundle_text_font",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_avilable_bundle_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template_10_avilable_bundle_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                headerLabel3.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headerLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headerLabel3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headerLabel3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headerLabel3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        y_Position=headerLabel3.frame.origin.y+headerLabel3.frame.size.height+5;
        [self addSubview:headerLabel3];
    }
          myAccountsTableView=[[UITableView alloc]init];
//          if([datavalArray count] > 0)
             myAccountsTableView.frame= CGRectMake(popupTemplate10Label.frame.origin.x+15, y_Position, SCREEN_WIDTH-70, (popupTemplate10Label.frame.size.height-(y_Position)));
//          else
//            alertLabel.frame=CGRectMake(popupTemplate10Label.frame.origin.x+15, y_Position, SCREEN_WIDTH-70, (popupTemplate10Label.frame.size.height-(y_Position)));
    
            myAccountsTableView.delegate=self;
            myAccountsTableView.dataSource=self;
            y_Position=myAccountsTableView.frame.origin.y+myAccountsTableView.frame.size.height;
            [self addSubview:myAccountsTableView];

//                alertLabel.hidden=NO;
//                alertLabel.textAlignment=NSTextAlignmentCenter;
//                alertLabel.numberOfLines=2;
//                [alertLabel sizeToFit];
//                myAccountsTableView.hidden=YES;
//                alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template_10_recent_transactions_not_available", propertyFileName,[NSBundle mainBundle], nil)];
//                y_Position=alertLabel.frame.origin.y+alertLabel.frame.size.height;
//                [self addSubview:alertLabel];
    
//        }
//        else
//        {
//            [self removeActivityIndicator];
//            alertLabel=[[UILabel alloc]init];
//            alertLabel.frame=CGRectMake(popupTemplate10Label.frame.origin.x+15, y_Position, SCREEN_WIDTH-70, (popupTemplate10Label.frame.size.height-(y_Position)));
//            alertLabel.hidden=NO;
//            alertLabel.textAlignment=NSTextAlignmentCenter;
//            alertLabel.numberOfLines=2;
//            [alertLabel sizeToFit];
//            myAccountsTableView.hidden=YES;
//            alertLabel.text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template_10_recent_transactions_not_available", propertyFileName,[NSBundle mainBundle], nil)];
//            y_Position=alertLabel.frame.origin.y+alertLabel.frame.size.height;
//            [self addSubview:alertLabel];
//            
//            
//        }
    
    
    if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        button1.frame=CGRectMake(popupTemplate10Label.frame.origin.x+15,y_Position,SCREEN_WIDTH-70,35);
        NSArray *button1_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_back_ground_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            button1.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        NSString *buttonTitle = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonTitle)
            [button1 setTitle:buttonTitle forState:UIControlStateNormal];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button2_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                button2_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template_10_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button1_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button1_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button1_TextColor objectAtIndex:0] floatValue] green:[[button1_TextColor objectAtIndex:1] floatValue] blue:[[button1_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [button1 setExclusiveTouch:YES];
        [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:button1];
    }
}
#pragma mark - TableView dataSource And Delegate method.
/**
 * This method is used to set add Tableview (list and delegate methods) of ChildTemplate14.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [datavalArray count];
}
/**
 * This method is used to set add Tableview for ChildTemplate14.
 *@param type- Declare Listview related UI(Labels,Images etc..base on Req).
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cashin";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    int X_Position=6;
    int Y_POSITION=10;
    // Value label1
    if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_list_item_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        valueLabel1=[[UILabel alloc]init];
        valueLabel1.frame=CGRectMake(X_Position, Y_POSITION, (SCREEN_WIDTH-(X_Position*2)), 22);
        
        NSString *headerStr=NSLocalizedStringFromTableInBundle(@"popup_template_10_list_item_label_param_type",propertyFileName,[NSBundle mainBundle], nil);
        
        if (headerStr)
            valueLabel1.text=[[datavalArray objectAtIndex:indexPath.row] objectForKey:headerStr];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template_10_list_item_label_text_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *label_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"popup_template_10_list_item_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template_10_list_item_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label_TextColor)
                valueLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_list_item_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template_10_list_item_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template1_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template_10_list_item_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template_10_list_item_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *label_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label_TextColor)
                valueLabel1.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template9_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                valueLabel1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                valueLabel1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            else
                valueLabel1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        valueLabel1.numberOfLines=2;
        valueLabel1.textAlignment=NSTextAlignmentCenter;
        [valueLabel1 sizeToFit];
        X_Position=X_Position+valueLabel1.frame.size.width+valueLabel1.frame.origin.x+25;
        [cell.contentView addSubview:valueLabel1];
    }
   
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       NSString *nextTemplatePropertyFile =  NSLocalizedStringFromTableInBundle(@"popup_template_10_list_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
        NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template_10_list_next_template",propertyFileName,[NSBundle mainBundle], nil);
        [localDataDictionary addEntriesFromDictionary:[datavalArray objectAtIndex:indexPath.row]];
        
        NSString *dataVal = NSLocalizedStringFromTableInBundle(@"popup_template_10_list_data_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
        
        NSString *typeName = [NSString stringWithFormat:@"%@_typeName",dataVal];
        NSString *typeDesc = [NSString stringWithFormat:@"%@_typeDesc",dataVal];
        
        if ([[datavalArray objectAtIndex:indexPath.row] objectForKey:@"typeName"]) {
            [localDataDictionary setObject:[[datavalArray objectAtIndex:indexPath.row] objectForKey:@"typeName"] forKey:typeName];
        }
        
        if ([[datavalArray objectAtIndex:indexPath.row] objectForKey:@"typeDesc"]) {
            [localDataDictionary setObject:[[datavalArray objectAtIndex:indexPath.row] objectForKey:@"typeDesc"] forKey:typeDesc];
        }
        NSLog(@"NextTemplate is...%@,%@,%@",nextTemplate,nextTemplatePropertyFile,[localDataDictionary description]);
        Class myclass = NSClassFromString(nextTemplate);
        
        if ([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            id obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplatePropertyFile andDelegate:self withDataDictionary:localDataDictionary withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:datavalArray withSubIndex:0];
            [[[self superview] superview] addSubview:(UIView *)obj];
        }
        else if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            id obj = [[myclass alloc] initWithNibName:nextTemplate bundle:nil withSelectedIndex:0 fromView:0 withFromView:nil withPropertyFile:nextTemplatePropertyFile withProcessorCode:nil dataArray:nil dataDictionary:localDataDictionary];
            UIViewController *vc = [(UINavigationController *)self.window.rootViewController visibleViewController];
            [vc.navigationController pushViewController:obj animated:NO];
            
        }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - WebService Call method.
/*
 * This method is used to set childtemplate2 webservice calling common method.
 */
-(void)commonWebServiceCallWithData:(NSMutableDictionary *)inputDataDictionary andProcessorCode:(NSString *)processorCode
{
    [self setActivityIndicator];
    WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
    NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:processorCode withInputDataModel:inputDataDictionary]];
    
    WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
    NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
    
    WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
    [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
}

-(void)processData:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processCode
{
    [self removeActivityIndicator];
    if (webServiceDataObject.faultCode && webServiceDataObject.faultString)
    {
        [self showAlertForErrorWithMessage:webServiceDataObject.faultString andErrorCode:nil];
    }
    else
    {
        if ([local_processorCode isEqualToString:PROCESSOR_CODE_UPGRADE_BUNDLE]) {
            NSString *jsondataString=webServiceDataObject.PaymentDetails3;
            NSArray *transactionHistoryRecords = [jsondataString componentsSeparatedByString:@","];
            int noOfRecords = (int)[transactionHistoryRecords count];
            NSMutableDictionary *tempDictionary;
            for (int index = 0; index < noOfRecords; index++)
            {
                tempDictionary = [[NSMutableDictionary alloc] init];
                NSMutableArray *transactionFileds = (NSMutableArray *)[[transactionHistoryRecords objectAtIndex:index] componentsSeparatedByString:@"|"];
                if ([transactionFileds count] > 1)
                {
                    NSLog(@"transactionFileds at %d: %@",index,transactionFileds);
                    if ([transactionFileds objectAtIndex:0])
                        [tempDictionary setObject:[transactionFileds objectAtIndex:0] forKey:BUNDLE_TRANSACTION_TYPE];
                    else
                        [tempDictionary setObject:@"" forKey:BUNDLE_TRANSACTION_TYPE];
                    
                    if ([transactionFileds objectAtIndex:1])
                        [tempDictionary setObject:[transactionFileds objectAtIndex:1] forKey:BUNDLE_TRANSACTION_AMOUNT];
                    else
                        [tempDictionary setObject:@"" forKey:BUNDLE_TRANSACTION_AMOUNT];
                    
                    if ([transactionFileds objectAtIndex:2])
                        [tempDictionary setObject:[transactionFileds objectAtIndex:2] forKey:BUNDLE_TRANSACTION_VALIDITY];
                    else
                        [tempDictionary setObject:@"" forKey:BUNDLE_TRANSACTION_VALIDITY];
                    
                    if ([transactionFileds objectAtIndex:3])
                        [tempDictionary setObject:[transactionFileds objectAtIndex:3] forKey:BUNDLE_FREE_TRANSACTIONS];
                    else
                        [tempDictionary setObject:@"" forKey:BUNDLE_FREE_TRANSACTIONS];
                    
                    [datavalArray addObject:tempDictionary];
                }
                if(datavalArray.count > 0)
                {
                    [myAccountsTableView reloadData];
                    alertLabel.hidden = YES;
                    myAccountsTableView.hidden=NO;
                }
                else
                {
                    alertLabel.hidden=NO;
                    myAccountsTableView.hidden=YES;
                }
            }
        }
    }
}
#pragma mark - Xml Error Handling methods.
/*
 * This method is used to set process data error handling.
 */

-(void)errorCodeHandlingInDBWithCode:(NSString *)errorCode withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
}

-(void)errorCodeHandlingInXML:(NSString *)errorCode andMessgae:(NSString *)message withProcessorCode:(NSString *)processorCode
{
    [self removeActivityIndicator];
    [self showAlertForErrorWithMessage:message andErrorCode:errorCode];
}

-(void)errorCodeHandlingInWebServiceRunningWithErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    [self showAlertForErrorWithMessage:nil andErrorCode:errorCode];
    
    myAccountsTableView.hidden=YES;
}
/*
 * This method is used to set process data error handling show alert based on type(Ticker or popup).
 */
-(void)showAlertForErrorWithMessage:(NSString *)message andErrorCode:(NSString *)errorCode
{
    [self removeActivityIndicator];
    NSString *errorMessage = nil;
    
    if (errorCode)
        errorMessage = NSLocalizedStringFromTableInBundle(errorCode,[AppDelegate getExternalErrorMessagePropertyFile],[NSBundle mainBundle], nil);
    else
        errorMessage = message;
    
    NSString *alertview_Type=NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    
    NSLog(@"AlertView Type is...%@",alertview_Type);
    
    if([alertview_Type compare:TICKER_TEXT] == NSOrderedSame)
    {
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        [[[self superview] superview] makeToast:errorMessage duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    }
    else if ([alertview_Type compare:POPUP_TEXT] == NSOrderedSame)
    {
        PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:errorMessage withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
        [[[self superview] superview] addSubview:popup];
    }
}
/**
 * This method is used to set button1 action of popup template10.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */
-(void)button1Action:(id)sender
{
    [self removeFromSuperview];
    //    NSString *alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    //    NSString *validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    //    NSString *nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template9_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    //    NSString *nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template9_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    //    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"popup_template9_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    //    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"popup_template9_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    //
    //
    //    NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template9_button1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    //    processor_Code = [Template getProcessorCodeWithData:data];
    //    NSString *transactionType = [Template getTransactionCodeWithData:data];
    //
    //    if (transactionType) {
    //        [localDataDictionary setObject:transactionType forKey:PARAMETER13];
    //    }
    //
    //    if (processor_Code) {
    //        [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
    //    }
    //
    //    NSLog(@"Action Type is...%@",actionType);
    //    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processor_Code Dictionary:localDataDictionary contentArray:nil alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:POPUP_TEMPLATE_3];
    //    obj.selector = @selector(processData);
    //    obj.target = self;
    //    [[NSNotificationCenter defaultCenter] postNotificationName:PPT3_BUTTON1_ACTION object:obj];
}
#pragma mark - Add ctivity indicator.
/**
 * This method is used to set Add Activity indicator for View.
 */
-(void) setActivityIndicator
{
    [[[[UIApplication sharedApplication] delegate] window] addSubview:activityIndicator];
    activityIndicator.hidden = NO;
    [activityIndicator startActivityIndicator];
}
#pragma mark - remove ctivity indicator.
/**
 * This method is used to set Activity indicator Remove from View.
 */
-(void) removeActivityIndicator
{
    [activityIndicator removeFromSuperview];
    [activityIndicator stopActivityIndicator];
}
/*
 * This method is used to remove Notification.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
