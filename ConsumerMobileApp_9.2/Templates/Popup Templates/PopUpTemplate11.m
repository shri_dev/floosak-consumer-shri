//
//  PopUpTemplate11.m
//  ConsumerClient
//
//  Created by android on 20/09/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import "PopUpTemplate11.h"
#import "ValidationsClass.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Toast.h"
#import "WebServiceConstants.h"
#import "DatabaseConstatants.h"
#import "Localization.h"
#import "NotificationConstants.h"
#import "Template.h"


@implementation PopUpTemplate11

@synthesize propertyFileName;
@synthesize popupTemplateLabel,featureTitle_label;
@synthesize headingTitle_label,headingTitle_border_label;
@synthesize key_label1,value_label1,value_border_label1;
@synthesize key_label2,value_label2,value_border_label2;
@synthesize key_label3,value_label3,value_border_label3;
@synthesize key_label4,value_label4,value_border_label4;
@synthesize key_label5,value_label5,value_border_label5;
@synthesize key_label6,value_label6,value_border_label6;
@synthesize key_label7,value_label7,value_border_label7;
@synthesize key_label8,value_label8,value_border_label8;
@synthesize key_label9,value_label9,value_border_label9;
@synthesize key_label10,value_label10,value_border_label10;
@synthesize button1,button2;


#pragma mark - Class Method Initialization.

- (id)initWithFrame:(CGRect)frame withPropertyName:(NSString *)propertyFile andDelegate:(id)delegate withDataDictionary:(NSDictionary *)dataDictionary withProcessorCode:(NSString *)processorCode  withTag:(int)tagValue withTitle:(NSString *)title  andMessage:(NSString *)message withSelectedIndexPath:(int)selectedIndexPath withDataArray:(NSArray *)dataArray withSubIndex:(int)subIndex
{
   
    NSLog(@"PropertyFileName:%@",propertyFile);
    self = [super initWithFrame:frame];
    if (self)
    {
        self.tag = tagValue;
        [self setBackgroundColor:[UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.4f]];
        reselectedValue = selectedIndexPath;
        selectedIndex=selectedIndexPath;
        propertyFileName = propertyFile;
        if(dataArray && dataArray.count > 0)
        {
            localDataArray = [[NSArray alloc]initWithArray:dataArray];
            arrayCount = (int)[dataArray count];
        }
        
        localDataDictionary=[[NSMutableDictionary alloc]initWithDictionary:dataDictionary];
        NSLog(@"Data Array ..%@,%@",dataArray,[localDataDictionary description]);
        [self addControlsforView];
    }
    return self;
}

#pragma mark - Create UI For Using UI Constraints.

-(void)addControlsforView
{
    popupTemplateLabel = [[UILabel alloc] init];
    popupTemplateLabel.frame = CGRectMake(20, 30, SCREEN_WIDTH-40 , SCREEN_HEIGHT-60);
    popupTemplateLabel.backgroundColor = [UIColor whiteColor];
    [self addSubview:popupTemplateLabel];
    
    // ------------------- Header Title Label ------------------- //
    
    float headingY_position=30;
    
    if ([NSLocalizedStringFromTableInBundle(@"popup_template11_title_label_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        headingTitle_label = [[UILabel alloc] init];
        headingTitle_label.frame = CGRectMake(40, headingY_position, SCREEN_WIDTH-80,50);
        headingTitle_label.backgroundColor = [UIColor clearColor];
        NSString *localStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:BUNDLE_TRANSACTION_TYPE];
        NSString *headerStr=[NSString stringWithFormat:@"%@ %@",localStr,[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_bundle", nil)]];
        
        if (headerStr)
            headingTitle_label.text=headerStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_title_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *headingColors;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_title_label_color",propertyFileName,[NSBundle mainBundle], nil))
                headingColors = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_title_label_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                headingColors = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                headingColors = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (headingColors)
                headingTitle_label.textColor = [UIColor colorWithRed:[[headingColors objectAtIndex:0] floatValue] green:[[headingColors objectAtIndex:1] floatValue] blue:[[headingColors objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_title_label_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_title_label_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_title_label_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_title_label_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headingTitle_label.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headingTitle_label.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *headingColors ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                headingColors = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                headingColors=[ValidationsClass colorWithHexString:application_default_text_color];
            if (headingColors)
                headingTitle_label.textColor = [UIColor colorWithRed:[[headingColors objectAtIndex:0] floatValue] green:[[headingColors objectAtIndex:1] floatValue] blue:[[headingColors objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                headingTitle_label.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                headingTitle_label.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                headingTitle_label.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        headingTitle_label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:headingTitle_label];
        nextY_position = headingTitle_label.frame.origin.y+headingTitle_label.frame.size.height+1;
    }
    
    // ------------------- Border ------------------- //
//    if ([NSLocalizedStringFromTableInBundle(@"popup_template11_title_border_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
//    {
        headingTitle_border_label = [[UILabel alloc] init];
        headingTitle_border_label.frame = CGRectMake(45, nextY_position, SCREEN_WIDTH-90 , 1);
        headingTitle_border_label.backgroundColor = [UIColor blackColor];
        nextY_position=nextY_position+10;
        [self addSubview:headingTitle_border_label];
//    }
    
    // ------------------- Section name label-------------//
    xPosition=35.0;
    // Field1(Label,Value label and Border label)
    if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field1_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label1=[[UILabel alloc]init];
        key_label1.frame=CGRectMake(xPosition,nextY_position,(SCREEN_WIDTH-(xPosition*2)/2),26);
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label1",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (![labelStr isEqualToString:@"null"])
            key_label1.text=labelStr;
        
        else
            key_label1.text=application_default_no_value_available;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label1_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label1_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label1.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label1_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label1_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label1_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label1_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label1.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label1.textAlignment = NSTextAlignmentLeft;
        nextY_position=key_label1.frame.origin.y;
        [self addSubview:key_label1];
        
        value_label1=[[UILabel alloc]init];
        value_label1.frame=CGRectMake(key_label1.frame.origin.x+key_label1.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *valueStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value1_param_type",propertyFileName,[NSBundle mainBundle], nil))];
            if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) || ([valueStr isEqualToString:@"(null)"])))
                value_label1.text =valueStr;
            else
                value_label1.text=application_default_no_value_available;
            
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value1_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value1_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                value_label1.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value1_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value1_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value1_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value1_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                value_label1.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label1.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label1.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        value_label1.textAlignment=NSTextAlignmentLeft;
        nextY_position=nextY_position+key_label1.frame.size.height;
        [self addSubview:value_label1];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field1_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label1 = [[UILabel alloc] init];
            value_border_label1.frame = CGRectMake(xPosition, nextY_position+1,(SCREEN_WIDTH-(xPosition*2)),1);
            value_border_label1.backgroundColor = [UIColor blackColor];
            nextY_position = nextY_position+value_border_label1.frame.size.height;
            [self addSubview:value_border_label1];
        }
    }
    
    // Field2(Label,Value label and Border label)
    if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field2_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label2=[[UILabel alloc]init];
        key_label2.frame=CGRectMake(xPosition, nextY_position,(SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label2",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label2.text=labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label2_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label2_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label2.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label2_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label2_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label2_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label2_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                key_label2.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label2.textAlignment = NSTextAlignmentLeft;
        [self addSubview:key_label2];
        
        value_label2=[[UILabel alloc]init];
        value_label2.frame=CGRectMake(key_label2.frame.origin.x+ key_label2.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
        NSString *valueStr=[NSString stringWithFormat:@"%@",[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value2_param_type",propertyFileName,[NSBundle mainBundle], nil))]];
        
        if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)||([valueStr isEqualToString:@"(null)"])))
                value_label2.text =[NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"application_amount_format",@"GeneralSettings",[NSBundle mainBundle], nil),valueStr];
         else
                value_label2.text=application_default_no_value_available;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value2_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value2_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                value_label2.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value2_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value2_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value2_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value2_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                value_label2.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label2.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label2.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        value_label2.textAlignment=NSTextAlignmentLeft;
        nextY_position=nextY_position+key_label2.frame.size.height;
        [self addSubview:value_label2];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field2_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label2 = [[UILabel alloc] init];
            value_border_label2.frame = CGRectMake(xPosition, nextY_position+1, (SCREEN_WIDTH-(xPosition*2)),1);
            value_border_label2.backgroundColor = [UIColor blackColor];
            nextY_position = nextY_position+value_border_label2.frame.size.height;
            [self addSubview:value_border_label2];
        }
    }
    
    // Field3(Label,Value label and Border label)
    if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field3_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label3=[[UILabel alloc]init];
        key_label3.frame=CGRectMake(xPosition, nextY_position,(SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label3",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label3.text=labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label3_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label3_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label3.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label3_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label3_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label3_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label3_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                key_label3.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label3.textAlignment = NSTextAlignmentLeft;
        [self addSubview:key_label3];
        
        value_label3=[[UILabel alloc]init];
        value_label3.frame=CGRectMake(key_label3.frame.origin.x+ key_label3.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
        NSString *valueStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value3_param_type",propertyFileName,[NSBundle mainBundle], nil))];
       
            if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)||([valueStr isEqualToString:@"(null)"])))
                value_label3.text =valueStr;
            else
                value_label3.text=application_default_no_value_available;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value3_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value3_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                value_label3.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value3_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value3_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value3_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value3_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                value_label3.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label3.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        value_label3.textAlignment=NSTextAlignmentLeft;
        nextY_position=nextY_position+key_label3.frame.size.height;
        [self addSubview:value_label3];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field3_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label3 = [[UILabel alloc] init];
            value_border_label3.frame = CGRectMake(xPosition, nextY_position+1, (SCREEN_WIDTH-(xPosition*2)),1);
            value_border_label3.backgroundColor = [UIColor blackColor];
            nextY_position = nextY_position+value_border_label3.frame.size.height;
            [self addSubview:value_border_label3];
        }
    }
    
    // Field4(Label,Value label and Border label)
    if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field4_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label4=[[UILabel alloc]init];
        key_label4.frame=CGRectMake(xPosition, nextY_position,(SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label4",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label4.text=labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label4_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label4_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label4.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label4_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label4_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label4_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label4_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                key_label4.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label4.textAlignment = NSTextAlignmentLeft;
        [self addSubview:key_label4];
        
        CGRect labelFrame=CGRectMake(key_label4.frame.origin.x+ key_label4.intrinsicContentSize.width+5,nextY_position,140,26);
        value_label4=[[UILabel alloc] init];
        value_label4.frame=labelFrame;
        value_label4.textAlignment=NSTextAlignmentLeft;
        [value_label4 setPreferredMaxLayoutWidth:labelFrame.size.width];
        value_label4.lineBreakMode = NSLineBreakByWordWrapping;
        value_label4.numberOfLines =0;
        value_label4=[[UILabel alloc]init];
        value_label4.frame=CGRectMake(key_label4.frame.origin.x+ key_label4.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
        NSString *valueStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value4_param_type",propertyFileName,[NSBundle mainBundle], nil))];
        
            if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)||([valueStr isEqualToString:@"(null)"])))
               [value_label4 setText:valueStr];
            else
                value_label4.text=application_default_no_value_available;

        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value4_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value4_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                value_label4.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value4_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value4_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value4_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value4_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                value_label4.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label4.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
//        value_label4.textAlignment=NSTextAlignmentLeft;
        nextY_position=nextY_position+key_label4.frame.size.height;
        [self addSubview:value_label4];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field4_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label4 = [[UILabel alloc] init];
            value_border_label4.frame = CGRectMake(xPosition, nextY_position+1, (SCREEN_WIDTH-(xPosition*2)),1);
            value_border_label4.backgroundColor = [UIColor blackColor];
            nextY_position = nextY_position+value_border_label4.frame.size.height;
            [self addSubview:value_border_label4];
        }
    }
    
    // Field5(Label,Value label and Border label)
    if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field5_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label5=[[UILabel alloc]init];
        key_label5.frame=CGRectMake(xPosition, nextY_position,(SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label5",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label5.text=labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label5_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label5_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label5.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label5_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label5_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label5_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label5_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                key_label5.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label5.textAlignment = NSTextAlignmentLeft;
        [self addSubview:key_label5];
        
        value_label5=[[UILabel alloc]init];
        value_label5.frame=CGRectMake(key_label5.frame.origin.x+ key_label5.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
        NSString *valueStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value5_param_type",propertyFileName,[NSBundle mainBundle], nil))];
        
            if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)||([valueStr isEqualToString:@"(null)"])))
                value_label5.text =valueStr;
            else
                value_label5.text=application_default_no_value_available;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value5_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value5_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                value_label5.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value5_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value5_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value5_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value5_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                value_label5.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label5.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        value_label5.textAlignment=NSTextAlignmentLeft;
        nextY_position=nextY_position+key_label5.frame.size.height;
        [self addSubview:value_label5];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field5_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label5 = [[UILabel alloc] init];
            value_border_label5.frame = CGRectMake(xPosition, nextY_position+1, (SCREEN_WIDTH-(xPosition*2)),1);
            value_border_label5.backgroundColor = [UIColor blackColor];
            nextY_position = nextY_position+value_border_label5.frame.size.height;
            [self addSubview:value_border_label5];
        }
    }
    
    // Field6(Label,Value label and Border label)
    if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field6_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label6=[[UILabel alloc]init];
        key_label6.frame=CGRectMake(xPosition, nextY_position,(SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label6",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label6.text=labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label6_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label6_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label6.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label6_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label6_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label6_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label6_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                key_label6.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label6.textAlignment = NSTextAlignmentLeft;
        [self addSubview:key_label6];
        
        value_label6=[[UILabel alloc]init];
        value_label6.frame=CGRectMake(key_label6.frame.origin.x+ key_label6.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *valueStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value6_param_type",propertyFileName,[NSBundle mainBundle], nil))];
       
            if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)||([valueStr isEqualToString:@"(null)"])))
                value_label6.text =valueStr;
            else
                value_label6.text=application_default_no_value_available;
   
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value6_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value6_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                value_label6.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value6_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value6_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value6_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value6_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                value_label6.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label6.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        value_label6.textAlignment=NSTextAlignmentLeft;
        nextY_position=nextY_position+key_label6.frame.size.height;
        [self addSubview:value_label6];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field6_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label6 = [[UILabel alloc] init];
            value_border_label6.frame = CGRectMake(xPosition, nextY_position+1, (SCREEN_WIDTH-(xPosition*2)),1);
            value_border_label6.backgroundColor = [UIColor blackColor];
            nextY_position = nextY_position+value_border_label6.frame.size.height;
            [self addSubview:value_border_label6];
        }
    }
    
    // Field7(Label,Value label and Border label)
    if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field7_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label7=[[UILabel alloc]init];
        key_label7.frame=CGRectMake(xPosition, nextY_position,(SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label7",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label7.text=labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label7_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label7_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label7.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label7_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label7_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label7_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label7_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                key_label7.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label7.textAlignment = NSTextAlignmentLeft;
        [self addSubview:key_label7];
        
        value_label7=[[UILabel alloc]init];
        value_label7.frame=CGRectMake(key_label7.frame.origin.x+ key_label7.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *valueStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value7_param_type",propertyFileName,[NSBundle mainBundle], nil))];
       
            if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)||([valueStr isEqualToString:@"(null)"])))
                value_label7.text =valueStr;
            else
                value_label7.text=application_default_no_value_available;
      
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value7_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value7_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                value_label7.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value7_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value7_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value7_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value7_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                value_label7.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label7.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        value_label7.textAlignment=NSTextAlignmentLeft;
        nextY_position=nextY_position+key_label7.frame.size.height;
        [self addSubview:value_label7];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field7_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label7 = [[UILabel alloc] init];
            value_border_label7.frame = CGRectMake(xPosition, nextY_position+1, (SCREEN_WIDTH-(xPosition*2)),1);
            value_border_label7.backgroundColor = [UIColor blackColor];
            nextY_position = nextY_position+value_border_label7.frame.size.height;
            [self addSubview:value_border_label7];
        }
    }
    // Field8(Label,Value label and Border label)
    if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field8_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label8=[[UILabel alloc]init];
        key_label8.frame=CGRectMake(xPosition, nextY_position,(SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label8",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label8.text=labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label8_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label8_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label8.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label8_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label8_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label8_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label8_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                key_label8.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label8.textAlignment = NSTextAlignmentLeft;
        [self addSubview:key_label8];
        
        value_label8=[[UILabel alloc]init];
        value_label8.frame=CGRectMake(key_label8.frame.origin.x+ key_label8.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *valueStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value8_param_type",propertyFileName,[NSBundle mainBundle], nil))];
      
            if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)||([valueStr isEqualToString:@"(null)"])))
                value_label8.text =valueStr;
            else
                value_label8.text=application_default_no_value_available;
      
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value8_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value8_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                value_label8.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value8_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value8_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value8_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value8_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                value_label8.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label8.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        value_label8.textAlignment=NSTextAlignmentLeft;
        nextY_position=nextY_position+key_label8.frame.size.height;
        [self addSubview:value_label8];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field8_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label8 = [[UILabel alloc] init];
            value_border_label8.frame = CGRectMake(xPosition, nextY_position+1, (SCREEN_WIDTH-(xPosition*2)),1);
            value_border_label8.backgroundColor = [UIColor blackColor];
            nextY_position = nextY_position+value_border_label8.frame.size.height;
            [self addSubview:value_border_label8];
        }
    }
    // Field9(Label,Value label and Border label)
        if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field9_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            key_label9=[[UILabel alloc]init];
            key_label9.frame=CGRectMake(xPosition, nextY_position,(SCREEN_WIDTH-(xPosition*2)/2), 26);
            
            NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label9",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (labelStr)
                key_label9.text=labelStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *labelText_Color;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_label9_color",propertyFileName,[NSBundle mainBundle], nil))
                    
                    labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label9_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (labelText_Color)
                    key_label9.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_label9_style",propertyFileName,[NSBundle mainBundle], nil))
                    
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label9_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_label9_size",propertyFileName,[NSBundle mainBundle], nil))
                    
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label9_size",propertyFileName,[NSBundle mainBundle], nil);
                
                else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                NSArray *labelText_Color ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    
                    labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
                if (labelText_Color)
                    key_label9.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    key_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    key_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    key_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            key_label9.textAlignment = NSTextAlignmentLeft;
            [self addSubview:key_label9];
            
            value_label9=[[UILabel alloc]init];
            value_label9.frame=CGRectMake(key_label9.frame.origin.x+ key_label9.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
            
            NSString *valueStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value9_param_type",propertyFileName,[NSBundle mainBundle], nil))];
           
                if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)||([valueStr isEqualToString:@"(null)"])))
                    value_label9.text =valueStr;
                else
                    value_label9.text=application_default_no_value_available;
         
            if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for label TextColor.
                NSArray *labelText_Color;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_value9_color",propertyFileName,[NSBundle mainBundle], nil))
                    labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value9_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
                
                if (labelText_Color)
                    value_label9.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for label Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_value9_style",propertyFileName,[NSBundle mainBundle], nil))
                    
                    textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value9_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                
                else
                    textStyle = application_default_text_style;
                
                // Properties for label Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_value9_size",propertyFileName,[NSBundle mainBundle], nil))
                    
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value9_size",propertyFileName,[NSBundle mainBundle], nil);
                
                else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for label textcolor
                NSArray *labelText_Color ;
                if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    
                    labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
                if (labelText_Color)
                    value_label9.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
                
                
                //Default Properties for label textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
                
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for label fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
                
                else
                    fontSize = application_default_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    value_label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    value_label9.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    value_label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            value_label9.textAlignment=NSTextAlignmentLeft;
            nextY_position=nextY_position+key_label9.frame.size.height;
            [self addSubview:value_label9];
            
            if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field9_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                value_border_label9 = [[UILabel alloc] init];
                value_border_label9.frame = CGRectMake(xPosition, nextY_position+1, (SCREEN_WIDTH-(xPosition*2)),1);
                value_border_label9.backgroundColor = [UIColor blackColor];
                nextY_position = nextY_position+value_border_label9.frame.size.height;
                [self addSubview:value_border_label9];
            }
        }
    
    // Field10(Label,Value label and Border label)
    if([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field10_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        key_label10=[[UILabel alloc]init];
        key_label10.frame=CGRectMake(xPosition, nextY_position,(SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_label10",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            key_label10.text=labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_label10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label10_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label10_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                key_label10.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label10_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_label10_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label10_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label10_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                key_label10.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                key_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                key_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                key_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        key_label10.textAlignment = NSTextAlignmentLeft;
        [self addSubview:key_label10];
        
        value_label10=[[UILabel alloc]init];
        value_label10.frame=CGRectMake(key_label10.frame.origin.x+ key_label10.intrinsicContentSize.width+5, nextY_position, (SCREEN_WIDTH-(xPosition*2)/2), 26);
        
        NSString *valueStr=[[localDataArray objectAtIndex:selectedIndex] objectForKey:(NSLocalizedStringFromTableInBundle(@"popup_template11_value10_param_type",propertyFileName,[NSBundle mainBundle], nil))];
       
            if (!((valueStr == (id)[NSNull null]) || (valueStr.length == 0) || ([valueStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)||([valueStr isEqualToString:@"(null)"])))
                value_label10.text =valueStr;
            else
                value_label10.text=application_default_no_value_available;
      
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_value10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            NSArray *labelText_Color;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value10_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_value10_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                labelText_Color = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (labelText_Color)
                value_label10.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value10_style",propertyFileName,[NSBundle mainBundle], nil))
                
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_value10_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_value10_size",propertyFileName,[NSBundle mainBundle], nil))
                
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_value10_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            NSArray *labelText_Color ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                
                labelText_Color = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                labelText_Color=[ValidationsClass colorWithHexString:application_default_text_color];
            if (labelText_Color)
                value_label10.textColor = [UIColor colorWithRed:[[labelText_Color objectAtIndex:0] floatValue] green:[[labelText_Color objectAtIndex:1] floatValue] blue:[[labelText_Color objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize= NSLocalizedStringFromTableInBundle(@"popup_template11_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                value_label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                value_label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                value_label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        value_label10.textAlignment=NSTextAlignmentLeft;
        nextY_position=nextY_position+key_label10.frame.size.height;
        [self addSubview:value_label10];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_labels_field10_border_visibilty",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            value_border_label10 = [[UILabel alloc] init];
            value_border_label10.frame = CGRectMake(xPosition, nextY_position+1, (SCREEN_WIDTH-(xPosition*2)),1);
            value_border_label10.backgroundColor = [UIColor blackColor];
            nextY_position = nextY_position+value_border_label10.frame.size.height;
            [self addSubview:value_border_label10];
        }
    }
    
    //Button1
    xPosition=30;
    if (([NSLocalizedStringFromTableInBundle(@"popup_template11_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)&&([NSLocalizedStringFromTableInBundle(@"popup_template11_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame))
    {
        button1=[UIButton buttonWithType:UIButtonTypeCustom];
        button1.frame=CGRectMake(xPosition, popupTemplateLabel.frame.size.height-20, (SCREEN_WIDTH-(xPosition*2))/2, 35);
        NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonStr)
            [button1 setTitle:buttonStr forState:UIControlStateNormal];
        
        // properties For Button backgroundColor
        NSArray *button1_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template11_button1_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button1_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            button1.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_button1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *buttonTextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                buttonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                buttonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                buttonTextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (buttonTextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[buttonTextColor objectAtIndex:0] floatValue] green:[[buttonTextColor objectAtIndex:1] floatValue] blue:[[buttonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
            //Default Properties for Button textcolor
            NSArray *button_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button_TextColor objectAtIndex:0] floatValue] green:[[button_TextColor objectAtIndex:1] floatValue] blue:[[button_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [button1 setTag:1];
        [self addSubview:button1];
        
        button2=[UIButton buttonWithType:UIButtonTypeCustom];
        button2.frame=CGRectMake(xPosition+button1.frame.size.width+10, popupTemplateLabel.frame.size.height-20, ((SCREEN_WIDTH/2)-40), 35);
        
        NSString *buttonStr1=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonStr1)
            [button2 setTitle:buttonStr1 forState:UIControlStateNormal];
        
        // properties For Button backgroundColor
        NSArray *button2_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template11_button2_color",propertyFileName,[NSBundle mainBundle], nil))
            button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button2_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button2_BackgroundColor)
            button2.backgroundColor=[UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_button2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *buttonTextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                buttonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                buttonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                buttonTextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (buttonTextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[buttonTextColor objectAtIndex:0] floatValue] green:[[buttonTextColor objectAtIndex:1] floatValue] blue:[[buttonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
            //Default Properties for Button textcolor
            NSArray *button_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button_TextColor objectAtIndex:0] floatValue] green:[[button_TextColor objectAtIndex:1] floatValue] blue:[[button_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        [button2 setTag:2];
        [self addSubview:button2];
    }
    //Button2
    else if ([NSLocalizedStringFromTableInBundle(@"popup_template11_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button2=[UIButton buttonWithType:UIButtonTypeCustom];
        button2.frame=CGRectMake(xPosition, popupTemplateLabel.frame.size.height-20, ((SCREEN_WIDTH)-2*xPosition), 35);
        
        NSString *buttonStr1=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonStr1)
            [button2 setTitle:buttonStr1 forState:UIControlStateNormal];
        
        // properties For Button backgroundColor
        NSArray *button2_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template11_button2_color",propertyFileName,[NSBundle mainBundle], nil))
            button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button2_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button2_BackgroundColor)
            button2.backgroundColor=[UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_button2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *buttonTextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                buttonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                buttonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                buttonTextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (buttonTextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[buttonTextColor objectAtIndex:0] floatValue] green:[[buttonTextColor objectAtIndex:1] floatValue] blue:[[buttonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
            //Default Properties for Button textcolor
            NSArray *button_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button_TextColor objectAtIndex:0] floatValue] green:[[button_TextColor objectAtIndex:1] floatValue] blue:[[button_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        [button2 setTag:2];
        [self addSubview:button2];
    }
    //Button1
    else if ([NSLocalizedStringFromTableInBundle(@"popup_template11_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button1=[UIButton buttonWithType:UIButtonTypeCustom];
        button1.frame=CGRectMake(xPosition, popupTemplateLabel.frame.size.height-20, ((SCREEN_WIDTH)-2*xPosition), 35);
        NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (buttonStr)
            [button1 setTitle:buttonStr forState:UIControlStateNormal];
        
        // properties For Button backgroundColor
        NSArray *button1_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"popup_template11_button1_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button1_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            button1.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        if ([NSLocalizedStringFromTableInBundle(@"popup_template11_button1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *buttonTextColor;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                buttonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                buttonTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                buttonTextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (buttonTextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[buttonTextColor objectAtIndex:0] floatValue] green:[[buttonTextColor objectAtIndex:1] floatValue] blue:[[buttonTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            
            else if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
            //Default Properties for Button textcolor
            NSArray *button_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button_TextColor objectAtIndex:0] floatValue] green:[[button_TextColor objectAtIndex:1] floatValue] blue:[[button_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"popup_template11_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize= application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [button1 setTag:1];
        [self addSubview:button1];
    }
    
}

//-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
//    CGSize constraint = CGSizeMake(width , 20000.0f);
//    CGSize title_size;
//    float totalHeight;
//
//    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
//    if ([text respondsToSelector:selector]) {
//        title_size = [text boundingRectWithSize:constraint
//                                        options:NSStringDrawingUsesLineFragmentOrigin
//                                     attributes:@{ NSFontAttributeName : font }
//                                        context:nil].size;
//
//        totalHeight = ceil(title_size.height);
//    } else {
//        title_size = [text sizeWithFont:font
//                      constrainedToSize:constraint
//                          lineBreakMode:NSLineBreakByWordWrapping];
//        totalHeight = title_size.height ;
//    }
//
//    CGFloat height = MAX(totalHeight, 40.0f);
//    return height;
//}


#pragma mark - Button Action Methods.
/**
 * This method is used to set button1 action of popup template11.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */

-(void)button1Action:(id)sender
{
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template11_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template11_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"popup_template11_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"popup_template11_button1_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    
    NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template11_button1_web_service_api_tranasaction_type",propertyFileName,[NSBundle mainBundle], nil);
    NSString *processor_Code = [Template getProcessorCodeWithData:data];
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    
    if (transactionType) {
        [localDataDictionary setObject:transactionType forKey:PARAMETER13];
    }
    
    if (processor_Code) {
        [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
    }
    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:localDataArray];

    NSLog(@"localData Data dictionary..%@",localDataDictionary);
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processor_Code Dictionary:localDataDictionary contentArray:array alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:POPUP_TEMPLATE_11];
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT11_BUTTON1_ACTION object:obj];

}

-(void)button2Action:(id)sender
{
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    validation_Type = NSLocalizedStringFromTableInBundle(@"application_validation_type", @"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle(@"popup_template11_button2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplate = NSLocalizedStringFromTableInBundle(@"popup_template11_button2_next_template",propertyFileName,[NSBundle mainBundle], nil);
    NSString *actionType =  NSLocalizedStringFromTableInBundle(@"popup_template11_button2_action_type",propertyFileName,[NSBundle mainBundle], nil);
    NSString *apiParamType = NSLocalizedStringFromTableInBundle(@"popup_template11_button2_web_service_fee_parameter_name",propertyFileName,[NSBundle mainBundle], nil);
    
    NSString *data = NSLocalizedStringFromTableInBundle(@"popup_template11_button2_web_service_api_tranasaction_type",propertyFileName,[NSBundle mainBundle], nil);
    NSString *processor_Code = [Template getProcessorCodeWithData:data];
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    
    if (transactionType) {
        [localDataDictionary setObject:transactionType forKey:PARAMETER13];
    }
    
    if (processor_Code) {
        [localDataDictionary setObject:processor_Code forKey:PARAMETER15];
    }
    
    NSLog(@"localData Data dictionary..%@",localDataDictionary);
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplate nextTemplatePropertyFile:nextTemplateProperty currentTemplatePropertyFile:propertyFileName ProcessorCode:processor_Code Dictionary:localDataDictionary contentArray:nil alertType:alertview_Type ValidationType:validation_Type inClass:self withApiParameter:apiParamType withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:transactionType withCurrentClass:POPUP_TEMPLATE_11];
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PPT11_BUTTON2_ACTION object:obj];
    
}

#pragma mark - BaseView process Data method.
/**
 * This method is used to set baseviewcontroller Process data notification.
 */
-(void)processData
{
    //Process Data
}

@end
