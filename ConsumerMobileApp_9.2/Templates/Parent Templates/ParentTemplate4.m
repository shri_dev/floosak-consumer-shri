//  ParentTemplate4.m
//  Consumer Client
//
//  Created by Integra Micro on 05/05/15.
//  Copyright (c) 2015 Integra. All rights reserved.

#import "ParentTemplate4.h"
#import "Localization.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Toast.h"
#import "ParentTemplate5.h"
#import "ParentTemplate7.h"
#import "PopUpTemplate2.h"
#import "ChildTemplate3.h"
#import "PopUpTemplate11.h"
#import "ParentTemplate7.h"
#import "AppDelegate.h"
#import "Template.h"
#import "NotificationConstants.h"
#import "WebSericeUtils.h"
#import "WebServiceConstants.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceDataObject.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "Constants.h"
#import "SpeechManager.h"

@interface ParentTemplate4 ()<OptionsMenuDelegate>
@end

@implementation ParentTemplate4

@synthesize parentScrollView;
@synthesize signupstepCountLabel,signupStep_ValueLabelBoarder;

@synthesize textfieldTitle_Label1,textfield1,dropDownButton1;
@synthesize textfieldTitle_Label2,textfield2,dropDownButton2;
@synthesize textfieldTitle_Label3,textfield3,dropDownButton3;
@synthesize textfieldTitle_Label4,textfield4,dropDownButton4;
@synthesize textfieldTitle_Label5,textfield5,dropDownButton5;
@synthesize textfieldTitle_Label6,textfield6,dropDownButton6;
@synthesize textfieldTitle_Label7,textfield7,dropDownButton7;
@synthesize textfieldTitle_Label8,textfield8,dropDownButton8;
@synthesize textfieldTitle_Label9,textfield9,dropDownButton9;
@synthesize textfieldTitle_Label10,textfield10,dropDownButton10;
@synthesize textfieldTitle_Label11,textfield11,dropDownButton11;
@synthesize textfieldTitle_Label12,textfield12,dropDownButton12;
@synthesize textfieldTitle_Label13,textfield13,dropDownButton13;
@synthesize textfieldTitle_Label14,textfield14,dropDownButton14;
@synthesize textfieldTitle_Label15,textfield15,dropDownButton15;

@synthesize datePicker1;

@synthesize button1, button2, button3;

@synthesize bgImageView, logoImageView;

#pragma mark - ParentTemplate4 UIViewController
/**
 * This method is used to set implemention of ParentTemplate4.
 */

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary
{
   NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        propertyFileName = propertyFileArray;
        local_fromViewe = fromView;
        
    }
    return self;
}

#pragma mark - UIViewController life Cycle Method

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self reloadView];
    if([propertyFileName isEqualToString:@"SignupPT4"]){
        [self getGovernoratesFromWebService];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(idTypeSelected:) name:@"IDTypeSelected" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateGovernorates:) name:@"UpdateGovernorates" object:nil];
    }
}
/**
 * This method is used to add view For Displaying Signup count Label.
 *@param type - SignUp Doc count Updated label.
 */
-(void) reloadView
{
    selfRegisDataDict = [[NSMutableDictionary alloc] init];
    
    nextTemplateProperty = nil;
    nextTemplate = nil;
    
    // Parent Background Color.
    NSArray *viewBackgroundColor;
    
    if (NSLocalizedStringFromTableInBundle(@"parent_template4_background_color",propertyFileName,[NSBundle mainBundle], nil)) {
        viewBackgroundColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_background_color",propertyFileName,[NSBundle mainBundle], nil)];
    }
    else{
        viewBackgroundColor= [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"application_default_backgorund_color",@"GeneralSettings",[NSBundle mainBundle], nil)];
    }
    self.view.backgroundColor=[UIColor colorWithRed:[[viewBackgroundColor objectAtIndex:0] floatValue] green:[[viewBackgroundColor objectAtIndex:1] floatValue] blue:[[viewBackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
    
    
    numberofFields=0;
    bgLbl = [[UILabel alloc] init];
    bgLbl.frame=CGRectMake(self.view.frame.origin.x, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH,SCREEN_HEIGHT);
    [bgLbl setBackgroundColor:[UIColor blackColor]];
    bgLbl.alpha=0.0;
    [self.view addSubview:bgLbl];
    
    //PageHeader
    
    NSString *imageNameStr;
    NSString *labelStr;
    NSArray *titleLabelTextColorArr;
    NSString *textStyle;
    NSString *fontSize;
    // Title bar Image.
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_image_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        imageNameStr=HEADER_IMAGE_NAME;
    }
    // TitleBar name.
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
        
        labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                titleLabelTextColorArr = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                titleLabelTextColorArr = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColorArr = [ValidationsClass colorWithHexString:application_default_text_color];
            
            // Properties for label Textstyle.
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
        }
        else
        {
            //Default Properties for label textcolor
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                titleLabelTextColorArr = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColorArr=[ValidationsClass colorWithHexString:application_default_text_color];
            
            //Default Properties for label textStyle
            
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_titlebar_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
        }
    }
    
    // To set Add pageHeader For ParentTemplate4.
    pageHeader = [[PageHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64 ) withHeaderTitle:labelStr?labelStr:@""  withLeftbarBtn1Image_IconName:MENU_IMAGE_NAME withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:imageNameStr  withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    
    if (titleLabelTextColorArr)
        pageHeader.header_titleLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColorArr objectAtIndex:0] floatValue] green:[[titleLabelTextColorArr objectAtIndex:1] floatValue] blue:[[titleLabelTextColorArr objectAtIndex:2] floatValue] alpha:1.0f];
    
    if ([textStyle isEqualToString:TEXT_STYLE_0])
       pageHeader.header_titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_1])
        pageHeader.header_titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
    
    else if ([textStyle isEqualToString:TEXT_STYLE_2])
        pageHeader.header_titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
    
    else
        pageHeader.header_titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
    
    [self.view addSubview:pageHeader];

    validationsArray = [[NSMutableArray alloc] init];
    
    label_X_Position = 10.0;
    label_Y_Position = 5.0;
    distance_Y = 5.0;
    filed_X_Position = 5.0;
    filed_Y_Position = 5.0;
    next_Y_Position = 0;
    
    borderColors = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_border_color",propertyFileName,[NSBundle mainBundle], nil)];
    
    parentScrollView = [[UIScrollView alloc] init];
    
    /*
     * This method is used to add ParentTemplate4 ToolBar (Cancel,Ok buttons).
     */
    numberToolbar = [[UIToolbar alloc] init];
    numberToolbar.frame=CGRectMake(0, 0, SCREEN_WIDTH, 40);
    numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:
    [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] style:UIBarButtonItemStyleDone target:self  action:@selector(cancelNumberPad)],[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_ok_button", nil)]] style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    NSString *selectedIconBtncolors =application_branding_color_theme;
    NSArray *selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:selectedIconBtncolors];
    numberToolbar.tintColor = [UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
    
    scroll_Y_Position = 0;
    startY_Position = pageHeader.frame.size.height+5;
    
    scroll_Y_Position = startY_Position;
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_title_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        scroll_Y_Position = 36;
        
        signupstepCountLabel=[[UILabel alloc] init];
        signupstepCountLabel.frame=CGRectMake(15, startY_Position, SCREEN_WIDTH-(label_X_Position*2), 35);
        signupstepCountLabel.backgroundColor=[UIColor clearColor];
        signupstepCountLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        signupstepCountLabel.textAlignment=NSTextAlignmentCenter;
        
        //HeaderLabel Tag
        [signupstepCountLabel setTag:999];
        
        //HeaderLabel Tag
        NSString *signUpStr;
        NSString *tempString = [Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_title_text",propertyFileName,[NSBundle mainBundle], nil)];
        if ([tempString rangeOfString:@"{0}"].location != NSNotFound) {
            signUpStr =[Template formatMessageWithMessage:tempString withReferenceValues:[NSString stringWithFormat:@"%d|%d",0,(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"totalDocuments"]]];
        }
        else
            signUpStr = tempString;
        
        if (signUpStr)
            signupstepCountLabel.text=signUpStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_title_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *titleLabelTextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_title_text_color",propertyFileName,[NSBundle mainBundle], nil))
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_title_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (titleLabelTextColor)
                signupstepCountLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColor objectAtIndex:0] floatValue] green:[[titleLabelTextColor objectAtIndex:1] floatValue] blue:[[titleLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_title_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_title_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_title_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_title_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                signupstepCountLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                signupstepCountLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                signupstepCountLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                signupstepCountLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *titleLabelTextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                titleLabelTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                titleLabelTextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (titleLabelTextColor)
                signupstepCountLabel.textColor = [UIColor colorWithRed:[[titleLabelTextColor objectAtIndex:0] floatValue] green:[[titleLabelTextColor objectAtIndex:1] floatValue] blue:[[titleLabelTextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_headertitle_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                signupstepCountLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                signupstepCountLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                signupstepCountLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                signupstepCountLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [self.view addSubview:signupstepCountLabel];
        
        // Signup Step Border Label
        label_Y_Position=signupstepCountLabel.frame.origin.y+signupstepCountLabel.frame.size.height+4.0;
        scroll_Y_Position = startY_Position+signupstepCountLabel.frame.size.height+10;
    }
    
  if([NSLocalizedStringFromTableInBundle(@"parent_template4_button3_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){ //KCK Coding
    bgImageView = [[UIImageView alloc]init];
    bgImageView.frame=CGRectMake(self.view.frame.origin.x, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH,SCREEN_HEIGHT);
    bgImageView.image = [UIImage imageNamed:@"splashbg.png"];
    [self.view addSubview:bgImageView];
    [self.view bringSubviewToFront:bgImageView];
    
    logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 0, SCREEN_WIDTH-50, SCREEN_HEIGHT-350)];
    logoImageView.image = [UIImage imageNamed:@"logo.png"];
    [bgImageView addSubview:logoImageView];
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(20, SCREEN_HEIGHT-310, SCREEN_WIDTH-40, 400)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    
    parentScrollView.frame = CGRectMake(label_X_Position*2, SCREEN_HEIGHT-300, SCREEN_WIDTH-label_X_Position*4 , 200);
    
    //KCK Coding
    
    //        parentScrollView.frame = CGRectMake(label_X_Position, scroll_Y_Position, SCREEN_WIDTH-16 , SCREEN_HEIGHT-250);
    
  } else {
    
    bgImageView = [[UIImageView alloc]init];
    bgImageView.frame=CGRectMake(self.view.frame.origin.x, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH,SCREEN_HEIGHT);
    bgImageView.image = [UIImage imageNamed:@"splashbg.png"];
    [self.view addSubview:bgImageView];
    
    logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-(SCREEN_WIDTH/3)/2, scroll_Y_Position-30, SCREEN_WIDTH/3, SCREEN_WIDTH/3)];
    logoImageView.image = [UIImage imageNamed:@"logo.png"];
    [self.view addSubview:logoImageView];
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(20, 200, SCREEN_WIDTH-40, SCREEN_HEIGHT-200)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgView];
    
    parentScrollView.frame = CGRectMake(label_X_Position*2, 200, SCREEN_WIDTH-(label_X_Position*4) , SCREEN_HEIGHT-270);
    
    //    parentScrollView.frame = CGRectMake(label_X_Position, scroll_Y_Position, SCREEN_WIDTH-16 , SCREEN_HEIGHT-200);
  }
        //parentScrollView.frame = CGRectMake(label_X_Position, scroll_Y_Position, SCREEN_WIDTH-16 , SCREEN_HEIGHT-250);
    //else
        //parentScrollView.frame = CGRectMake(label_X_Position, scroll_Y_Position, SCREEN_WIDTH-16 , SCREEN_HEIGHT-200);
    
    parentScrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:parentScrollView];
    
    inputField_X_Position = 5.0;
    inputField_Y_Position = 5.0;
    
    [self addControles];
    
    activityIndicator = [[ActivityIndicator alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:activityIndicator];
    activityIndicator.hidden = YES;
}


/**
 * This method is used to set add UIconstraints Frame of ParentTemplate4.
 @param Type- Label,Text field,drop down and Button
 * Set Text (Size,color and font size).
 */
-(void)addControles
{
    isDebugging = NO;
    NSLog(@"adding controles..");
    int localTag = 100;
    
    //To Know Application Mode  0- SIngle User 1 - Multi User
    //To get Locally saved Phone Number.
    userApplicationMode = (int)[NSLocalizedStringFromTableInBundle(@"application_default_user_support_mode",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue];
    phoneNumberStr=[[NSUserDefaults standardUserDefaults] objectForKey:ACTIVATIONMOBILENUMBER];
    multiUserPhnStr=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
    
    // Field1 FirstName
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        // Label1
        textfieldTitle_Label1 = [[UILabel alloc] init];
        textfieldTitle_Label1.frame = CGRectMake(inputField_X_Position, inputField_Y_Position, parentScrollView.frame.size.width-10, 25);
        textfieldTitle_Label1.backgroundColor = [UIColor clearColor];
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label1_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
             textfieldTitle_Label1.text =labelStr;
        
         textfieldTitle_Label1.textAlignment = NSTextAlignmentLeft;
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            //Properties for label TextColor
            
            NSArray *label1_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label1_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label1_TextColor)
                textfieldTitle_Label1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label1_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label1_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label1_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label1_TextColor)
                textfieldTitle_Label1.textColor = [UIColor colorWithRed:[[label1_TextColor objectAtIndex:0] floatValue] green:[[label1_TextColor objectAtIndex:1] floatValue] blue:[[label1_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        [parentScrollView addSubview:textfieldTitle_Label1];
        
        // TextField1
        filed_Y_Position = textfieldTitle_Label1.frame.origin.y+textfieldTitle_Label1.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield1 = [[CustomTextField alloc] init];
            textfield1.autocorrectionType = UITextAutocorrectionTypeNo;
            textfield1.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
            [textfield1 setBorderStyle:UITextBorderStyleNone];
           [self SetTextFieldBorder:textfield1];
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield1.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([textfield1 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    if (NSLocalizedStringFromTableInBundle(@"parent_template4_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value1_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield1.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField1TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField1TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField1TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField1TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField1TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField1TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField1TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField1TextColor objectAtIndex:0] floatValue] green:[[textField1TextColor objectAtIndex:1] floatValue] blue:[[textField1TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield1.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield1.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                   textfield1.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield1.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            // property for KeyBoardType
            [textfield1 setTag:localTag];
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield1.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield1.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield1.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield1.secureTextEntry=YES;
                textfield1.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield1.inputView = datePicker1;
                datePicker1.tag = textfield1.tag;
            }
            
            else
                textfield1.keyboardType = UIKeyboardTypeDefault;

            textfield1.inputAccessoryView = numberToolbar;
            textfield1.delegate = self;
            //localTag++;
            next_Y_Position = textfieldTitle_Label1.frame.origin.y+textfield1.frame.origin.y+textfield1.frame.size.height;
            if(isDebugging){
                if(isDebugging){
                    NSLog(@"debuging..");
//                    int lowerBound = 111111;
//                    int upperBound = 999999;
//                    int num = lowerBound + arc4random() % (upperBound - lowerBound);
//                    NSString *phoneStr = [NSString stringWithFormat:@"777%d", num];
//                    [textfield1 setText:phoneStr];
//                    [textfield1 setText:@"700777015"];
                    [textfield1 setText:@"700987091"];
//                    [textfield1 setText:@"967777888999"];
                }
            }
            [parentScrollView addSubview:textfield1];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value1_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label1 text] forKey:@"labelName"];

            if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value1_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value1_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
                 textfield1.text=phoneNumberStr;
                 textfield1.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield1.text forKey:@"value"];
            }
            else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value1_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
            {
                textfield1.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                textfield1.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield1.text forKey:@"value"];
            }
            else
            {
                [tempDictionary setObject:[textfield1 text] forKey:@"value"];
                [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value1_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value1_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value1_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value1_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            }
        }
        // DropDown 1
        else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_visibility", propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton1 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton1 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton1 setTitleEdgeInsets:titleInsets];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_hint",propertyFileName,[NSBundle mainBundle], nil)];
            if (dropDownStr) {
                [dropDownButton1 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton1 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton1.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton1 setTag:1];
            if([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton1 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton1.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton1.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton1.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else{
                
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton1.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton1.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton1.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton1.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [dropDownButton1 setExclusiveTouch:YES];
            dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
               
                 UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton1.frame.size.width-dropDownButton1.intrinsicContentSize.width-8.0));
                [dropDownButton1 setTitleEdgeInsets:titleInsets];
                dropDownButton1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton1 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
            [parentScrollView addSubview:dropDownButton1];
            
            [[dropDownButton1 layer] setBorderWidth:0.5f];
            [[dropDownButton1 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton1.frame.size.width-28,filed_Y_Position+14,22,11);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            next_Y_Position = textfieldTitle_Label1.frame.origin.y+dropDownButton1.frame.origin.y+dropDownButton1.frame.size.height;
            [parentScrollView addSubview:imageView];
        
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value1_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label1 text] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton1.titleLabel.text forKey:@"value"];
        }
        
        numberofFields++;
        [validationsArray addObject:tempDictionary];
    }
    localTag++;
    //Field2 Father's Name
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        //Label2
        textfieldTitle_Label2 = [[UILabel alloc] init];
        textfieldTitle_Label2.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
        textfieldTitle_Label2.backgroundColor = [UIColor clearColor];
        textfieldTitle_Label2.lineBreakMode = NSLineBreakByTruncatingTail;
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label2_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label2.text = labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label2_font_attributes_override", propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label2_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label2_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label2_TextColor)
                textfieldTitle_Label2.textColor = [UIColor colorWithRed:[[label2_TextColor objectAtIndex:0] floatValue] green:[[label2_TextColor objectAtIndex:1] floatValue] blue:[[label2_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label2.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label2.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label2.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label2_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label2_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label2_TextColor)
                textfieldTitle_Label2.textColor = [UIColor colorWithRed:[[label2_TextColor objectAtIndex:0] floatValue] green:[[label2_TextColor objectAtIndex:1] floatValue] blue:[[label2_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label2.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label2.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label2.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [parentScrollView addSubview:textfieldTitle_Label2];
        filed_Y_Position = textfieldTitle_Label2.frame.origin.y+textfieldTitle_Label2.frame.size.height+distance_Y;
        
        //TextField2
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield2 = [[CustomTextField alloc] init];
            textfield2.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
            [textfield2 setBorderStyle:UITextBorderStyleNone];
            [self SetTextFieldBorder:textfield2];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield2.placeholder = valueStr ;

            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield2 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"parent_template4_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value2_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield2.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
            
                // Properties for textField TextColor.
                
                NSArray *textField2TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField2TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField2TextColor)
                    textfield2.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield2.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield2.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield2.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                //Default Properties for textfiled textcolor
                
                NSArray *textField2TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField2TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField2TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField2TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField2TextColor objectAtIndex:0] floatValue] green:[[textField2TextColor objectAtIndex:1] floatValue] blue:[[textField2TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield2.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield2.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield2.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield2.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            // property for KeyBoardType
            NSString *keyboardType;
            [textfield2 setTag:localTag];
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value2_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield2.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield2.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield2.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield2.secureTextEntry=YES;
                textfield2.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield2.inputView = datePicker1;
                datePicker1.tag = textfield2.tag;
            }
            
            else
                textfield2.keyboardType = UIKeyboardTypeDefault;
            
            textfield2.inputAccessoryView = numberToolbar;
            textfield2.delegate = self;
            //localTag++;
            if(isDebugging){
                [textfield2 setText:@"1111"];
            }
            
//
            if([propertyFileName isEqualToString:@"ActivationPT4"]){
//                textfield2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textfield2.placeholder attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:6.0]}];
            }
            [parentScrollView addSubview:textfield2];
            
            next_Y_Position = textfield2.frame.origin.y+textfield2.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value2_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label2 text] forKey:@"labelName"];
            
            if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value2_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value2_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
                textfield2.text=phoneNumberStr;
                textfield2.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield2.text forKey:@"value"];
            }
            else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value2_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
            {
                textfield2.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                textfield2.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield2.text forKey:@"value"];
            }
            else
            {
                [tempDictionary setObject:[textfield2 text] forKey:@"value"];
                [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value2_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value2_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value2_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value2_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            }
        }
        
        //DropDown2
        else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton2 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton2 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton2 setTitleEdgeInsets:titleInsets];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton2 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton2 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton2.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton2 setTag:2];
//            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton2 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton2.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton2.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton2.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton2.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                   dropDownButton2.titleLabel.font= [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton2.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton2.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            
            [dropDownButton2 setExclusiveTouch:YES];
            dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                 UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton2.frame.size.width-dropDownButton2.intrinsicContentSize.width-8.0));
                [dropDownButton2 setTitleEdgeInsets:titleInsets];
                dropDownButton2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton2 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
            [parentScrollView addSubview:dropDownButton2];
            
            [[dropDownButton2 layer] setBorderWidth:0.5f];
            [[dropDownButton2 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton2.frame.size.width-28,filed_Y_Position+14,22,11);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [parentScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton2.frame.origin.y+dropDownButton2.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value2_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label2 text] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton2.titleLabel.text forKey:@"value"];
        }
        
        numberofFields++;
        [validationsArray addObject:tempDictionary];
    }
    localTag++;
    //Field3
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label3 = [[UILabel alloc] init];
        textfieldTitle_Label3.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
        textfieldTitle_Label3.backgroundColor = [UIColor clearColor];
        textfieldTitle_Label3.lineBreakMode = NSLineBreakByTruncatingTail;
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label3_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label3.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label3_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label3_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label3_TextColor)
                textfieldTitle_Label3.textColor = [UIColor colorWithRed:[[label3_TextColor objectAtIndex:0] floatValue] green:[[label3_TextColor objectAtIndex:1] floatValue] blue:[[label3_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label3_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label3_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label3.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label3.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label3_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label3_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label3_TextColor)
                textfieldTitle_Label3.textColor = [UIColor colorWithRed:[[label3_TextColor objectAtIndex:0] floatValue] green:[[label3_TextColor objectAtIndex:1] floatValue] blue:[[label3_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label3.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label3.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        [parentScrollView addSubview:textfieldTitle_Label3];
        
        //TextField3
        filed_Y_Position = textfieldTitle_Label3.frame.origin.y+textfieldTitle_Label3.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield3 = [[CustomTextField alloc] init];
            textfield3.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
            [textfield3 setBorderStyle:UITextBorderStyleNone];
            [self SetTextFieldBorder:textfield3];
          
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            if (valueStr)
                textfield3.placeholder =valueStr ;
            
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                
                //Properties for textfield Hint text color
                
                if ([textfield3 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"parent_template4_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value3_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield3.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField3TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField3TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField3TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField3TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField3TextColor)
                    textfield3.textColor = [UIColor colorWithRed:[[textField3TextColor objectAtIndex:0] floatValue] green:[[textField3TextColor objectAtIndex:1] floatValue] blue:[[textField3TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield3.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield3.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField3TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField3TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField3TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField3TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField3TextColor objectAtIndex:0] floatValue] green:[[textField3TextColor objectAtIndex:1] floatValue] blue:[[textField3TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield3.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield3.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield3.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield3.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            // property for KeyBoardType
            [textfield3 setTag:localTag];
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value3_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield3.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield3.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield3.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield3.secureTextEntry=YES;
                textfield3.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield3.inputView = datePicker1;
                datePicker1.tag = textfield3.tag;
            }
            
            else
                textfield3.keyboardType = UIKeyboardTypeDefault;
            
            textfield3.inputAccessoryView = numberToolbar;
            textfield3.delegate = self;
            //localTag++;
            if(isDebugging){
                [textfield3 setText:@"sample fathername"];
            }
            [parentScrollView addSubview:textfield3];
            next_Y_Position = textfield3.frame.origin.y+textfield3.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label3 text] forKey:@"labelName"];
            
            if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
                textfield3.text=phoneNumberStr;
                textfield3.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield3.text forKey:@"value"];
            }
            else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
            {
                textfield3.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                textfield3.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield3.text forKey:@"value"];
            }
            else
            {
                [tempDictionary setObject:[textfield3 text] forKey:@"value"];
                [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            }
            
        }
        
        //DropDown3
        else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton3 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton3 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton3 setTitleEdgeInsets:titleInsets];
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton3 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton3 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton3.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton3 setTag:3];
//            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton3 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton3.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton3.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton3.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton3.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton3.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton3.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton3.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            
            [dropDownButton3 setExclusiveTouch:YES];
            dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                 UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton3.frame.size.width-dropDownButton3.intrinsicContentSize.width-8.0));
                [dropDownButton3 setTitleEdgeInsets:titleInsets];
                dropDownButton3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton3 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
            [parentScrollView addSubview:dropDownButton3];
            
            [[dropDownButton3 layer] setBorderWidth:0.5f];
            [[dropDownButton3 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton3.frame.size.width-28,filed_Y_Position+14,22,11);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [parentScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton3.frame.origin.y+dropDownButton3.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value3_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label3 text] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton3.titleLabel.text forKey:@"value"];
        }
        
        numberofFields++;
        [validationsArray addObject:tempDictionary];
    }
    localTag++;
    // Field 4
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        
        //Label4
        textfieldTitle_Label4 = [[UILabel alloc] init];
        textfieldTitle_Label4.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
        textfieldTitle_Label4.backgroundColor = [UIColor clearColor];
        textfieldTitle_Label4.lineBreakMode = NSLineBreakByTruncatingTail;
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label4_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label4.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label4_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label4_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label4_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label4_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label4_TextColor)
                textfieldTitle_Label4.textColor = [UIColor colorWithRed:[[label4_TextColor objectAtIndex:0] floatValue] green:[[label4_TextColor objectAtIndex:1] floatValue] blue:[[label4_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label4_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label4_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label4.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label4.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label4_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label4_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label4_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label4_TextColor)
                textfieldTitle_Label4.textColor = [UIColor colorWithRed:[[label4_TextColor objectAtIndex:0] floatValue] green:[[label4_TextColor objectAtIndex:1] floatValue] blue:[[label4_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label4.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label4.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        [parentScrollView addSubview:textfieldTitle_Label4];
        filed_Y_Position = textfieldTitle_Label4.frame.origin.y+textfieldTitle_Label4.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            //TextField4
            textfield4 = [[CustomTextField alloc] init];
            textfield4.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
            [textfield4 setBorderStyle:UITextBorderStyleNone];
            [self SetTextFieldBorder:textfield4];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield4.placeholder =valueStr ;
            
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                               //Properties for textfield Hint text color
                
                if ([textfield4 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textField4HintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"parent_template4_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textField4HintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textField4HintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value4_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textField4HintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textField4HintColor)
                        textfield4.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textField4HintColor objectAtIndex:0] floatValue] green:[[textField4HintColor objectAtIndex:1] floatValue] blue:[[textField4HintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField4TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField4TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField4TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField4TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField4TextColor)
                    textfield4.textColor = [UIColor colorWithRed:[[textField4TextColor objectAtIndex:0] floatValue] green:[[textField4TextColor objectAtIndex:1] floatValue] blue:[[textField4TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield4.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield4.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField4TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField4TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField4TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField4TextColor)
                    textfield4.textColor = [UIColor colorWithRed:[[textField4TextColor objectAtIndex:0] floatValue] green:[[textField4TextColor objectAtIndex:1] floatValue] blue:[[textField4TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield4.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield4.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield4.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield4.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [textfield4 setTag:localTag];
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
            {
                keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value4_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            }
            else
            {
                keyboardType = application_default_value_keyboard_type;
            }
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield4.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield4.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield4.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield4.secureTextEntry=YES;
                textfield4.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield4.inputView = datePicker1;
                datePicker1.tag = textfield4.tag;
              //KCK Coding
              UIImageView *imageView = [[UIImageView alloc] init];
              imageView.frame=CGRectMake(textfield4.frame.size.width-33,7,25,25);
              imageView.image = [UIImage imageNamed:@"calender.png"];
              [textfield4 addSubview:imageView];
            }
            
            else
                textfield4.keyboardType = UIKeyboardTypeDefault;
            
            textfield4.inputAccessoryView = numberToolbar;
            textfield4.delegate = self;
            //localTag++;
            if(isDebugging){
                [textfield4 setText:@"sample grandfathername"];
            }
            [parentScrollView addSubview:textfield4];
            
            next_Y_Position = textfield4.frame.origin.y+textfield4.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value4_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label4 text] forKey:@"labelName"];
            
            if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value4_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value4_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
                textfield4.text=phoneNumberStr;
                textfield4.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield4.text forKey:@"value"];
            }
            else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value4_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
            {
                textfield4.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                textfield4.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield4.text forKey:@"value"];
            }
            else
            {
                 [tempDictionary setObject:[textfield4 text] forKey:@"value"];
                 [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value4_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value4_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value4_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value4_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            }
            
        }
        else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton4 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton4 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
          [dropDownButton4 setBackgroundColor:[UIColor clearColor]];
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton4 setTitleEdgeInsets:titleInsets];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton4 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton4 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton4.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton4 setTag:4];
//            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton4 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton4.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton4.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton4.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton4.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton4.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton4.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton4.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton4.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [dropDownButton4 setExclusiveTouch:YES];
            dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                 UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton4.frame.size.width-dropDownButton4.intrinsicContentSize.width-8.0));
                [dropDownButton4 setTitleEdgeInsets:titleInsets];
                dropDownButton4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton4 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
            [parentScrollView addSubview:dropDownButton4];
            
//            [[dropDownButton4 layer] setBorderWidth:0.5f];
//            [[dropDownButton4 layer] setBorderColor:[UIColor blackColor].CGColor];
//
//            UIImageView *imageView = [[UIImageView alloc] init];
//            imageView.frame=CGRectMake(dropDownButton4.frame.size.width-28,filed_Y_Position+14,22,11);
//            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
//            [parentScrollView addSubview:imageView];

          UIImageView *imageView = [[UIImageView alloc] init];
          imageView.frame=CGRectMake(textfield4.frame.size.width-70,6,25,25);
          imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
          [textfield4 addSubview:imageView];
          
            next_Y_Position = dropDownButton4.frame.origin.y+dropDownButton4.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value4_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label4 text] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton4.titleLabel.text forKey:@"value"];
        }
        [dropDownButton4 setBackgroundColor:[UIColor clearColor]];
        numberofFields++;
        [validationsArray addObject:tempDictionary];
    }
    localTag++;
    //Field5
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
      textfieldTitle_Label5 = [[UILabel alloc] init];
      textfieldTitle_Label5.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
      textfieldTitle_Label5.backgroundColor = [UIColor clearColor];
      textfieldTitle_Label5.lineBreakMode = NSLineBreakByTruncatingTail;
      
      NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label5_text",propertyFileName,[NSBundle mainBundle], nil)];
      
      if (labelStr)
        textfieldTitle_Label5.text =labelStr;
      
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        // Properties for label TextColor.
        
        NSArray *label5_TextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_label5_text_color",propertyFileName,[NSBundle mainBundle], nil))
          label5_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
          label5_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          label5_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];

//          if([propertyFileName isEqualToString:@"SignupPT4"]){
//              label5_TextColor = [ValidationsClass colorWithHexString:@"#f79429"];
//          }
        
        if (label5_TextColor)
          textfieldTitle_Label5.textColor = [UIColor colorWithRed:[[label5_TextColor objectAtIndex:0] floatValue] green:[[label5_TextColor objectAtIndex:1] floatValue] blue:[[label5_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        // Properties for label Textstyle.
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_label5_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label5_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_text_style;
        
        
        // Properties for label Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_label5_text_style",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label5_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfieldTitle_Label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfieldTitle_Label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfieldTitle_Label5.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfieldTitle_Label5.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        
      }
      else
      {
        //Default Properties for label textcolor
        
        NSArray *label4_TextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
          label4_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          label4_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
        if (label4_TextColor)
          textfieldTitle_Label5.textColor = [UIColor colorWithRed:97/255.0 green:97/255.0 blue:97/255.0 alpha:1.0f];
        
        //Default Properties for label textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_text_style;
        
        //Default Properties for label fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfieldTitle_Label5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfieldTitle_Label5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfieldTitle_Label5.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfieldTitle_Label5.font= [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      
      [parentScrollView addSubview:textfieldTitle_Label5];
      filed_Y_Position = textfieldTitle_Label5.frame.origin.y+textfieldTitle_Label5.frame.size.height+distance_Y;
      
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        textfield5 = [[CustomTextField alloc] init];
        textfield5.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
        //[textfield5 setBorderStyle:UITextBorderStyleBezel];
        [self SetTextFieldBorder:textfield5];
        
        NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (valueStr)
          textfield5.placeholder =valueStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
          //Properties for textfield Hint text color
          
          if ([textfield5 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
            NSArray *textField5HintColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil))
              textField5HintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
              textField5HintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value5_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
              textField5HintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
            
            if (textField5HintColor)
              textfield5.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textField5HintColor objectAtIndex:0] floatValue] green:[[textField5HintColor objectAtIndex:1] floatValue] blue:[[textField5HintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
          }
          
          // Properties for textField TextColor.
          
          NSArray *textField5TextColor;
          
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
            textField5TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
            textField5TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else
            textField5TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
          
          if (textField5TextColor)
            textfield5.textColor = [UIColor colorWithRed:[[textField5TextColor objectAtIndex:0] floatValue] green:[[textField5TextColor objectAtIndex:1] floatValue] blue:[[textField5TextColor objectAtIndex:2] floatValue] alpha:1.0f];
          
          // Properties for textField Textstyle.
          
          NSString *textStyle;
          if(NSLocalizedStringFromTableInBundle(@"parent_template4_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
          else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
          else
            textStyle = application_default_value_text_style;
          
          
          // Properties for textField Font size.
          
          NSString *fontSize;
          if(NSLocalizedStringFromTableInBundle(@"parent_template4_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
          else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
          else
            fontSize = application_default_value_text_size;
          
          if ([textStyle isEqualToString:TEXT_STYLE_0])
            textfield5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_1])
            textfield5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_2])
            textfield5.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
          
          else
            textfield5.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
          //Default Properties for textfiled textcolor
          
          NSArray *textField5TextColor ;
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
            textField5TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
          else
            textField5TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
          if (textField5TextColor)
            textfield5.textColor = [UIColor colorWithRed:[[textField5TextColor objectAtIndex:0] floatValue] green:[[textField5TextColor objectAtIndex:1] floatValue] blue:[[textField5TextColor objectAtIndex:2] floatValue] alpha:1.0f];
          
          //Default Properties for textfiled textStyle
          
          NSString *textStyle;
          if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
          else
            textStyle = application_default_text_style;
          
          //Default Properties for textfiled fontSize
          
          NSString *fontSize;
          if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
          else
            fontSize = application_default_value_text_size;
          
          if ([textStyle isEqualToString:TEXT_STYLE_0])
            textfield5.font = [UIFont systemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_1])
            textfield5.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_2])
            textfield5.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
          
          else
            textfield5.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        //Property for keyboard type
        [textfield5 setTag:localTag];
        NSString *keyboardType;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
          keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value5_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
        else
          keyboardType = application_default_value_keyboard_type;
        
        if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
          textfield5.keyboardType = UIKeyboardTypeDefault;
        else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
          textfield5.keyboardType = UIKeyboardTypeDecimalPad;
        else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
          textfield5.keyboardType = UIKeyboardTypePhonePad;
        else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
          textfield5.secureTextEntry=YES;
          textfield5.keyboardType = UIKeyboardTypeNumberPad;
        }
        else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
        {
          datePicker1 = [[UIDatePicker alloc] init];
          datePicker1.datePickerMode = UIDatePickerModeDate;
          [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
          self.textfield5.inputView = datePicker1;
          datePicker1.tag = textfield5.tag;
        }
        
        else
          textfield5.keyboardType = UIKeyboardTypeDefault;
        
        textfield5.inputAccessoryView = numberToolbar;
        textfield5.delegate = self;
        //localTag++;
          if(isDebugging){
              [textfield5 setText:@"sample familyname"];
          }
        [parentScrollView addSubview:textfield5];
        next_Y_Position = textfield5.frame.origin.y+textfield5.frame.size.height+distance_Y;
        
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value5_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:[textfieldTitle_Label5 text] forKey:@"labelName"];
        
        if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value5_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value5_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
          textfield5.text=phoneNumberStr;
          textfield5.userInteractionEnabled=NO;
          [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
          [tempDictionary setObject:textfield5.text forKey:@"value"];
        }
        else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value5_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
        {
          textfield5.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
          textfield5.userInteractionEnabled=NO;
          [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
          [tempDictionary setObject:textfield5.text forKey:@"value"];
        }
        else
        {
          [tempDictionary setObject:[textfield5 text] forKey:@"value"];
          [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value5_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value5_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value5_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value5_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        }
      }
      else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        dropDownButton5 = [UIButton buttonWithType:UIButtonTypeSystem];
        [dropDownButton5 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
        
        UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
        [dropDownButton5 setTitleEdgeInsets:titleInsets];
        
        NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_hint",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (dropDownStr) {
          [dropDownButton5 setTitle:dropDownStr forState:UIControlStateNormal];
          [dropDownButton5 setTitle:dropDownStr forState:UIControlStateHighlighted];
        }
        
        dropDownButton5.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [dropDownButton5 setTag:5];
        //            localTag++;
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
          // Properties for dropdown TextColor.
          
          NSArray *dropDownTextColor;
          
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_text_color",propertyFileName,[NSBundle mainBundle], nil))
            dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_text_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
            dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else
            dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
          
          if (dropDownTextColor)
            [dropDownButton5 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
          
          // Properties for dropdown Textstyle.
          
          NSString *textStyle;
          if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_text_style",propertyFileName,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_text_style",propertyFileName,[NSBundle mainBundle], nil);
          else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
          else
            textStyle = application_default_drop_down_value_text_style;
          
          // Properties for dropdown Font size.
          
          NSString *fontSize;
          if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_text_size",propertyFileName,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_text_size",propertyFileName,[NSBundle mainBundle], nil);
          else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
          else
            fontSize = application_default_drop_down_value_text_size;
          
          if ([textStyle isEqualToString:TEXT_STYLE_0])
            dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_1])
            dropDownButton5.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_2])
            dropDownButton5.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
          
          else
            dropDownButton5.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
          
          
        }
        else
        {
          //Default Properties for dropdown textcolor
          
          NSArray *dropDownTextColor ;
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
            dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
          else
            dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
          if (dropDownTextColor)
            dropDownButton5.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
          
          //Default Properties for dropdown textStyle
          
          NSString *textStyle;
          if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
          else
            textStyle = application_default_drop_down_value_text_style;
          
          //Default Properties for dropdown fontSize
          
          NSString *fontSize;
          if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
          else
            fontSize = application_default_drop_down_value_text_size;
          
          if ([textStyle isEqualToString:TEXT_STYLE_0])
            dropDownButton5.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_1])
            dropDownButton5.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_2])
            dropDownButton5.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
          
          else
            dropDownButton5.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
          
        }
        [dropDownButton5 setExclusiveTouch:YES];
        dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
        {
          UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton5.frame.size.width-dropDownButton5.intrinsicContentSize.width-8.0));
          [dropDownButton5 setTitleEdgeInsets:titleInsets];
          dropDownButton5.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        }
        [dropDownButton5 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
        [parentScrollView addSubview:dropDownButton5];
        
        //      [[dropDownButton5 layer] setBorderWidth:0.5f];
        //      [[dropDownButton5 layer] setBorderColor:[UIColor blackColor].CGColor];
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.frame=CGRectMake(dropDownButton5.frame.size.width-28,filed_Y_Position+9,25,25);
        imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
        [parentScrollView addSubview:imageView];
        
        next_Y_Position = dropDownButton5.frame.origin.y+dropDownButton5.frame.size.height+distance_Y;
        
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
        [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value5_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
        [tempDictionary setObject:[textfieldTitle_Label5 text] forKey:@"labelName"];
        [tempDictionary setObject:@"" forKey:@"value"];
      }
      
      numberofFields++;
      [validationsArray addObject:tempDictionary];
      
      UIView *bttomBoreder = [[UIView alloc]initWithFrame:CGRectMake(dropDownButton5.frame.origin.x, dropDownButton5.frame.size.height, dropDownButton5.frame.size.width-10, 1)];
      NSArray *button2_BackgroundColor;
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
        button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
      else
        button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
      
      if (button2_BackgroundColor)
        bttomBoreder.backgroundColor = [UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      [dropDownButton5 addSubview:bttomBoreder];
      
      //[dropDownButton5 setBackgroundColor:[UIColor redColor]];
    }
    localTag++;
    //Field6
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field6_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label6 = [[UILabel alloc] init];
        textfieldTitle_Label6.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
        textfieldTitle_Label6.backgroundColor = [UIColor clearColor];
        textfieldTitle_Label6.lineBreakMode = NSLineBreakByTruncatingTail;
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label6_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label6.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label6_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label6_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label6_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label6_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label6_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label6_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label6_TextColor)
                textfieldTitle_Label6.textColor = [UIColor colorWithRed:[[label6_TextColor objectAtIndex:0] floatValue] green:[[label6_TextColor objectAtIndex:1] floatValue] blue:[[label6_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label6_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label6_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label6_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label6_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label6.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label6.font= [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for label textcolor
            
            NSArray *label6_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label6_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label6_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label6_TextColor)
                textfieldTitle_Label6.textColor = [UIColor colorWithRed:[[label6_TextColor objectAtIndex:0] floatValue] green:[[label6_TextColor objectAtIndex:1] floatValue] blue:[[label6_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label6.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label6.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [parentScrollView addSubview:textfieldTitle_Label6];
        filed_Y_Position = textfieldTitle_Label6.frame.origin.y+textfieldTitle_Label6.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value6_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            
            textfield6 = [[CustomTextField alloc] init];
            textfield6.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
            [textfield6 setBorderStyle:UITextBorderStyleNone];
          [self SetTextFieldBorder:textfield6];
          
            NSString *valueStr=[Localization languageSelectedStringForKey: NSLocalizedStringFromTableInBundle(@"parent_template4_value6_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield6.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([textfield6 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"parent_template4_value6_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value6_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield6.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField6TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_value6_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField6TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value6_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField6TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField6TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField6TextColor)
                    textfield6.textColor = [UIColor colorWithRed:[[textField6TextColor objectAtIndex:0] floatValue] green:[[textField6TextColor objectAtIndex:1] floatValue] blue:[[textField6TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value6_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value6_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value6_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value6_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield6.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield6.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField6TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField6TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField6TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField6TextColor)
                    textfield6.textColor = [UIColor colorWithRed:[[textField6TextColor objectAtIndex:0] floatValue] green:[[textField6TextColor objectAtIndex:1] floatValue] blue:[[textField6TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield6.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield6.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield6.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                   textfield6.font= [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            //Property for KeyBoardType
            [textfield6 setTag:localTag];
            NSString *keyboardType;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value6_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
            {
                keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value6_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            }
            else{
                keyboardType = application_default_value_keyboard_type;
            }
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield6.keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield6.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield6.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield6.secureTextEntry=YES;
                textfield6.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield6.inputView = datePicker1;
                datePicker1.tag = textfield6.tag;
            }
            
            else
                textfield6.keyboardType = UIKeyboardTypeDefault;
            
            textfield6.inputAccessoryView = numberToolbar;
            textfield6.delegate = self;
            //localTag++;
            if(isDebugging){
                [textfield6 setText:@"22/02/1994"];
            }
            [parentScrollView addSubview:textfield6];
            
            next_Y_Position = textfield6.frame.origin.y+textfield6.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value6_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value6_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label6 text] forKey:@"labelName"];
            
            if (userApplicationMode == 0 && phoneNumberStr.length > 0 &&     ([NSLocalizedStringFromTableInBundle(@"parent_template4_value6_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value6_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
                textfield6.text=phoneNumberStr;
                textfield6.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield6.text forKey:@"value"];
            }
            else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value6_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
            {
                textfield6.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                textfield6.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield6.text forKey:@"value"];
            }
            else
            {
                [tempDictionary setObject:[textfield6 text] forKey:@"value"];
                [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value6_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value6_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value6_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value6_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            }
        }
        else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton6 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton6 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton6 setTitleEdgeInsets:titleInsets];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton6 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton6 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton6.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton6 setTag:6];
//            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton6 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton6.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton6.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton6.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton6.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton6.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton6.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton6.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton6.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton6.titleLabel.font= [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [dropDownButton6 setExclusiveTouch:YES];
            dropDownButton6.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                 UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton6.frame.size.width-dropDownButton6.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton6.frame.size.width-dropDownButton6.intrinsicContentSize.width-8.0));
                [dropDownButton6 setTitleEdgeInsets:titleInsets];
                dropDownButton6.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton6 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
            [parentScrollView addSubview:dropDownButton6];

            UIImageView *imageView = [[UIImageView alloc] init];
        imageView.frame=CGRectMake(dropDownButton6.frame.size.width-40,filed_Y_Position+14,22,11);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [parentScrollView addSubview:imageView];
          
            next_Y_Position = dropDownButton6.frame.origin.y+dropDownButton6.frame.size.height+distance_Y;            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value6_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label6 text] forKey:@"labelName"];
            [tempDictionary setObject:@"" forKey:@"value"];
        }
      
      
      UIView *bttomBoreder = [[UIView alloc]initWithFrame:CGRectMake(dropDownButton6.frame.origin.x, dropDownButton6.frame.size.height, dropDownButton6.frame.size.width-10, 1)];
      NSArray *button2_BackgroundColor;
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
        button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
      else
        button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
      
      if (button2_BackgroundColor)
        bttomBoreder.backgroundColor = [UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      [dropDownButton6 addSubview:bttomBoreder];
      
        numberofFields++;
        [validationsArray addObject:tempDictionary];
    }
    localTag++;
    //Field7
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field7_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label7 = [[UILabel alloc] init];
        textfieldTitle_Label7.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
        textfieldTitle_Label7.backgroundColor = [UIColor clearColor];
        textfieldTitle_Label7.lineBreakMode = NSLineBreakByTruncatingTail;
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label7_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label7.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label7_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label7_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label7_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label7_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label7_TextColor)
                textfieldTitle_Label7.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label7_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label7_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label7_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label7_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label7.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else{
            //Default Properties for label textcolor
            
            NSArray *label7_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label7_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label7_TextColor)
                textfieldTitle_Label7.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label7.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [parentScrollView addSubview:textfieldTitle_Label7];
        
        //TextField 7
        filed_Y_Position = textfieldTitle_Label7.frame.origin.y+textfieldTitle_Label7.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value7_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
            
            textfield7 = [[CustomTextField alloc] init];
            textfield7.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
            [textfield7 setBorderStyle:UITextBorderStyleNone];
            [self SetTextFieldBorder:textfield7];
          
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value7_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield7.placeholder=valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([textfield7 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"parent_template4_value7_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value7_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield7.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField7TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_value7_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value7_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField7TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField7TextColor)
                    textfield7.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value7_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value7_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield7.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField7TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField7TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField7TextColor)
                    textfield7.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                   textfield7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield7.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield7.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield7.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            NSString *keyboardType;
            [textfield7 setTag:localTag];
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value7_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value7_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield7 .keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield7.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield7.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield7.secureTextEntry=YES;
                textfield7.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield7.inputView = datePicker1;
                datePicker1.tag = textfield7.tag;
            }
            
            else
                textfield7.keyboardType = UIKeyboardTypeDefault;
            
            textfield7.inputAccessoryView = numberToolbar;
            textfield7.delegate = self;
            //localTag++;
            if(isDebugging){
                [textfield7 setText:@"Test Birthplace"];
            }
            [parentScrollView addSubview:textfield7];
            
            next_Y_Position = textfield7.frame.origin.y+textfield7.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value7_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label7 text] forKey:@"labelName"];
            
            if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
                textfield7.text=phoneNumberStr;
                textfield7.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield7.text forKey:@"value"];
            }
           else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
            {
                textfield7.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                textfield7.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield7.text forKey:@"value"];
            }
            else
            {
                [tempDictionary setObject:[textfield7 text] forKey:@"value"];
                [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            }
        }
        else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton7 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton7 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton7 setTitleEdgeInsets:titleInsets];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_hint",propertyFileName,[NSBundle mainBundle], nil)];
            if (dropDownStr) {
                [dropDownButton7 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton7 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            dropDownButton7.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton7 setTag:7];
//            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton7 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton7.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton7.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton7.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton7.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton7.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton7.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton7.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton7.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton7.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [dropDownButton7 setExclusiveTouch:YES];
            dropDownButton7.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                 UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton7.frame.size.width-dropDownButton7.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton7.frame.size.width-dropDownButton7.intrinsicContentSize.width-8.0));
                [dropDownButton7 setTitleEdgeInsets:titleInsets];
                dropDownButton7.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton7 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
            [parentScrollView addSubview:dropDownButton7];
            
            [[dropDownButton7 layer] setBorderWidth:0.5f];
            [[dropDownButton7 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton7.frame.size.width-28,filed_Y_Position+14,22,11);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [parentScrollView addSubview:imageView];
            
            next_Y_Position = dropDownButton7.frame.origin.y+dropDownButton7.frame.size.height+distance_Y;            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label7 text] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton7.titleLabel.text forKey:@"value"];
        }
      [dropDownButton7 setBackgroundColor:[UIColor clearColor]];
        
        numberofFields++;
        [validationsArray addObject:tempDictionary];
    }
    localTag++;
    //Field8
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field8_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label8 = [[UILabel alloc] init];
        textfieldTitle_Label8.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
        textfieldTitle_Label8.backgroundColor = [UIColor clearColor];
        textfieldTitle_Label8.lineBreakMode = NSLineBreakByTruncatingTail;
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label8_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label8.text =labelStr;
       
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label8_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label8_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label8_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label8_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label8_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label8_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label8_TextColor)
                textfieldTitle_Label8.textColor = [UIColor colorWithRed:[[label8_TextColor objectAtIndex:0] floatValue] green:[[label8_TextColor objectAtIndex:1] floatValue] blue:[[label8_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label8_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label8_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label8_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label8_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label8.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            
        }else{
            
            //Default Properties for label textcolor
            
            NSArray *label8_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label8_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label8_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label8_TextColor)
                textfieldTitle_Label8.textColor = [UIColor colorWithRed:[[label8_TextColor objectAtIndex:0] floatValue] green:[[label8_TextColor objectAtIndex:1] floatValue] blue:[[label8_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label8.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [parentScrollView addSubview:textfieldTitle_Label8];
        filed_Y_Position = textfieldTitle_Label8.frame.origin.y+textfieldTitle_Label8.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value8_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield8 = [[CustomTextField alloc] init];
            textfield8.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
            [textfield8 setBorderStyle:UITextBorderStyleNone];
          [self SetTextFieldBorder:textfield8];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value8_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield8.placeholder =valueStr ;
            
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([textfield8 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"parent_template4_value8_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value8_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield8.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField8TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_value8_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField8TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value8_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField8TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField8TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField8TextColor)
                    textfield8.textColor = [UIColor colorWithRed:[[textField8TextColor objectAtIndex:0] floatValue] green:[[textField8TextColor objectAtIndex:1] floatValue] blue:[[textField8TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value8_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value8_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value8_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value8_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield8.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField8TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField8TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField8TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField8TextColor)
                    textfield8.textColor = [UIColor colorWithRed:[[textField8TextColor objectAtIndex:0] floatValue] green:[[textField8TextColor objectAtIndex:1] floatValue] blue:[[textField8TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                   textfield8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield8.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield8.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield8.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            //Property For KeyboardType
            
            NSString *keyboardType;
            [textfield8 setTag:localTag];
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value8_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value8_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield8 .keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield8.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield8.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield8.secureTextEntry=YES;
                textfield8.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield8.inputView = datePicker1;
                datePicker1.tag = textfield8.tag;
            }
            
            else
                textfield8.keyboardType = UIKeyboardTypeDefault;
            
            textfield8.inputAccessoryView = numberToolbar;
            textfield8.delegate = self;
            //localTag++;
            [parentScrollView addSubview:textfield8];
            
            next_Y_Position = textfield8.frame.origin.y+textfield8.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value8_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value8_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label8 text] forKey:@"labelName"];
            
            if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value8_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value8_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
                textfield8.text=phoneNumberStr;
                textfield8.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield8.text forKey:@"value"];
            }
            else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value8_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
            {
                textfield8.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                textfield8.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield8.text forKey:@"value"];
            }
            else
            {
                [tempDictionary setObject:[textfield8 text] forKey:@"value"];
                [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value8_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value8_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value8_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value8_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            }
        }
        else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton8 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton8 setFrame:CGRectMake(filed_X_Position-2, filed_Y_Position, parentScrollView.frame.size.width-4,40)];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton8 setTitleEdgeInsets:titleInsets];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton8 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton8 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            
            dropDownButton8.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton8 setTag:8];
//            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton8 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton8.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton8.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton8.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton8.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton8.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                   dropDownButton8.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton8.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton8.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton8.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            
            [dropDownButton8 setExclusiveTouch:YES];
            dropDownButton8.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                 UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton8.frame.size.width-dropDownButton8.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton8.frame.size.width-dropDownButton8.intrinsicContentSize.width-8.0));
                [dropDownButton8 setTitleEdgeInsets:titleInsets];
                dropDownButton8.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton8 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
            [parentScrollView addSubview:dropDownButton8];

          UIImageView *imageView = [[UIImageView alloc] init];
          imageView.frame=CGRectMake(dropDownButton8.frame.size.width-40,filed_Y_Position+5,25,25);
          imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
          [parentScrollView addSubview:imageView];
          
          next_Y_Position = dropDownButton8.frame.origin.y+dropDownButton8.frame.size.height+distance_Y;

//          UIImageView *imageView = [[UIImageView alloc] init];
//      imageView.frame=CGRectMake(dropDownButton8.frame.size.width-40,filed_Y_Position+5,25,25);
//          imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
//          [textfield9 addSubview:imageView];
//
//          next_Y_Position = dropDownButton8.frame.origin.y+dropDownButton8.frame.size.height+distance_Y;
          
          UIView *bttomBoreder = [[UIView alloc]initWithFrame:CGRectMake(dropDownButton8.frame.origin.x, dropDownButton8.frame.size.height, dropDownButton8.frame.size.width-10, 1)];
          
          NSArray *button2_BackgroundColor;
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
            button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
          else
            button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
          if (button2_BackgroundColor)
            bttomBoreder.backgroundColor = [UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
          
          [dropDownButton8 addSubview:bttomBoreder];
            
//            [[dropDownButton8 layer] setBorderWidth:0.5f];
//            [[dropDownButton8 layer] setBorderColor:[UIColor blackColor].CGColor];
          
          
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value8_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label8 text] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton8.titleLabel.text forKey:@"value"];
        }
        
        numberofFields++;
        [validationsArray addObject:tempDictionary];
    }
    localTag++;
    //Field9
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field9_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label9 = [[UILabel alloc] init];
        textfieldTitle_Label9.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
        textfieldTitle_Label9.backgroundColor = [UIColor clearColor];
        textfieldTitle_Label9.lineBreakMode = NSLineBreakByTruncatingTail;
        
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label9_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
            textfieldTitle_Label9.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label9_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label9_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label9_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label9_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label9_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label9_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label9_TextColor)
                textfieldTitle_Label9.textColor = [UIColor colorWithRed:[[label9_TextColor objectAtIndex:0] floatValue] green:[[label9_TextColor objectAtIndex:1] floatValue] blue:[[label9_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label9_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label9_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label9_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label9_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label9.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        else{
            //Default Properties for label textcolor
            
            NSArray *label9_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label9_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label9_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label9_TextColor)
                textfieldTitle_Label9.textColor = [UIColor colorWithRed:[[label9_TextColor objectAtIndex:0] floatValue] green:[[label9_TextColor objectAtIndex:1] floatValue] blue:[[label9_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label9.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [parentScrollView addSubview:textfieldTitle_Label9];
        filed_Y_Position = textfieldTitle_Label9.frame.origin.y+textfieldTitle_Label9.frame.size.height+distance_Y;
        // Field9
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value9_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield9 = [[CustomTextField alloc] init];
            textfield9.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
            [textfield9 setBorderStyle:UITextBorderStyleNone];
            [self SetTextFieldBorder:textfield9];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value9_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield9.placeholder =valueStr;
            
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                
                if ([textfield9 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"parent_template4_value9_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value9_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield9.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }

                
                
                // Properties for textField TextColor.
                
                NSArray *textField9TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField9TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField9TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField9TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField9TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField9TextColor objectAtIndex:0] floatValue] green:[[textField9TextColor objectAtIndex:1] floatValue] blue:[[textField9TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield9.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField9TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField9TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField9TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField9TextColor)
                    textfield9.textColor = [UIColor colorWithRed:[[textField9TextColor objectAtIndex:0] floatValue] green:[[textField9TextColor objectAtIndex:1] floatValue] blue:[[textField9TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield9.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                   textfield9.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield9.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            NSString *keyboardType;
            [textfield9 setTag:localTag];
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value9_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
                keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value9_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            else
                keyboardType = application_default_value_keyboard_type;
            
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield9 .keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield9.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield9.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield9.secureTextEntry=YES;
                textfield9.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield9.inputView = datePicker1;
                datePicker1.tag = textfield9.tag;
            }
            
            else
                textfield9.keyboardType = UIKeyboardTypeDefault;
            
            textfield9.inputAccessoryView = numberToolbar;
            textfield9.delegate = self;
           // localTag++;
            [parentScrollView addSubview:textfield9];
            
            next_Y_Position = textfield9.frame.origin.y+textfield9.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value9_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value9_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label9 text] forKey:@"labelName"];
            
            if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value9_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value9_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
                textfield9.text=phoneNumberStr;
                textfield9.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield9.text forKey:@"value"];
            }
            else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value9_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
            {
                textfield9.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                textfield9.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield9.text forKey:@"value"];
            }
            else
            {
                [tempDictionary setObject:[textfield9 text] forKey:@"value"];
                [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value9_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value9_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value9_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value9_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            }
        }
        // DropDown Field9
        else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton9 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton9 setFrame:CGRectMake(filed_X_Position-2, filed_Y_Position, parentScrollView.frame.size.width-4,40)];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton9 setTitleEdgeInsets:titleInsets];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (dropDownStr) {
                [dropDownButton9 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton9 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton9.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton9 setTag:9];
//            localTag++;
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton9 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton9.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton9.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton9.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton9.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton9.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton9.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton9.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                     dropDownButton9.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                   dropDownButton9.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
            }
            
            [dropDownButton9 setExclusiveTouch:YES];
            dropDownButton9.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
//                 UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton9.frame.size.width-dropDownButton9.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton9.frame.size.width-dropDownButton9.intrinsicContentSize.width-8.0));
//                [dropDownButton9 setTitleEdgeInsets:titleInsets];
//                dropDownButton9.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton9 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
            [parentScrollView addSubview:dropDownButton9];

          
          UIImageView *imageView = [[UIImageView alloc] init];
          imageView.frame=CGRectMake(dropDownButton9.frame.size.width-40,filed_Y_Position+5,25,25);
          imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
          [parentScrollView addSubview:imageView];
          
          next_Y_Position = dropDownButton9.frame.origin.y+dropDownButton9.frame.size.height+distance_Y;
          
          UIView *bttomBoreder = [[UIView alloc]initWithFrame:CGRectMake(dropDownButton9.frame.origin.x, dropDownButton9.frame.size.height, dropDownButton9.frame.size.width-10, 1)];
          
          NSArray *button2_BackgroundColor;
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
            button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
          else
            button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
          if (button2_BackgroundColor)
            bttomBoreder.backgroundColor = [UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
          
          [dropDownButton9 addSubview:bttomBoreder];
          
          
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value9_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label9 text] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton9.titleLabel.text forKey:@"value"];
        }
        
        numberofFields++;
        [validationsArray addObject:tempDictionary];
    }
    
    localTag++;
    //Field10
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field10_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
        textfieldTitle_Label10 = [[UILabel alloc] init];
        textfieldTitle_Label10.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
        textfieldTitle_Label10.backgroundColor = [UIColor clearColor];
        textfieldTitle_Label10.lineBreakMode = NSLineBreakByTruncatingTail;
        NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label10_text",propertyFileName,[NSBundle mainBundle], nil)];
        
        if (labelStr)
           textfieldTitle_Label10.text =labelStr;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for label TextColor.
            
            NSArray *label10_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label10_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label10_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label10_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label10_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            
            else
                label10_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
            
            if (label10_TextColor)
                textfieldTitle_Label10.textColor = [UIColor colorWithRed:[[label10_TextColor objectAtIndex:0] floatValue] green:[[label10_TextColor objectAtIndex:1] floatValue] blue:[[label10_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            // Properties for label Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label10_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label10_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            
            // Properties for label Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label10_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label10_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            
        }else{
            //Default Properties for label textcolor
            
            NSArray *label10_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
                label10_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                label10_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
            if (label10_TextColor)
                textfieldTitle_Label10.textColor = [UIColor colorWithRed:[[label10_TextColor objectAtIndex:0] floatValue] green:[[label10_TextColor objectAtIndex:1] floatValue] blue:[[label10_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
            
            //Default Properties for label textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_text_style;
            
            //Default Properties for label fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                textfieldTitle_Label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                textfieldTitle_Label10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                textfieldTitle_Label10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                textfieldTitle_Label10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        [parentScrollView addSubview:textfieldTitle_Label10];
        filed_Y_Position = textfieldTitle_Label10.frame.origin.y+textfieldTitle_Label10.frame.size.height+distance_Y;
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value10_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            textfield10 = [[CustomTextField alloc] init];
            textfield10.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
            [textfield10 setBorderStyle:UITextBorderStyleNone];
            [self SetTextFieldBorder:textfield10];
            
            NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value10_hint",propertyFileName,[NSBundle mainBundle], nil)];
            
            if (valueStr)
                textfield10.placeholder =valueStr ;
            
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                //Properties for textfield Hint text color
                if ([textfield10 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    NSArray *textFieldHintColor;
                    
                    if (NSLocalizedStringFromTableInBundle(@"parent_template4_value10_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value10_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
                        textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
                    
                    else
                        textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                    
                    if (textFieldHintColor)
                        textfield10.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
                }
                
                // Properties for textField TextColor.
                
                NSArray *textField10TextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_value10_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField10TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value10_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField10TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    textField10TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
                
                if (textField10TextColor)
                    textfield1.textColor = [UIColor colorWithRed:[[textField10TextColor objectAtIndex:0] floatValue] green:[[textField10TextColor objectAtIndex:1] floatValue] blue:[[textField10TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                // Properties for textField Textstyle.
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value10_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value10_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_value_text_style;
                
                
                // Properties for textField Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_value10_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value10_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                //Default Properties for textfiled textcolor
                
                NSArray *textField10TextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    textField10TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    textField10TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
                if (textField10TextColor)
                    textfield10.textColor = [UIColor colorWithRed:[[textField10TextColor objectAtIndex:0] floatValue] green:[[textField10TextColor objectAtIndex:1] floatValue] blue:[[textField10TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for textfiled textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_text_style;
                
                //Default Properties for textfiled fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                    fontSize = application_default_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    textfield10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    textfield10.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    textfield10.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    textfield10.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            NSString *keyboardType;
            [textfield10 setTag:localTag];
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_value10_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
            {
                keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value10_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
            }
            else{
                keyboardType = application_default_value_keyboard_type;
            }
            if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
                textfield10 .keyboardType = UIKeyboardTypeDefault;
            else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
                textfield10.keyboardType = UIKeyboardTypeDecimalPad;
            else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
                textfield10.keyboardType = UIKeyboardTypePhonePad;
            else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
                textfield10.secureTextEntry=YES;
                textfield10.keyboardType = UIKeyboardTypeNumberPad;
            }
            else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
            {
                datePicker1 = [[UIDatePicker alloc] init];
                datePicker1.datePickerMode = UIDatePickerModeDate;
                [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
                self.textfield10.inputView = datePicker1;
                datePicker1.tag = textfield10.tag;
            }
            
            else
                textfield7.keyboardType = UIKeyboardTypeDefault;
            
            textfield10.inputAccessoryView = numberToolbar;
            textfield10.delegate = self;
            //localTag++;
            [parentScrollView addSubview:textfield10];
            if(isDebugging){
                NSLog(@"debuging..");
                int lowerBound = 11111111;
                int upperBound = 99999999;
                int num = lowerBound + arc4random() % (upperBound - lowerBound);
                NSString *idStr = [NSString stringWithFormat:@"657%d", num];
                [textfield10 setText:idStr];
            }

            next_Y_Position = textfield10.frame.origin.y+textfield10.frame.size.height+distance_Y;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value10_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value10_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label10 text] forKey:@"labelName"];
            
            if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value10_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value10_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
                textfield10.text=phoneNumberStr;
                textfield10.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield10.text forKey:@"value"];
            }
            
            else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value10_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
            {
                textfield10.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
                textfield10.userInteractionEnabled=NO;
                [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
                [tempDictionary setObject:textfield10.text forKey:@"value"];
            }
            else
            {
                [tempDictionary setObject:[textfield10 text] forKey:@"value"];
                [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value10_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value10_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value10_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9:NSLocalizedStringFromTableInBundle(@"parent_template4_value10_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            }
        }
        else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            dropDownButton10 = [UIButton buttonWithType:UIButtonTypeSystem];
            [dropDownButton10 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
            
            UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
            [dropDownButton10 setTitleEdgeInsets:titleInsets];
            
            NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_hint",propertyFileName,[NSBundle mainBundle], nil)];
            if (dropDownStr) {
                [dropDownButton10 setTitle:dropDownStr forState:UIControlStateNormal];
                [dropDownButton10 setTitle:dropDownStr forState:UIControlStateHighlighted];
            }
            
            dropDownButton10.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [dropDownButton10 setTag:10];
            if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
            {
                // Properties for dropdown TextColor.
                NSArray *dropDownTextColor;
                
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                
                else
                    dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                
                if (dropDownTextColor)
                    [dropDownButton10 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
                
                // Properties for dropdown Textstyle.
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;                
                
                // Properties for dropdown Font size.
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton10.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton10.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton10.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton10.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            else
            {
                
                //Default Properties for dropdown textcolor
                
                NSArray *dropDownTextColor ;
                if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
                    dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                else
                    dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
                if (dropDownTextColor)
                    dropDownButton10.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
                
                //Default Properties for dropdown textStyle
                
                NSString *textStyle;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
                    textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
                else
                    textStyle = application_default_drop_down_value_text_style;
                
                //Default Properties for dropdown fontSize
                
                NSString *fontSize;
                if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
                    fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
                else
                fontSize = application_default_drop_down_value_text_size;
                
                if ([textStyle isEqualToString:TEXT_STYLE_0])
                    dropDownButton10.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_1])
                    dropDownButton10.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
                
                else if ([textStyle isEqualToString:TEXT_STYLE_2])
                    dropDownButton10.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
                
                else
                    dropDownButton10.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            }
            [dropDownButton10 setExclusiveTouch:YES];
            dropDownButton10.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
            if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
            {
                 UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton10.frame.size.width-dropDownButton10.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton10.frame.size.width-dropDownButton10.intrinsicContentSize.width-8.0));
                [dropDownButton10 setTitleEdgeInsets:titleInsets];
                dropDownButton10.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            }
            [dropDownButton10 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
            [parentScrollView addSubview:dropDownButton10];
            
            [[dropDownButton10 layer] setBorderWidth:0.5f];
            [[dropDownButton10 layer] setBorderColor:[UIColor blackColor].CGColor];
            
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame=CGRectMake(dropDownButton10.frame.size.width-28,filed_Y_Position+14,22,11);
            imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
            [parentScrollView addSubview:imageView];
            
            next_Y_Position = distance_Y+dropDownButton10.frame.origin.y+dropDownButton10.frame.size.height;
            
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
            [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value10_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
            [tempDictionary setObject:[textfieldTitle_Label10 text] forKey:@"labelName"];
            [tempDictionary setObject:dropDownButton10.titleLabel.text forKey:@"value"];
        }
        
        numberofFields++;
        [validationsArray addObject:tempDictionary];
    }
    localTag++;
  
  
 //Kck Coding
  
  // Field11
  if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field11_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
  {
    NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
    textfieldTitle_Label11 = [[UILabel alloc] init];
    textfieldTitle_Label11.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
    textfieldTitle_Label11.backgroundColor = [UIColor clearColor];
    textfieldTitle_Label11.lineBreakMode = NSLineBreakByTruncatingTail;
    
    NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label11_text",propertyFileName,[NSBundle mainBundle], nil)];
    
    if (labelStr)
      textfieldTitle_Label11.text =labelStr;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label11_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      // Properties for label TextColor.
      
      NSArray *label7_TextColor;
      
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label11_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label11_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else
        label7_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
      
      if (label7_TextColor)
        textfieldTitle_Label11.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      // Properties for label Textstyle.
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label11_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label11_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      
      // Properties for label Font size.
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label11_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label11_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label11.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label11.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    else{
      //Default Properties for label textcolor
      
      NSArray *label7_TextColor ;
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      else
        label7_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
      if (label7_TextColor)
        textfieldTitle_Label11.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      //Default Properties for label textStyle
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      //Default Properties for label fontSize
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label11.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label11.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    [parentScrollView addSubview:textfieldTitle_Label11];
    
    //TextField 11
    filed_Y_Position = textfieldTitle_Label11.frame.origin.y+textfieldTitle_Label11.frame.size.height+distance_Y;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value11_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
      
      textfield11 = [[CustomTextField alloc] init];
      textfield11.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
      [textfield11 setBorderStyle:UITextBorderStyleNone];
      [self SetTextFieldBorder:textfield11];
      
      NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value11_hint",propertyFileName,[NSBundle mainBundle], nil)];
      
      if (valueStr)
        textfield11.placeholder=valueStr;
      
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value11_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        //Properties for textfield Hint text color
        
        if ([textfield11 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
          NSArray *textFieldHintColor;
          
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_value11_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value11_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else
            textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
          
          if (textFieldHintColor)
            textfield11.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
        }
        
        // Properties for textField TextColor.
        
        NSArray *textField7TextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_value7_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value7_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          textField7TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
        
        if (textField7TextColor)
          textfield11.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        // Properties for textField Textstyle.
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value11_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value11_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_value_text_style;
        
        
        // Properties for textField Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield11.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield11.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for textfiled textcolor
        
        NSArray *textField7TextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          textField7TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
        if (textField7TextColor)
          textfield11.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for textfiled textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_text_style;
        
        //Default Properties for textfiled fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield11.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield11.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield11.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
      }
      NSString *keyboardType;
      [textfield11 setTag:localTag];
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_value11_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
        keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value11_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
      else
        keyboardType = application_default_value_keyboard_type;
      
      if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
        textfield11 .keyboardType = UIKeyboardTypeDefault;
      else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
        textfield11.keyboardType = UIKeyboardTypeDecimalPad;
      else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
        textfield11.keyboardType = UIKeyboardTypePhonePad;
      else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
        textfield11.secureTextEntry=YES;
        textfield11.keyboardType = UIKeyboardTypeNumberPad;
      }
      else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
      {
        datePicker1 = [[UIDatePicker alloc] init];
        datePicker1.datePickerMode = UIDatePickerModeDate;
        [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.textfield11.inputView = datePicker1;
        datePicker1.tag = textfield11.tag;
      }
      
      else
        textfield11.keyboardType = UIKeyboardTypeDefault;
      
      textfield11.inputAccessoryView = numberToolbar;
      textfield11.delegate = self;
      //localTag++;
       if(isDebugging){
           [textfield11 setText:@"12/12/2020"];
       }
      [parentScrollView addSubview:textfield11];
      
      next_Y_Position = textfield11.frame.origin.y+textfield11.frame.size.height+distance_Y;
      
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value11_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value11_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label11 text] forKey:@"labelName"];
      
      if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value11_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value11_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
        textfield11.text=phoneNumberStr;
        textfield11.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield7.text forKey:@"value"];
      }
      else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value11_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
      {
        textfield11.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
        textfield11.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield11.text forKey:@"value"];
      }
      else
      {
        [tempDictionary setObject:[textfield11 text] forKey:@"value"];
        [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value11_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value11_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value11_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value11_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      dropDownButton11 = [UIButton buttonWithType:UIButtonTypeSystem];
      [dropDownButton11 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
      
      UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
      [dropDownButton11 setTitleEdgeInsets:titleInsets];
      
      NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_hint",propertyFileName,[NSBundle mainBundle], nil)];
      if (dropDownStr) {
        [dropDownButton11 setTitle:dropDownStr forState:UIControlStateNormal];
        [dropDownButton11 setTitle:dropDownStr forState:UIControlStateHighlighted];
      }
      dropDownButton11.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
      [dropDownButton11 setTag:11];
      //            localTag++;
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        // Properties for dropdown TextColor.
        
        NSArray *dropDownTextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        
        if (dropDownTextColor)
          [dropDownButton11 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
        
        // Properties for dropdown Textstyle.
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        // Properties for dropdown Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton11.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton11.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton11.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton11.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for dropdown textcolor
        NSArray *dropDownTextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        if (dropDownTextColor)
          dropDownButton11.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for dropdown textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        //Default Properties for dropdown fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton11.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton11.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton11.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton11.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      
      [dropDownButton11 setExclusiveTouch:YES];
      dropDownButton11.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
      NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
      if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
      {
        UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton11.frame.size.width-dropDownButton11.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton11.frame.size.width-dropDownButton11.intrinsicContentSize.width-8.0));
        [dropDownButton11 setTitleEdgeInsets:titleInsets];
        dropDownButton11.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
      }
      [dropDownButton11 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
      [parentScrollView addSubview:dropDownButton11];
      
      [[dropDownButton11 layer] setBorderWidth:0.5f];
      [[dropDownButton11 layer] setBorderColor:[UIColor blackColor].CGColor];
      
      UIImageView *imageView = [[UIImageView alloc] init];
      imageView.frame=CGRectMake(dropDownButton11.frame.size.width-28,filed_Y_Position+14,22,11);
      imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
      [parentScrollView addSubview:imageView];
      
      next_Y_Position = dropDownButton11.frame.origin.y+dropDownButton11.frame.size.height+distance_Y;
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value11_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label11 text] forKey:@"labelName"];
      [tempDictionary setObject:dropDownButton11.titleLabel.text forKey:@"value"];
    }
    [dropDownButton11 setBackgroundColor:[UIColor clearColor]];
    
    numberofFields++;
    [validationsArray addObject:tempDictionary];
  }
  
  //Field12
if([NSLocalizedStringFromTableInBundle(@"parent_template4_field12_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
  {
    NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
    textfieldTitle_Label12 = [[UILabel alloc] init];
    textfieldTitle_Label12.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
    textfieldTitle_Label12.backgroundColor = [UIColor clearColor];
    textfieldTitle_Label12.lineBreakMode = NSLineBreakByTruncatingTail;
    
    NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label12_text",propertyFileName,[NSBundle mainBundle], nil)];
    
    if (labelStr)
      textfieldTitle_Label12.text =labelStr;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label12_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      // Properties for label TextColor.
      
      NSArray *label7_TextColor;
      
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label12_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label12_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else
        label7_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
      
      if (label7_TextColor)
        textfieldTitle_Label12.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      // Properties for label Textstyle.
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label12_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label12_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      
      // Properties for label Font size.
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label7_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label7_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label12.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label12.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    else{
      //Default Properties for label textcolor
      
      NSArray *label7_TextColor ;
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      else
        label7_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
      if (label7_TextColor)
        textfieldTitle_Label12.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      //Default Properties for label textStyle
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      //Default Properties for label fontSize
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label12.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label12.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    [parentScrollView addSubview:textfieldTitle_Label12];
    
    //TextField 11
    filed_Y_Position = textfieldTitle_Label12.frame.origin.y+textfieldTitle_Label12.frame.size.height+distance_Y;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value12_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
      
      textfield12 = [[CustomTextField alloc] init];
      textfield12.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
      [textfield12 setBorderStyle:UITextBorderStyleNone];
      [self SetTextFieldBorder:textfield12];
      
      NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value12_hint",propertyFileName,[NSBundle mainBundle], nil)];
      
      if (valueStr)
        textfield12.placeholder=valueStr;
      
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value12_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        //Properties for textfield Hint text color
        
        if ([textfield12 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
          NSArray *textFieldHintColor;
          
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_value12_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value12_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else
            textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
          
          if (textFieldHintColor)
            textfield12.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
        }
        
        // Properties for textField TextColor.
        
        NSArray *textField7TextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_value12_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value12_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          textField7TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
        
        if (textField7TextColor)
          textfield12.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        // Properties for textField Textstyle.
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value12_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value12_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_value_text_style;
        
        
        // Properties for textField Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield12.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield12.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for textfiled textcolor
        
        NSArray *textField7TextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          textField7TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
        if (textField7TextColor)
          textfield12.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for textfiled textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_text_style;
        
        //Default Properties for textfiled fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield12.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield12.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield12.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
      }
      NSString *keyboardType;
      [textfield12 setTag:localTag];
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_value12_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
        keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value12_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
      else
        keyboardType = application_default_value_keyboard_type;
      
      if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
        textfield12 .keyboardType = UIKeyboardTypeDefault;
      else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
        textfield12.keyboardType = UIKeyboardTypeDecimalPad;
      else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
        textfield12.keyboardType = UIKeyboardTypePhonePad;
      else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
        textfield12.secureTextEntry=YES;
        textfield12.keyboardType = UIKeyboardTypeNumberPad;
      }
      else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
      {
        datePicker1 = [[UIDatePicker alloc] init];
        datePicker1.datePickerMode = UIDatePickerModeDate;
        [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.textfield12.inputView = datePicker1;
        datePicker1.tag = textfield12.tag;
      }
      
      else
        textfield12.keyboardType = UIKeyboardTypeDefault;
      
      textfield12.inputAccessoryView = numberToolbar;
      textfield12.delegate = self;
      //localTag++;
        if(isDebugging){
            [textfield12 setText:@"TestState"];
        }
      [parentScrollView addSubview:textfield12];
      next_Y_Position = textfield12.frame.origin.y+textfield12.frame.size.height+distance_Y;
      
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value12_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value12_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label11 text] forKey:@"labelName"];
      
      if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value12_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value12_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
        textfield12.text=phoneNumberStr;
        textfield12.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield12.text forKey:@"value"];
      }
      else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
      {
        textfield12.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
        textfield12.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield12.text forKey:@"value"];
      }
      else
      {
        [tempDictionary setObject:[textfield12 text] forKey:@"value"];
        [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value12_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value12_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value12_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value12_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value12_visibility", propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      dropDownButton12 = [UIButton buttonWithType:UIButtonTypeSystem];
      [dropDownButton12 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
      
      UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
      [dropDownButton12 setTitleEdgeInsets:titleInsets];
      
      NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value12_hint",propertyFileName,[NSBundle mainBundle], nil)];
      if (dropDownStr) {
        [dropDownButton12 setTitle:dropDownStr forState:UIControlStateNormal];
        [dropDownButton12 setTitle:dropDownStr forState:UIControlStateHighlighted];
      }
      dropDownButton12.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
      [dropDownButton12 setTag:12];
      //            localTag++;
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value12_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        // Properties for dropdown TextColor.
        
        NSArray *dropDownTextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value12_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value12_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        
        if (dropDownTextColor)
          [dropDownButton12 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
        
        // Properties for dropdown Textstyle.
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value12_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value12_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        // Properties for dropdown Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value12_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value12_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton12.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton12.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton12.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton12.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for dropdown textcolor
        NSArray *dropDownTextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        if (dropDownTextColor)
          dropDownButton12.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for dropdown textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        //Default Properties for dropdown fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton12.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton12.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton12.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton12.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      
      [dropDownButton12 setExclusiveTouch:YES];
      dropDownButton12.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
      NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
      if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
      {
        UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton12.frame.size.width-dropDownButton12.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton12.frame.size.width-dropDownButton12.intrinsicContentSize.width-8.0));
        [dropDownButton12 setTitleEdgeInsets:titleInsets];
        dropDownButton12.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
      }
      [dropDownButton12 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
      [parentScrollView addSubview:dropDownButton12];
      
      [[dropDownButton12 layer] setBorderWidth:0.5f];
      [[dropDownButton12 layer] setBorderColor:[UIColor blackColor].CGColor];
      
      UIImageView *imageView = [[UIImageView alloc] init];
      imageView.frame=CGRectMake(dropDownButton12.frame.size.width-28,filed_Y_Position+14,22,11);
      imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
      [parentScrollView addSubview:imageView];
      
      next_Y_Position = dropDownButton12.frame.origin.y+dropDownButton12.frame.size.height+distance_Y;
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label12 text] forKey:@"labelName"];
      [tempDictionary setObject:dropDownButton12.titleLabel.text forKey:@"value"];
    }
    [dropDownButton12 setBackgroundColor:[UIColor clearColor]];
    
    numberofFields++;
    [validationsArray addObject:tempDictionary];
  }
  
  //Filed13
  if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field13_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
  {
    NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
    textfieldTitle_Label13 = [[UILabel alloc] init];
    textfieldTitle_Label13.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
    textfieldTitle_Label13.backgroundColor = [UIColor clearColor];
    textfieldTitle_Label13.lineBreakMode = NSLineBreakByTruncatingTail;
    
    NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label13_text",propertyFileName,[NSBundle mainBundle], nil)];
    
    if (labelStr)
      textfieldTitle_Label13.text =labelStr;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label13_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      // Properties for label TextColor.
      
      NSArray *label7_TextColor;
      
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label13_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label13_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else
        label7_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
      
      if (label7_TextColor)
        textfieldTitle_Label13.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      // Properties for label Textstyle.
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label13_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label13_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      
      // Properties for label Font size.
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label13_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label13_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label13.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label13.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    else{
      //Default Properties for label textcolor
      
      NSArray *label7_TextColor ;
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      else
        label7_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
      if (label7_TextColor)
        textfieldTitle_Label13.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      //Default Properties for label textStyle
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      //Default Properties for label fontSize
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label13.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label13.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    [parentScrollView addSubview:textfieldTitle_Label13];
    
    //TextField 11
    filed_Y_Position = textfieldTitle_Label13.frame.origin.y+textfieldTitle_Label13.frame.size.height+distance_Y;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value13_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
      
      textfield13 = [[CustomTextField alloc] init];
      textfield13.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
      [textfield13 setBorderStyle:UITextBorderStyleNone];
      [self SetTextFieldBorder:textfield13];
      
      NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value13_hint", propertyFileName,[NSBundle mainBundle], nil)];
      
      if (valueStr)
        textfield13.placeholder=valueStr;
      
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value13_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        //Properties for textfield Hint text color
        
        if ([textfield13 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
          NSArray *textFieldHintColor;
          
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_value13_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value13_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else
            textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
          
          if (textFieldHintColor)
            textfield13.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
        }
        
        // Properties for textField TextColor.
        
        NSArray *textField7TextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_value13_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value13_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          textField7TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
        
        if (textField7TextColor)
          textfield13.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        // Properties for textField Textstyle.
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value13_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value13_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_value_text_style;
        
        
        // Properties for textField Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield13.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield13.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for textfiled textcolor
        
        NSArray *textField7TextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          textField7TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
        if (textField7TextColor)
          textfield13.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for textfiled textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_text_style;
        
        //Default Properties for textfiled fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield13.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield13.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield13.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
      }
      NSString *keyboardType;
      [textfield13 setTag:localTag];
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_value13_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
        keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value13_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
      else
        keyboardType = application_default_value_keyboard_type;
      
      if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
        textfield13 .keyboardType = UIKeyboardTypeDefault;
      else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
        textfield13.keyboardType = UIKeyboardTypeDecimalPad;
      else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
        textfield13.keyboardType = UIKeyboardTypePhonePad;
      else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
        textfield13.secureTextEntry=YES;
        textfield13.keyboardType = UIKeyboardTypeNumberPad;
      }
      else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
      {
        datePicker1 = [[UIDatePicker alloc] init];
        datePicker1.datePickerMode = UIDatePickerModeDate;
        [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.textfield13.inputView = datePicker1;
        datePicker1.tag = textfield13.tag;
      }
      
      else
        textfield13.keyboardType = UIKeyboardTypeDefault;
      
      textfield13.inputAccessoryView = numberToolbar;
      textfield13.delegate = self;
      //localTag++;
      if(isDebugging){
          [textfield13 setText:@"Test District"];
      }
      [parentScrollView addSubview:textfield13];
        
      
      next_Y_Position = textfield13.frame.origin.y+textfield13.frame.size.height+distance_Y;
      
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value13_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value13_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label13 text] forKey:@"labelName"];
      
      if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value13_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value13_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
        textfield13.text=phoneNumberStr;
        textfield13.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield13.text forKey:@"value"];
      }
      else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
      {
        textfield13.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
        textfield13.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield13.text forKey:@"value"];
      }
      else
      {
        [tempDictionary setObject:[textfield13 text] forKey:@"value"];
        [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value7_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      dropDownButton13 = [UIButton buttonWithType:UIButtonTypeSystem];
      [dropDownButton13 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
      
      UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
      [dropDownButton13 setTitleEdgeInsets:titleInsets];
      
      NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_hint",propertyFileName,[NSBundle mainBundle], nil)];
      if (dropDownStr) {
        [dropDownButton13 setTitle:dropDownStr forState:UIControlStateNormal];
        [dropDownButton13 setTitle:dropDownStr forState:UIControlStateHighlighted];
      }
      dropDownButton13.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
      [dropDownButton13 setTag:13];
      //            localTag++;
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        // Properties for dropdown TextColor.
        
        NSArray *dropDownTextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        
        if (dropDownTextColor)
          [dropDownButton13 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
        
        // Properties for dropdown Textstyle.
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        // Properties for dropdown Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton13.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton13.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton13.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton13.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for dropdown textcolor
        NSArray *dropDownTextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        if (dropDownTextColor)
          dropDownButton13.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for dropdown textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        //Default Properties for dropdown fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton13.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton13.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton13.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton13.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      
      [dropDownButton13 setExclusiveTouch:YES];
      dropDownButton13.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
      NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
      if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
      {
        UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton13.frame.size.width-dropDownButton13.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton13.frame.size.width-dropDownButton13.intrinsicContentSize.width-8.0));
        [dropDownButton13 setTitleEdgeInsets:titleInsets];
        dropDownButton13.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
      }
      [dropDownButton13 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
      [parentScrollView addSubview:dropDownButton13];
      
      [[dropDownButton13 layer] setBorderWidth:0.5f];
      [[dropDownButton13 layer] setBorderColor:[UIColor blackColor].CGColor];
      
      UIImageView *imageView = [[UIImageView alloc] init];
      imageView.frame=CGRectMake(dropDownButton13.frame.size.width-28,filed_Y_Position+14,22,11);
      imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
      [parentScrollView addSubview:imageView];
      
      next_Y_Position = dropDownButton13.frame.origin.y+dropDownButton13.frame.size.height+distance_Y;
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value13_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label13 text] forKey:@"labelName"];
      [tempDictionary setObject:dropDownButton13.titleLabel.text forKey:@"value"];
    }
    [dropDownButton13 setBackgroundColor:[UIColor clearColor]];
    
    numberofFields++;
    [validationsArray addObject:tempDictionary];
  }
  
  //Field14
  if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field14_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
  {
    NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
    textfieldTitle_Label14 = [[UILabel alloc] init];
    textfieldTitle_Label14.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
    textfieldTitle_Label14.backgroundColor = [UIColor clearColor];
    textfieldTitle_Label14.lineBreakMode = NSLineBreakByTruncatingTail;
    
    NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label14_text",propertyFileName,[NSBundle mainBundle], nil)];
    
    if (labelStr)
      textfieldTitle_Label14.text =labelStr;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label14_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      // Properties for label TextColor.
      
      NSArray *label7_TextColor;
      
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label14_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label14_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else
        label7_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
      
      if (label7_TextColor)
        textfieldTitle_Label14.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      // Properties for label Textstyle.
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label14_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label14_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      
      // Properties for label Font size.
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label14_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label14_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label14.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label14.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    else{
      //Default Properties for label textcolor
      
      NSArray *label7_TextColor ;
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      else
        label7_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
      if (label7_TextColor)
        textfieldTitle_Label14.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      //Default Properties for label textStyle
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      //Default Properties for label fontSize
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label14.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label14.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    [parentScrollView addSubview:textfieldTitle_Label14];
    
    //TextField 14
    filed_Y_Position = textfieldTitle_Label14.frame.origin.y+textfieldTitle_Label14.frame.size.height+distance_Y;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value7_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
      
      textfield14 = [[CustomTextField alloc] init];
      textfield14.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
      [textfield14 setBorderStyle:UITextBorderStyleNone];
      [self SetTextFieldBorder:textfield14];
      
      NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value14_hint",propertyFileName,[NSBundle mainBundle], nil)];
      
      if (valueStr)
        textfield14.placeholder=valueStr;
      
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value7_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        //Properties for textfield Hint text color
        
        if ([textfield14 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
          NSArray *textFieldHintColor;
          
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_value14_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value14_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else
            textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
          
          if (textFieldHintColor)
            textfield14.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
        }
        
        // Properties for textField TextColor.
        
        NSArray *textField7TextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_value14_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value14_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          textField7TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
        
        if (textField7TextColor)
          textfield14.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        // Properties for textField Textstyle.
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value14_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value14_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_value_text_style;
        
        
        // Properties for textField Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield14.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield14.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for textfiled textcolor
        
        NSArray *textField7TextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          textField7TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
        if (textField7TextColor)
          textfield14.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for textfiled textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_text_style;
        
        //Default Properties for textfiled fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield14.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield14.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield14.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
      }
      NSString *keyboardType;
      [textfield14 setTag:localTag];
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_value14_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
        keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value14_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
      else
        keyboardType = application_default_value_keyboard_type;
      
      if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
        textfield14 .keyboardType = UIKeyboardTypeDefault;
      else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
        textfield14.keyboardType = UIKeyboardTypeDecimalPad;
      else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
        textfield14.keyboardType = UIKeyboardTypePhonePad;
      else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
        textfield14.secureTextEntry=YES;
        textfield14.keyboardType = UIKeyboardTypeNumberPad;
      }
      else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
      {
        datePicker1 = [[UIDatePicker alloc] init];
        datePicker1.datePickerMode = UIDatePickerModeDate;
        [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.textfield14.inputView = datePicker1;
        datePicker1.tag = textfield14.tag;
      }
      
      else
        textfield14.keyboardType = UIKeyboardTypeDefault;
      
      textfield14.inputAccessoryView = numberToolbar;
      textfield14.delegate = self;
      //localTag++;
        if(isDebugging){
            [textfield14 setText:@"Test Street"];
        }
      [parentScrollView addSubview:textfield14];
      
      next_Y_Position = textfield14.frame.origin.y+textfield14.frame.size.height+distance_Y;
      
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value14_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value14_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label11 text] forKey:@"labelName"];
      
      if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value14_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value14_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
        textfield14.text=phoneNumberStr;
        textfield14.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield14.text forKey:@"value"];
      }
      else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value14_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
      {
        textfield14.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
        textfield14.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield14.text forKey:@"value"];
      }
      else
      {
        [tempDictionary setObject:[textfield14 text] forKey:@"value"];
        [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value14_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value14_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value14_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value14_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      dropDownButton14 = [UIButton buttonWithType:UIButtonTypeSystem];
      [dropDownButton14 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
      
      UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
      [dropDownButton14 setTitleEdgeInsets:titleInsets];
      
      NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_hint",propertyFileName,[NSBundle mainBundle], nil)];
      if (dropDownStr) {
        [dropDownButton14 setTitle:dropDownStr forState:UIControlStateNormal];
        [dropDownButton14 setTitle:dropDownStr forState:UIControlStateHighlighted];
      }
      dropDownButton14.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
      [dropDownButton14 setTag:14];
      //            localTag++;
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        // Properties for dropdown TextColor.
        
        NSArray *dropDownTextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        
        if (dropDownTextColor)
          [dropDownButton14 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
        
        // Properties for dropdown Textstyle.
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        // Properties for dropdown Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton14.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton14.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton14.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton14.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for dropdown textcolor
        NSArray *dropDownTextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        if (dropDownTextColor)
          dropDownButton14.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for dropdown textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        //Default Properties for dropdown fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton14.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton14.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton14.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton14.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      
      [dropDownButton14 setExclusiveTouch:YES];
      dropDownButton14.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
      NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
      if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
      {
        UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton14.frame.size.width-dropDownButton14.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton14.frame.size.width-dropDownButton14.intrinsicContentSize.width-8.0));
        [dropDownButton14 setTitleEdgeInsets:titleInsets];
        dropDownButton14.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
      }
      [dropDownButton14 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
      [parentScrollView addSubview:dropDownButton14];
      
      [[dropDownButton14 layer] setBorderWidth:0.5f];
      [[dropDownButton14 layer] setBorderColor:[UIColor blackColor].CGColor];
      
      UIImageView *imageView = [[UIImageView alloc] init];
      imageView.frame=CGRectMake(dropDownButton14.frame.size.width-28,filed_Y_Position+14,22,11);
      imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
      [parentScrollView addSubview:imageView];
      
      next_Y_Position = dropDownButton14.frame.origin.y+dropDownButton14.frame.size.height+distance_Y;
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value14_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label14 text] forKey:@"labelName"];
      [tempDictionary setObject:dropDownButton14.titleLabel.text forKey:@"value"];
    }
    [dropDownButton14 setBackgroundColor:[UIColor clearColor]];
    
    numberofFields++;
    [validationsArray addObject:tempDictionary];
  }
  //Field 15
  if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field15_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
  {
    NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
    textfieldTitle_Label15 = [[UILabel alloc] init];
    textfieldTitle_Label15.frame = CGRectMake(inputField_X_Position, next_Y_Position,parentScrollView.frame.size.width-10, 30);
    textfieldTitle_Label15.backgroundColor = [UIColor clearColor];
    textfieldTitle_Label15.lineBreakMode = NSLineBreakByTruncatingTail;
    
    NSString *labelStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_label15_text",propertyFileName,[NSBundle mainBundle], nil)];
    
    if (labelStr)
      textfieldTitle_Label15.text =labelStr;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_label15_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      // Properties for label TextColor.
      
      NSArray *label7_TextColor;
      
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label15_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label15_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      
      else
        label7_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
      
      if (label7_TextColor)
        textfieldTitle_Label15.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      // Properties for label Textstyle.
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label15_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label15_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      
      // Properties for label Font size.
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label15_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label15_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label15.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label15.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    else{
      //Default Properties for label textcolor
      
      NSArray *label7_TextColor ;
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil))
        label7_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_color",propertyFileName,[NSBundle mainBundle], nil)];
      else
        label7_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
      if (label7_TextColor)
        textfieldTitle_Label15.textColor = [UIColor colorWithRed:[[label7_TextColor objectAtIndex:0] floatValue] green:[[label7_TextColor objectAtIndex:1] floatValue] blue:[[label7_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
      
      //Default Properties for label textStyle
      
      NSString *textStyle;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil))
        textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_style",propertyFileName,[NSBundle mainBundle], nil);
      else
        textStyle = application_default_text_style;
      
      //Default Properties for label fontSize
      
      NSString *fontSize;
      if(NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil))
        fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_label_text_size",propertyFileName,[NSBundle mainBundle], nil);
      else
        fontSize = application_default_text_size;
      
      if ([textStyle isEqualToString:TEXT_STYLE_0])
        textfieldTitle_Label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_1])
        textfieldTitle_Label15.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
      
      else if ([textStyle isEqualToString:TEXT_STYLE_2])
        textfieldTitle_Label15.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
      
      else
        textfieldTitle_Label15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      
    }
    [parentScrollView addSubview:textfieldTitle_Label15];
    
    //TextField 11
    filed_Y_Position = textfieldTitle_Label15.frame.origin.y+textfieldTitle_Label15.frame.size.height+distance_Y;
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value15_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame){
      
      textfield15 = [[CustomTextField alloc] init];
      textfield15.frame = CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40);
      [textfield15 setBorderStyle:UITextBorderStyleNone];
      [self SetTextFieldBorder:textfield15];
      
      NSString *valueStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_value15_hint",propertyFileName,[NSBundle mainBundle], nil)];
      
      if (valueStr)
        textfield15.placeholder=valueStr;
      
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_value15_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        //Properties for textfield Hint text color
        
        if ([textfield11 respondsToSelector:@selector(setAttributedPlaceholder:)]) {
          NSArray *textFieldHintColor;
          
          if (NSLocalizedStringFromTableInBundle(@"parent_template4_value15_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value15_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_hint_color",propertyFileName,[NSBundle mainBundle], nil))
            textFieldHintColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value_hint_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else
            textFieldHintColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
          
          if (textFieldHintColor)
            textfield15.attributedPlaceholder =[[NSAttributedString alloc] initWithString:valueStr attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:[[textFieldHintColor objectAtIndex:0] floatValue] green:[[textFieldHintColor objectAtIndex:1] floatValue] blue:[[textFieldHintColor objectAtIndex:2] floatValue] alpha:1.0f]}];
        }
        
        // Properties for textField TextColor.
        
        NSArray *textField7TextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_value15_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_value15_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          textField7TextColor = [ValidationsClass colorWithHexString:application_default_value_text_color];
        
        if (textField7TextColor)
          textfield15.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        // Properties for textField Textstyle.
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value15_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_value15_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_value_text_style;
        
        
        // Properties for textField Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_value1_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield15.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield15.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for textfiled textcolor
        
        NSArray *textField7TextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          textField7TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          textField7TextColor=[ValidationsClass colorWithHexString:application_default_value_text_color];
        if (textField7TextColor)
          textfield15.textColor = [UIColor colorWithRed:[[textField7TextColor objectAtIndex:0] floatValue] green:[[textField7TextColor objectAtIndex:1] floatValue] blue:[[textField7TextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for textfiled textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_text_style;
        
        //Default Properties for textfiled fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_input_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          textfield15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          textfield15.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          textfield15.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          textfield15.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
      }
      NSString *keyboardType;
      [textfield15 setTag:localTag];
      if (NSLocalizedStringFromTableInBundle(@"parent_template4_value15_keyboard_type",propertyFileName,[NSBundle mainBundle], nil))
        keyboardType = NSLocalizedStringFromTableInBundle(@"parent_template4_value15_keyboard_type",propertyFileName,[NSBundle mainBundle], nil);
      else
        keyboardType = application_default_value_keyboard_type;
      
      if ([keyboardType isEqualToString:DEFAULTKEYBOARDTYPE_0])
        textfield15 .keyboardType = UIKeyboardTypeDefault;
      else if ([keyboardType isEqualToString:DECIMALKEYBOARDTYPE_1])
        textfield15.keyboardType = UIKeyboardTypeDecimalPad;
      else if ([keyboardType isEqualToString:PHONENUMBERKEYBOARDTYPE_2])
        textfield15.keyboardType = UIKeyboardTypePhonePad;
      else if ([keyboardType isEqualToString:NUMBERWITHSECUREKEYBOARDTYPE_3]){
        textfield15.secureTextEntry=YES;
        textfield15.keyboardType = UIKeyboardTypeNumberPad;
      }
      else if ([keyboardType isEqualToString:DATEOFBIRTHKEYBOARDTYPE])
      {
        datePicker1 = [[UIDatePicker alloc] init];
        datePicker1.datePickerMode = UIDatePickerModeDate;
        [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.textfield15.inputView = datePicker1;
        datePicker1.tag = textfield15.tag;
      }
      
      else
        textfield15.keyboardType = UIKeyboardTypeDefault;
      
      textfield15.inputAccessoryView = numberToolbar;
      textfield15.delegate = self;
      //localTag++;
        if(isDebugging){
            [textfield15 setText:@"test zone"];
        }
      [parentScrollView addSubview:textfield15];
      
      next_Y_Position = textfield15.frame.origin.y+textfield15.frame.size.height+distance_Y;
      
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value15_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_value15_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label15 text] forKey:@"labelName"];
      
      if (userApplicationMode == 0 && phoneNumberStr.length > 0 && ([NSLocalizedStringFromTableInBundle(@"parent_template4_value15_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER] || [NSLocalizedStringFromTableInBundle(@"parent_template4_value15_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER])){
        textfield15.text=phoneNumberStr;
        textfield15.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield15.text forKey:@"value"];
      }
      else if ((userApplicationMode == 1 && [multiUserPhnStr length]>0 &&[NSLocalizedStringFromTableInBundle(@"parent_template4_value15_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER]))
      {
        textfield15.text=[[NSUserDefaults standardUserDefaults] objectForKey:SAVEDMOBILENUMBER];
        textfield15.userInteractionEnabled=NO;
        [tempDictionary setObject:PARAMETER9 forKey:@"value_inputtype"];
        [tempDictionary setObject:textfield15.text forKey:@"value"];
      }
      else
      {
        [tempDictionary setObject:[textfield15 text] forKey:@"value"];
        [tempDictionary setObject:([NSLocalizedStringFromTableInBundle(@"parent_template4_value15_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:SAVEDMOBILENUMBER] ||[NSLocalizedStringFromTableInBundle(@"parent_template4_value15_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:ACTIVATIONMOBILENUMBER]|| [NSLocalizedStringFromTableInBundle(@"parent_template4_value15_inputtype",propertyFileName,[NSBundle mainBundle], nil) isEqualToString:PARAMETER9])?PARAMETER9: NSLocalizedStringFromTableInBundle(@"parent_template4_value15_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      }
    }
    else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
      dropDownButton15 = [UIButton buttonWithType:UIButtonTypeSystem];
      [dropDownButton15 setFrame:CGRectMake(filed_X_Position, filed_Y_Position, parentScrollView.frame.size.width-10,40)];
      
      UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, 8.0f, 0.0f, -8.0f);
      [dropDownButton15 setTitleEdgeInsets:titleInsets];
      
      NSString *dropDownStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_hint",propertyFileName,[NSBundle mainBundle], nil)];
      if (dropDownStr) {
        [dropDownButton15 setTitle:dropDownStr forState:UIControlStateNormal];
        [dropDownButton15 setTitle:dropDownStr forState:UIControlStateHighlighted];
      }
      dropDownButton15.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
      [dropDownButton15 setTag:15];
      //            localTag++;
      if ([NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        // Properties for dropdown TextColor.
        
        NSArray *dropDownTextColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        
        else
          dropDownTextColor = [ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        
        if (dropDownTextColor)
          [dropDownButton15 setTitleColor:[UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
        
        // Properties for dropdown Textstyle.
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        // Properties for dropdown Font size.
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton15.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton15.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton15.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton15.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      else
      {
        //Default Properties for dropdown textcolor
        NSArray *dropDownTextColor ;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil))
          dropDownTextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
          dropDownTextColor=[ValidationsClass colorWithHexString:application_default_drop_down_value_text_color];
        if (dropDownTextColor)
          dropDownButton15.titleLabel.textColor = [UIColor colorWithRed:[[dropDownTextColor objectAtIndex:0] floatValue] green:[[dropDownTextColor objectAtIndex:1] floatValue] blue:[[dropDownTextColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        //Default Properties for dropdown textStyle
        
        NSString *textStyle;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil))
          textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_style",propertyFileName,[NSBundle mainBundle], nil);
        else
          textStyle = application_default_drop_down_value_text_style;
        
        //Default Properties for dropdown fontSize
        
        NSString *fontSize;
        if(NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil))
          fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value_text_size",propertyFileName,[NSBundle mainBundle], nil);
        else
          fontSize = application_default_drop_down_value_text_size;
        
        if ([textStyle isEqualToString:TEXT_STYLE_0])
          dropDownButton15.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_1])
          dropDownButton15.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
        
        else if ([textStyle isEqualToString:TEXT_STYLE_2])
          dropDownButton15.titleLabel.font= [UIFont italicSystemFontOfSize:[fontSize floatValue]];
        
        else
          dropDownButton15.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
      }
      
      [dropDownButton15 setExclusiveTouch:YES];
      dropDownButton15.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
      NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
      if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
      {
        UIEdgeInsets titleInsets=UIEdgeInsetsMake(0.0f, -(dropDownButton15.frame.size.width-dropDownButton15.intrinsicContentSize.width-8.0), 0.0f,(dropDownButton15.frame.size.width-dropDownButton15.intrinsicContentSize.width-8.0));
        [dropDownButton15 setTitleEdgeInsets:titleInsets];
        dropDownButton15.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
      }
      [dropDownButton15 addTarget:self action:@selector(dropdownSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
      [parentScrollView addSubview:dropDownButton15];
      
      [[dropDownButton15 layer] setBorderWidth:0.5f];
      [[dropDownButton15 layer] setBorderColor:[UIColor blackColor].CGColor];
      
      UIImageView *imageView = [[UIImageView alloc] init];
      imageView.frame=CGRectMake(dropDownButton15.frame.size.width-28,filed_Y_Position+14,22,11);
      imageView.image = [UIImage imageNamed:DROPDOWN_IMAGE_NAME];
      [parentScrollView addSubview:imageView];
      
      next_Y_Position = dropDownButton15.frame.origin.y+dropDownButton15.frame.size.height+distance_Y;
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_validation_type",propertyFileName,[NSBundle mainBundle], nil) forKey:@"validation_type"];
      [tempDictionary setObject:NSLocalizedStringFromTableInBundle(@"parent_template4_drop_down_value15_inputtype",propertyFileName,[NSBundle mainBundle], nil) forKey:@"value_inputtype"];
      [tempDictionary setObject:[textfieldTitle_Label15 text] forKey:@"labelName"];
      [tempDictionary setObject:dropDownButton15.titleLabel.text forKey:@"value"];
    }
    [dropDownButton15 setBackgroundColor:[UIColor clearColor]];
    
    numberofFields++;
    [validationsArray addObject:tempDictionary];
  }
  
  [parentScrollView setContentSize:CGSizeMake(SCREEN_WIDTH-60, (numberofFields)*88)];
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_field11_visibilty",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
           [self addButtons];

}

-(void)viewWillAppear:(BOOL)animated
{
    [self reloadOptionMenu];
    [super viewWillAppear:animated];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromUploadDocsScreen"]){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFromUploadDocsScreen"];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    //    [super viewWillDisappear:YES];
}

#pragma mark - Options Menu Creation.
/**
 * This method is used to set reload OptionsMenu for ParentTemplate4.
 */
-(void) reloadOptionMenu
{
    alertview_Type = NSLocalizedStringFromTableInBundle(@"application_display_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    [self loadOptionsMenu];
}

/**
 * This method is used to set add OptionsMenu for ParentTemplate4.
 */
-(void)loadOptionsMenu
{
    optionsMenu = [[OptionsMenu alloc] initWithFrame:CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS) withPropertyFileName:nil withIndex:0];
    optionsMenu.delegate = self;
    [self.view addSubview:optionsMenu];
}

#pragma mark - ParentTemplate4 UICOntraints Creation.
/**
 * This method is used to set add Buttons for ParentTemplate4.
 */
-(void)addButtons
{

    button1=[UIButton buttonWithType:UIButtonTypeCustom];
  
    button1.frame=CGRectMake(label_X_Position*2+10, CGRectGetMaxY(parentScrollView.frame) +10, (SCREEN_WIDTH-70)/2, 45);
    
    button2 = [UIButton buttonWithType:UIButtonTypeCustom];
  
    button2.frame=CGRectMake(label_X_Position*2+((SCREEN_WIDTH-50)/2)+5, CGRectGetMaxY(parentScrollView.frame)+10, (SCREEN_WIDTH-70)/2, 45);
    
    button3=[UIButton buttonWithType:UIButtonTypeCustom];
  
    button3.frame=CGRectMake(label_X_Position*2, CGRectGetMaxY(button1.frame)+5, SCREEN_WIDTH-45, 45);
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_button1_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_button1_text_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_button1_text",propertyFileName,[NSBundle mainBundle], nil)];
            if (buttonStr)
                [button1 setTitle:buttonStr forState:UIControlStateNormal];
        }
        button1.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        // properties For Button backgroundColor
        NSArray *button1_BackgroundColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_button1_background_color",propertyFileName,[NSBundle mainBundle], nil))
            button1_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button1_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button1_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button1_BackgroundColor)
            button1.backgroundColor=[UIColor colorWithRed:[[button1_BackgroundColor objectAtIndex:0] floatValue] green:[[button1_BackgroundColor objectAtIndex:1] floatValue] blue:[[button1_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        if([NSLocalizedStringFromTableInBundle(@"parent_template4_button1_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_button1_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button1_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button_TextColor objectAtIndex:0] floatValue] green:[[button_TextColor objectAtIndex:1] floatValue] blue:[[button_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button1_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_button1_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button1_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_button1_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button_TextColor)
                [button1 setTitleColor:[UIColor colorWithRed:[[button_TextColor objectAtIndex:0] floatValue] green:[[button_TextColor objectAtIndex:1] floatValue] blue:[[button_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button1.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button1.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button1.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        [button1 setTag:11];
        [button1 setExclusiveTouch:YES];
        [button1 addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button1];
    }
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_button2_text_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_button2_text",propertyFileName,[NSBundle mainBundle], nil)];
            if (buttonStr)
                [button2 setTitle:buttonStr forState:UIControlStateNormal];
        }
        
        button2.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        // properties For Button backgroundColor
        NSArray *button2_BackgroundColor;
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
            button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        if (button2_BackgroundColor)
            button2.backgroundColor=[UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        if([NSLocalizedStringFromTableInBundle(@"parent_template4_button2_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button2_TextColor;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_button2_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button2_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button2_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button2_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button2_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button2_TextColor objectAtIndex:0] floatValue] green:[[button2_TextColor objectAtIndex:1] floatValue] blue:[[button2_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button2_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_button2_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button2_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_button2_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button_TextColor)
                [button2 setTitleColor:[UIColor colorWithRed:[[button_TextColor objectAtIndex:0] floatValue] green:[[button_TextColor objectAtIndex:1] floatValue] blue:[[button_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize=application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button2.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button2.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button2.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        
        [button2 setTag:12];
        [button2 setExclusiveTouch:YES];
        [button2 addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:button2];
    }
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_button3_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        
        
        if ([NSLocalizedStringFromTableInBundle(@"parent_template4_button3_text_visibility",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(@"parent_template4_button3_text",propertyFileName,[NSBundle mainBundle], nil)];
            if (buttonStr)
                [button3 setTitle:buttonStr forState:UIControlStateNormal];
        }

        button3.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        // properties For Button backgroundColor
        
        NSArray *button3_BackgroundColor;
        
        if (NSLocalizedStringFromTableInBundle(@"parent_template4_button3_background_color",propertyFileName,[NSBundle mainBundle], nil))
            button3_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button3_background_color",propertyFileName,[NSBundle mainBundle], nil)];
        else
            button3_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
        
        
        if (button3_BackgroundColor)
            button3.backgroundColor=[UIColor colorWithRed:[[button3_BackgroundColor objectAtIndex:0] floatValue] green:[[button3_BackgroundColor objectAtIndex:1] floatValue] blue:[[button3_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f];
        
        if([NSLocalizedStringFromTableInBundle(@"parent_template4_button3_font_attributes_override",propertyFileName,[NSBundle mainBundle], nil)  caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
            // Properties for Button TextColor.
            
            NSArray *button3_TextColor;
            
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_button3_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button3_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else if (NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button3_TextColor = [ValidationsClass colorWithHexString:application_default_button_text_color];
            
            if (button3_TextColor)
                [button3 setTitleColor:[UIColor colorWithRed:[[button3_TextColor objectAtIndex:0] floatValue] green:[[button3_TextColor objectAtIndex:1] floatValue] blue:[[button3_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            // Properties for Button Textstyle.
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button3_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_button3_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            
            // Properties for Button Font size.
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button3_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_button3_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button3.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button3.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else
        {
            //Default Properties for Button textcolor
            
            NSArray *button3_TextColor ;
            if (NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil))
                button3_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_color",propertyFileName,[NSBundle mainBundle], nil)];
            else
                button3_TextColor=[ValidationsClass colorWithHexString:application_default_button_text_color];
            if (button3_TextColor)
                [button3 setTitleColor:[UIColor colorWithRed:[[button3_TextColor objectAtIndex:0] floatValue] green:[[button3_TextColor objectAtIndex:1] floatValue] blue:[[button3_TextColor objectAtIndex:2] floatValue] alpha:1.0f] forState:UIControlStateNormal];
            
            //Default Properties for Button textStyle
            
            NSString *textStyle;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil))
                textStyle = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_style",propertyFileName,[NSBundle mainBundle], nil);
            else
                textStyle = application_default_button_text_style;
            
            //Default Properties for Button fontSize
            
            NSString *fontSize;
            if(NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil))
                fontSize = NSLocalizedStringFromTableInBundle(@"parent_template4_button_text_size",propertyFileName,[NSBundle mainBundle], nil);
            else
                fontSize = application_default_button_text_size;
            
            if ([textStyle isEqualToString:TEXT_STYLE_0])
                button3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_1])
                button3.titleLabel.font = [UIFont boldSystemFontOfSize:[fontSize floatValue]];
            
            else if ([textStyle isEqualToString:TEXT_STYLE_2])
                button3.titleLabel.font = [UIFont italicSystemFontOfSize:[fontSize floatValue]];
            
            else
                button3.titleLabel.font = [UIFont systemFontOfSize:[fontSize floatValue]];
            
        }
        [button3 setTag:13];
        [button3 setExclusiveTouch:YES];
        [button3 addTarget:self action:@selector(button3Action:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:button3];
    }
    
    
    if ([NSLocalizedStringFromTableInBundle(@"parent_template4_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame && [NSLocalizedStringFromTableInBundle(@"parent_template4_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] != NSOrderedSame)
    {
        button1.frame=CGRectMake(label_X_Position*2, CGRectGetMinY(button1.frame), SCREEN_WIDTH-40, 45);
    }
    else if ([NSLocalizedStringFromTableInBundle(@"parent_template4_button1_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] != NSOrderedSame && [NSLocalizedStringFromTableInBundle(@"parent_template4_button2_visibility",propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
    {
        button2.frame=CGRectMake(label_X_Position*2, CGRectGetMinY(button2.frame), SCREEN_WIDTH-40, 45);
    }

}

#pragma mark - Button Action methods.
/**
 * This method is used to set button1 action of ParentTemplate4.
 @prameters are defined in button action those are
 * Application display type(ticker,PopUp),validation type,next template,next template PropertyFile,Action type,
 Feature-(processorCode,transaction type).
 */
-(void)button1Action:(id)sender
{
    [activeField resignFirstResponder];
    
    actionType = NSLocalizedStringFromTableInBundle(@"parent_template4_button1_action_type",propertyFileName,[NSBundle mainBundle], nil);
    alertviewType = NSLocalizedStringFromTableInBundle(@"application_display_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplatePropertyFileName =  NSLocalizedStringFromTableInBundle(@"parent_template4_button1_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplateName = NSLocalizedStringFromTableInBundle(@"parent_template4_button1_next_template",propertyFileName,[NSBundle mainBundle], nil);
    validationType = NSLocalizedStringFromTableInBundle(@"application_validation_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    
    NSString *data = NSLocalizedStringFromTableInBundle(@"parent_template4_button1_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    processorCode = [Template getProcessorCodeWithData:data];
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    
    if (transactionType) {
        [selfRegisDataDict setObject:transactionType forKey:PARAMETER13];
    }
    [selfRegisDataDict setObject:processorCode forKey:PARAMETER15];
    NSLog(@"Process data is..%@",[selfRegisDataDict description]);
    
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplateName nextTemplatePropertyFile:nextTemplatePropertyFileName currentTemplatePropertyFile:propertyFileName ProcessorCode:processorCode Dictionary:selfRegisDataDict contentArray:validationsArray alertType:alertviewType ValidationType:validationType inClass:self.view withApiParameter:nil withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:PARENT_TEMPLATE_4];
    obj.selector = @selector(processData);
    obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PT4_BUTTON1_ACTION object:obj];

}
/**
 * This method is used to set button2 action of ParentTemplate4.
 */
-(void)button2Action:(id)sender
{
    [activeField resignFirstResponder];

    if([propertyFileName isEqualToString:@"SignupPT4"]){
        NSString *msgString = @"";
        BOOL matchesLength = YES;
        NSInteger textLength = textfield10.text.length;
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        if ([textfield10.text rangeOfCharacterFromSet:notDigits].location != NSNotFound)
        {
            PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@", [Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_invalid_ID_number", nil)]] withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [self.view addSubview:popup];
            return;
        }
        if([docType isEqualToString:@"National ID"] &&
            textLength != 11){
            matchesLength = NO;
            msgString = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_national_id_length_mismatch", nil)]];
        }
        if([docType isEqualToString:@"Passport"] && textLength != 8){
            matchesLength = NO;
            msgString = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_passport_length_mismatch", nil)]];
        }
        if([docType isEqualToString:@"Family Card"] && textLength != 6){
            matchesLength = NO;
            msgString = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_family_card_length_mismatch", nil)]];
        }
        if(!matchesLength){
            PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:msgString withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [self.view addSubview:popup];
            return;
        }
    }
        actionType = NSLocalizedStringFromTableInBundle(@"parent_template4_button2_action_type", propertyFileName, [NSBundle mainBundle], nil);
        NSLog(@"actionType: %@", actionType);
        NSString *data = NSLocalizedStringFromTableInBundle(@"parent_template4_button2_web_service_api_name",propertyFileName, [NSBundle mainBundle], nil);
        processorCode = [Template getProcessorCodeWithData:data];
        NSString *transactionType = [Template getTransactionCodeWithData:data];
        
        if (transactionType) {
            [selfRegisDataDict setObject:transactionType forKey:PARAMETER13];
        }
        [selfRegisDataDict setObject:processorCode forKey:PARAMETER15];
        
        alertviewType = NSLocalizedStringFromTableInBundle(@"application_display_type",@"GeneralSettings",[NSBundle mainBundle], nil);
        NSLog(@"alertviewType: %@",alertviewType);
        
        nextTemplatePropertyFileName =  NSLocalizedStringFromTableInBundle(@"parent_template4_button2_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
        NSLog(@"nextTemplatePropertyFileName: %@",nextTemplatePropertyFileName);
        
        nextTemplateName = NSLocalizedStringFromTableInBundle(@"parent_template4_button2_next_template",propertyFileName,[NSBundle mainBundle], nil);
        NSLog(@"nextTemplateName: %@",nextTemplateName);
    
        validationType = NSLocalizedStringFromTableInBundle(@"application_validation_type",@"GeneralSettings",[NSBundle mainBundle], nil);
        NSLog(@"validationType: %@",validationType);
        
        NSLog(@"Validation Array :%@", validationsArray);
        
        Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplateName nextTemplatePropertyFile:nextTemplatePropertyFileName currentTemplatePropertyFile:propertyFileName ProcessorCode:processorCode Dictionary:selfRegisDataDict contentArray:validationsArray alertType:alertviewType ValidationType:validationType inClass:self.view withApiParameter:nil withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:PARENT_TEMPLATE_4];
        obj.selector = @selector(processData);
        obj.target = self;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:textfield1.text forKey:@"LastUsedMobileNumberForReg"];
    [[NSNotificationCenter defaultCenter] postNotificationName:PT4_BUTTON2_ACTION object:obj];
}

/**
 * This method is used to set button2 action of ParentTemplate4.
 */
-(void)button3Action:(id)sender
{
    [activeField resignFirstResponder];
    actionType = NSLocalizedStringFromTableInBundle(@"parent_template4_button3_action_type",propertyFileName,[NSBundle mainBundle], nil);
    alertviewType = NSLocalizedStringFromTableInBundle(@"application_display_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    nextTemplatePropertyFileName =  NSLocalizedStringFromTableInBundle(@"parent_template4_button3_next_template_properties_file",propertyFileName,[NSBundle mainBundle], nil);
    nextTemplateName = NSLocalizedStringFromTableInBundle(@"parent_template4_button3_next_template",propertyFileName,[NSBundle mainBundle], nil);
    validationType = NSLocalizedStringFromTableInBundle(@"application_validation_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    NSString *data = NSLocalizedStringFromTableInBundle(@"parent_template4_button3_web_service_api_name",propertyFileName,[NSBundle mainBundle], nil);
    processorCode = [Template getProcessorCodeWithData:data];
    
    NSString *transactionType = [Template getTransactionCodeWithData:data];
    
    if (transactionType){
        [selfRegisDataDict setObject:transactionType forKey:PARAMETER13];
    }
    [selfRegisDataDict setObject:processorCode forKey:PARAMETER15];
    
    Template *obj = [Template initWithactionType:actionType nextTemplate:nextTemplateName nextTemplatePropertyFile:nextTemplatePropertyFileName currentTemplatePropertyFile:propertyFileName ProcessorCode:processorCode Dictionary:selfRegisDataDict?selfRegisDataDict:nil contentArray:validationsArray alertType:alertviewType ValidationType:validationType inClass:self.view withApiParameter:nil withLableValidation:nil withSelectedLocalProcessorCode:nil withSelectedIndex:-1 withTransactionType:nil withCurrentClass:PARENT_TEMPLATE_4];
        obj.selector = @selector(processData);
        obj.target = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:PT4_BUTTON3_ACTION object:obj];
}

/**
 * This method is used to set baseviewcontroller Process data notification.
 */
-(void) processData
{
    NSLog(@"processing data in pt4..");
    //Process Data
    if ([local_processorCode isEqualToString:PROCESSOR_CODE_IMAGE_CROP]) {
        NSLog(@"Test things here..");
        /*
         * This method is used to add Camera interface when user selects profile pic.
         */
        [self.view endEditing:YES];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate:self cancelButtonTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancelbutton", nil)] destructiveButtonTitle: nil otherButtonTitles:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_take_Photo", nil)],[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_choose_from_gallery", nil)], nil];
        [actionSheet showInView:self.view];
    }
}


#pragma mark - DRopDown Selection
/*
 * This method is used to add ParentTemplate4 dropdown button action.
 */
-(void)dropdownSelectionAction:(id)sender
{
    [self.view endEditing:YES];
    
    //Changed Implementation
    btn = (UIButton *)sender;
    
    NSLog(@"Sender : %@", sender);
    UIButton *btn = (UIButton*) sender;
    if([propertyFileName isEqualToString:@"SignupPT4"] && btn.tag == 13){
        if(!districts){
            PopUpTemplate2 *popup = [[PopUpTemplate2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) withPropertyName:nil andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:2 withTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_alert", nil)]] andMessage:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_hint_governorate", nil)]] withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [self.view addSubview:popup];
            return;
        }
    }
    
//    NSString *keyVal=[NSString stringWithFormat:@"parent_template4_drop_down_value%ld_hint",(long)btn.tag];
//    NSString *buttonStr=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(keyVal,propertyFileName,[NSBundle mainBundle], nil)];
//    if ([btn.titleLabel.text isEqualToString:buttonStr])
//    {
//        NSString *textToSpeech=[NSString stringWithFormat:@"parent_template4_drop_down_value%ld_text_to_speech",(long)btn.tag];
//        NSString *text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(textToSpeech, propertyFileName, [NSBundle mainBundle], nil)];
//        if (!([text caseInsensitiveCompare:@""]==NSOrderedSame) && ! ([text caseInsensitiveCompare:textToSpeech]==NSOrderedSame))
//        {
//            [[SpeechManager sharedManager] speakWithString:text];
//        }
//    }

    dropdownString = [NSString stringWithFormat:@"parent_template4_drop_down_value%ld",(long)btn.tag];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:@"UpdateData" object:nil];
    DatabaseManager *dbManager = [[DatabaseManager alloc] initWithDatabaseName:DATABASE_NAME];
    if([dropdownString isEqualToString:@"parent_template4_drop_down_value12"]){
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
        {
            dropDownDataArray = [[NSArray alloc] initWithArray:[self getGovernorates]];
        }else{
            dropDownDataArray = [[NSArray alloc] initWithArray:[self getGovernorates]];
        }
    }else if([dropdownString isEqualToString:@"parent_template4_drop_down_value13"]){
        NSLog(@"Adding districts..");
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:userLanguage] isEqualToString:@"Arabic"])
        {
            dropDownDataArray = [[NSArray alloc] initWithArray:[self getDistricts]];
        }else{
            dropDownDataArray = [[NSArray alloc] initWithArray:[self getDistricts]];
        }
    }else{
        dropDownDataArray = [[NSArray alloc] initWithArray:[dbManager getAllDropDownDetailsForKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)]];
    }
    nextTemplateProperty =  NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template_properties_file"], propertyFileName, [NSBundle mainBundle], nil);
    
    if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"1"]){
         nextTemplate=CHILD_TEMPLATE_3;
    }
    else if ([NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_next_template"],propertyFileName,[NSBundle mainBundle], nil) isEqualToString:@"2"]){
        nextTemplate=POPUP_TEMPLATE_8;
    }

        if (![nextTemplateProperty isEqualToString:@""] && ![nextTemplate isEqualToString:@""])
        {
            Class nextClass = NSClassFromString(nextTemplate);
            id object = nil;
            
            if ([nextClass instancesRespondToSelector:@selector(initWithNibName:bundle:withPropertyName:hasDidSelectFunction:withDataDictionary:withDataArray:withPIN:withProcessorCode:withType:fromView:insideView:)])
            {
                object = [[nextClass alloc] initWithNibName:nextTemplate bundle:nil withPropertyName:nextTemplateProperty hasDidSelectFunction:NO withDataDictionary:nil withDataArray:dropDownDataArray withPIN:nil withProcessorCode:nil withType:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil) fromView:0 insideView:nil];
                [(ChildTemplate3 *)object setBaseSelector:@selector(updateData:)];
                [(ChildTemplate3 *)object setBasetarget:self];
                AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                UINavigationController *navCntrl = (UINavigationController *)delegate.window.rootViewController;
                if (navCntrl && [navCntrl isKindOfClass:[UINavigationController class]])
                {
                    [navCntrl pushViewController:object animated:NO];
                }
            }
            else if ([nextClass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray:withSubIndex:)])
            {
                object = [[nextClass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:nextTemplateProperty andDelegate:self withDataDictionary:nil withProcessorCode:nil withTag:1 withTitle:nil andMessage:nil  withSelectedIndexPath:0 withDataArray:dropDownDataArray?dropDownDataArray:nil withSubIndex:0];
                [object setBasetarget:self];
                [object setBaseSelector:@selector(updateData:)];
                [self.view addSubview:(UIView *)object];
            }
        }

}

#pragma mark - DropDown Data update.
/*
 * This method is used to add Parenttemplate4 dropdown updated data to particular field.
 */
- (void)updateData:(id)object
{
    if ([[object objectForKey:DROP_DOWN_TYPE] isEqualToString:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_data_webservice_name"],propertyFileName,[NSBundle mainBundle], nil)])
    {
        NSString *dropDownValue = [object objectForKey:DROP_DOWN_TYPE_DESC];
        if (dropDownValue.length == 0) {
            dropDownValue = [object objectForKey:DROP_DOWN_TYPE_NAME];
        }
        [btn setTitle:dropDownValue forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitle:dropDownValue forState:UIControlStateHighlighted];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [selfRegisDataDict setObject:[object objectForKey:DROP_DOWN_TYPE_NAME] forKey:NSLocalizedStringFromTableInBundle([dropdownString stringByAppendingString:@"_inputtype"],propertyFileName,[NSBundle mainBundle], nil)];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateData" object:nil];
}

#pragma mark - Options Menu Delegate Method.
/**
 * This method is used to set Delegate Method of Option menu For ParentTemplate4.
 *@Param type - Options menu (Terms And conditions,Set language etc)
 *Based on User selected options menu feature will be shown Next View with Highilighted.
 *  Options menu feature Name with Next Template and NextTemplate Property file.
 */
-(void)optionsMenu_didSelectRowAtIndexPath:(NSInteger)indexpath withPropertyFile:(NSString *)propertyFile nextTemplate:(NSString *)nextTemplateName1 andProcessorCode:(NSString *)processorCode1
{
    [self.view endEditing:YES];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    optionsMenu.frame = CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
    bgLbl.alpha=0.0;
    [UIView commitAnimations];
    
    if (![propertyFile isEqualToString:@""] && ![nextTemplateName1 isEqualToString:@""])
    {
        Class myclass = NSClassFromString(nextTemplateName1);
        id obj = nil;
        if ([myclass instancesRespondToSelector:@selector(initWithNibName:bundle:withSelectedIndex:fromView:withFromView:withPropertyFile:withProcessorCode:dataArray:dataDictionary:)])
        {
            obj = [[myclass alloc] initWithNibName:nextTemplateName bundle:nil withSelectedIndex:(int)indexpath fromView:0 withFromView:propertyFile withPropertyFile:propertyFile withProcessorCode:processorCode dataArray:nil dataDictionary:nil];
            [self.navigationController pushViewController:(UIViewController*)obj animated:NO];
        }
        else if([myclass instancesRespondToSelector:@selector(initWithFrame:withPropertyName:andDelegate:withDataDictionary:withProcessorCode:withTag:withTitle:andMessage:withSelectedIndexPath:withDataArray: withSubIndex:)])
        {
            obj = [[myclass alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT) withPropertyName:propertyFile andDelegate:self withDataDictionary:webServiceRequestInputDetails withProcessorCode:nil withTag:-1 withTitle:nil andMessage:nil withSelectedIndexPath:0 withDataArray:nil withSubIndex:0];
            [self.view addSubview:(UIView *)obj];
        }
    }
}

#pragma mark  - TAPGESTURE FOR HIDING SIDE MENU VIEW
/**
 * This method is used to set Delegate Method used for Hiding  Option menu From View.
 */
-(void)optionsMenu_tapGestureCalled
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    optionsMenu.frame = CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
    bgLbl.alpha=0.0;
    [UIView commitAnimations];
}


#pragma mark - MENU BUTTON ACTION.
/**
 * This method is used to set Delegate Method of PageHeaderView Button Action(Pop to Previous view).
 */
- (void)menuBtn_Action
{
    [self.view endEditing:YES];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if (optionsMenu.frame.origin.x == 0)
    {
        optionsMenu.frame = CGRectMake(-SCREEN_WIDTH, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
        bgLbl.alpha=0.0;
    }
    else
    {
        optionsMenu.frame = CGRectMake(0, SIDE_POPUP_VIEW_YPOS, SCREEN_WIDTH, SCREEN_HEIGHT - SIDE_POPUP_VIEW_YPOS);
        bgLbl.alpha=0.4;
    }
    [ UIView commitAnimations ];
}


#pragma mark - UITextFieldDelegate -
/**
 * This method is used to keybord show and hide method(resign,become responder).
 */
- (void)textFieldDidBeginEditing:(CustomTextField *)textField
{
    activeField = textField;
    if (next_Y_Position>140){
        CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y);
        [parentScrollView setContentOffset:scrollPoint animated:YES];
    }
  
  
    
//        if ([textField.text isEqualToString:@""] && textField.text.length == 0)
//        {
//            NSString *txtFieldNum=[NSString stringWithFormat:@"parent_template4_value%ld_text_to_speech",(textField.tag-100)+1];
//            NSString *text=[Localization languageSelectedStringForKey:NSLocalizedStringFromTableInBundle(txtFieldNum, propertyFileName, [NSBundle mainBundle], nil)];
//        
//            
//            if (!([text caseInsensitiveCompare:@""]==NSOrderedSame) && !([text caseInsensitiveCompare:txtFieldNum]==NSOrderedSame))
//            {
//                [[SpeechManager sharedManager] speakWithString:text];
//            }
//        }
}

- (void)textFieldDidEndEditing:(CustomTextField *)textField
{
    [textField resignFirstResponder];
    if(![propertyFileName isEqualToString:@"SignupPT4"]){
        [parentScrollView setContentOffset:CGPointZero animated:YES];
    }
   // [[SpeechManager sharedManager] stopSpeaking];
}

- (BOOL)textFieldShouldBeginEditing:(CustomTextField *)textField
{
    
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    
    if ([[userDefault valueForKey:userLanguage] isEqualToString:ar])
    {
        textField.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        textField.textAlignment=NSTextAlignmentLeft;
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(CustomTextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(CustomTextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance =(SCREEN_HEIGHT>=667)?(-60):(-140);
    const float movementDuration = 0.3f;
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    parentScrollView.frame = CGRectOffset(parentScrollView.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark - Toolbar methods
/**
 * This method is used to cancel button action of tool bar.
 */
-(void)cancelNumberPad
{
    [activeField resignFirstResponder];
    activeField.text = @"";
}
/**
 * This method is used to ok button action of tool bar.
 */
-(void)doneWithNumberPad
{
    if ([activeField isFirstResponder])
        [activeField resignFirstResponder];
    
    if ([activeField.inputView isKindOfClass:[UIDatePicker class]])
    {
        NSLog(@"%@",datePicker1.date);
        NSLog(@"%@",activeField.placeholder);
        
        [self datePickerValueChanged:(UIDatePicker *)activeField.inputView];
    }
}

#pragma mark - Date picker method.
/**
 * This method is used For open date picker.
 */
-(void) datePickerValueChanged:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)sender;
    int viewTag = (int)picker.tag;
    NSLog(@"View Tag is....%d",viewTag);

    //remove maximum date for ID expiry date field
    if(viewTag != 110){
        [picker setMaximumDate:[NSDate date]];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    
    [dateFormat setDateFormat:SETDATEFORMATDATEMONTHYEAR];
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    
    [(CustomTextField *)[parentScrollView viewWithTag:viewTag] setText:dateString];
}
#pragma mark UIAction Sheet Delegate
/**
 * This method is used to set Open camera when user tap on Upload button.
 *@param type - Select From Options(Take photo and Select From gallery).
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex)
    {
        case 0:
            [self scanPicture:1];
            break;
        case 1:
            [self scanPicture:2];
            break;
        default:
            break;
    }
}


- (void)scanPicture : (int)type
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.allowsEditing = YES;
    controller.delegate = self;
    if(type == 1)
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
            controller.mediaTypes = [NSArray arrayWithObject:(NSString*)kUTTypeImage];
            [self.navigationController presentViewController: controller animated: YES completion: nil];
        }
        else
        {
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_consumer_client", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_no_camera_found_in_this_device", nil)]] delegate:nil cancelButtonTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] otherButtonTitles:nil, nil];
//            [alert show];
            
           UIAlertController *alert= [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_consumer_client", nil)]] message:[NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_no_camera_found_in_this_device", nil)]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:[Localization languageSelectedStringForKey:NSLocalizedString(@"label_id_cancel", nil)] style:UIAlertActionStyleCancel handler:nil]];
            UIViewController *topController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
            while (topController.presentedViewController)
            {
                topController = topController.presentedViewController;
            }
            [topController presentViewController:alert animated:NO completion:nil];
        }
    }
    else if (type == 2)
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
        {
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
            controller.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
            [self.navigationController presentViewController: controller animated: YES completion: nil];
        }
        else if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeSavedPhotosAlbum])
        {
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeSavedPhotosAlbum];
            controller.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
            [self.navigationController presentViewController: controller animated: YES completion: nil];
        }
    }
}

#pragma mark - UIImagePicker Delegate methods.
/**
 * This method is used to set by using this delegate Open The camera Source Type.
 *@param type- Image Source Type(JPEG or PNG)
 *           - Image Size(KB).
 */
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSData *imageData=nil;
    NSString *base64ImageData;
    
     NSString *imageTypeStr=NSLocalizedStringFromTableInBundle(@"application_upload_image_type",@"GeneralSettings",[NSBundle mainBundle], nil);
    
    // Convert an base64 Encoding Value.
    if ([[imageTypeStr uppercaseString] isEqualToString:@"JPEG"]) {
        imageData= UIImageJPEGRepresentation(chosenImage, 0.2);
        [[UIPasteboard generalPasteboard] setData:imageData forPasteboardType:(id)kUTTypeJPEG];
    }
    else if ([imageTypeStr isEqualToString:@"PNG"]) {
        imageData =UIImagePNGRepresentation(chosenImage);
        [[UIPasteboard generalPasteboard] setData:imageData forPasteboardType:(id)kUTTypePNG];
    }
//    else if ([imageTypeStr isEqualToString:@""])
//    {
//        imageData = UIImageJPEGRepresentation(chosenImage, 0.2);
//    }
    else
    {
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10010", nil)];
        NSString *replaceString = @"<size>";
        NSString *message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:imageTypeStr];
        
        // Backgroundcolor of toast.
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        
        // textColor of toast
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        [self.view makeToast:message duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    }
    
    
    NSString *imageSizeStr=[NSByteCountFormatter stringFromByteCount:imageData.length countStyle:NSByteCountFormatterCountStyleFile];
        
    if ([[[imageSizeStr componentsSeparatedByString:@" KB"] objectAtIndex:0] integerValue] < [NSLocalizedStringFromTableInBundle(@"application_upload_image_size",@"GeneralSettings",[NSBundle mainBundle], nil) integerValue])
    {
        base64ImageData = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        NSLog(@"Base 64 image data is..%@",base64ImageData);
        [selfRegisDataDict setObject:base64ImageData forKey:PARAMETER19];
    }
    else{
        NSString *localizedString = [Localization languageSelectedStringForKey:NSLocalizedString(@"internal_10011", nil)];
        NSString *replaceString = @"<size>";
        NSString *message = [localizedString stringByReplacingOccurrencesOfString:replaceString withString:imageSizeStr];
        
        // Backgroundcolor of toast.
        NSArray *backGroundColor = [ValidationsClass colorWithHexString:application_branding_color_theme];
        
        // textColor of toast
        NSArray *textColor = [ValidationsClass colorWithHexString:application_default_text_color];
        
        [self.view makeToast:message duration:[NSLocalizedStringFromTableInBundle(@"application_ticker_display_time",@"GeneralSettings",[NSBundle mainBundle], nil) floatValue] position:CSToastPositionBottom backgroundColor:[UIColor colorWithRed:[[backGroundColor objectAtIndex:0] floatValue] green:[[backGroundColor objectAtIndex:1] floatValue] blue:[[backGroundColor objectAtIndex:2] floatValue] alpha:1.0f] textColor:[UIColor colorWithRed:[[textColor objectAtIndex:0] floatValue] green:[[textColor objectAtIndex:1] floatValue] blue:[[textColor objectAtIndex:2] floatValue] alpha:1.0f]];
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void) removeViewsWithClassType:(BOOL) clear
{
    NSString *processorStr = local_processorCode;
    NSString *displayText = DigitalKYCUploadString;
    
    if (clear) {
        NSArray *viewsToRemove = [self.view subviews];
        for (UIView *view in viewsToRemove) {
            [view removeFromSuperview];
        }
        [self reloadView];
        [self reloadOptionMenu];
    }
    
    if ([processorStr isEqualToString:PROCESSOR_CODE_SELF_REG_DIGITAL_KYC]) {
        if ([[self.view viewWithTag:999] isKindOfClass:[UIButton class]]) {
            [(UIButton *)[self.view viewWithTag:999] setEnabled:YES];
            [(UIButton *)[self.view viewWithTag:999] setTitle:displayText forState:UIControlStateNormal];
            DigitalKYCUploadString = nil;

        }
        else
        {
            [(UILabel *)[self.view viewWithTag:999] setText:displayText];
            DigitalKYCUploadString = nil;
        }
    }
}
#pragma Mark SetTextField Bottom Border color

-(void)SetTextFieldBorder :(CustomTextField *)textField{
  
  // properties For Button backgroundColor
  NSArray *button2_BackgroundColor;
  if (NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil))
    button2_BackgroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"parent_template4_button2_background_color",propertyFileName,[NSBundle mainBundle], nil)];
  else
    button2_BackgroundColor=[ValidationsClass colorWithHexString:application_default_button_background_color];
  
  CALayer *border = [CALayer layer];
  CGFloat borderWidth = 1;
  
  if (button2_BackgroundColor)
    border.borderColor =[UIColor colorWithRed:[[button2_BackgroundColor objectAtIndex:0] floatValue] green:[[button2_BackgroundColor objectAtIndex:1] floatValue] blue:[[button2_BackgroundColor objectAtIndex:2] floatValue] alpha:1.0f].CGColor;
  
  border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
  border.borderWidth = borderWidth;
  [textField.layer addSublayer:border];
  textField.layer.masksToBounds = YES;
  
}

-(NSArray*)getGovernoratesArabic{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSString *keys[4];
    keys[0] = @"_id";
    keys[1] = @"drop_down_type";
    keys[2] = @"typeDesc";
    keys[3] = @"typeName";
    
    NSString *values1[4];
values1[0] = @"8010";
values1[1] = @"STATE";
values1[2] = @"أبين";
values1[3] = @"أبين";
NSDictionary *state1 = [NSDictionary dictionaryWithObjects:values1 forKeys:keys count:4]; 
[arr addObject:state1]; 

NSString *values2[4];
values2[0] = @"8425";
values2[1] = @"STATE";
values2[2] = @"ريمة";
values2[3] = @"ريمة";
NSDictionary *state2 = [NSDictionary dictionaryWithObjects:values2 forKeys:keys count:4]; 
[arr addObject:state2]; 

NSString *values3[4];
values3[0] = @"8771";
values3[1] = @"STATE";
values3[2] = @"لحج";
values3[3] = @"لحج";
NSDictionary *state3 = [NSDictionary dictionaryWithObjects:values3 forKeys:keys count:4]; 
[arr addObject:state3]; 

NSString *values4[4];
values4[0] = @"8035";
values4[1] = @"STATE";
values4[2] = @"الجوف";
values4[3] = @"الجوف";
NSDictionary *state4 = [NSDictionary dictionaryWithObjects:values4 forKeys:keys count:4]; 
[arr addObject:state4]; 

NSString *values5[4];
values5[0] = @"8505";
values5[1] = @"STATE";
values5[2] = @"حضرموت";
values5[3] = @"حضرموت";
NSDictionary *state5 = [NSDictionary dictionaryWithObjects:values5 forKeys:keys count:4]; 
[arr addObject:state5]; 

NSString *values6[4];
values6[0] = @"8015";
values6[1] = @"STATE";
values6[2] = @"عمران";
values6[3] = @"عمران";
NSDictionary *state6 = [NSDictionary dictionaryWithObjects:values6 forKeys:keys count:4]; 
[arr addObject:state6]; 

NSString *values7[4];
values7[0] = @"8213";
values7[1] = @"STATE";
values7[2] = @"ذمار";
values7[3] = @"ذمار";
NSDictionary *state7 = [NSDictionary dictionaryWithObjects:values7 forKeys:keys count:4]; 
[arr addObject:state7]; 

NSString *values8[4];
values8[0] = @"8729";
values8[1] = @"STATE";
values8[2] = @"إب";
values8[3] = @"إب";
NSDictionary *state8 = [NSDictionary dictionaryWithObjects:values8 forKeys:keys count:4]; 
[arr addObject:state8]; 

NSString *values9[4];
values9[0] = @"8017";
values9[1] = @"STATE";
values9[2] = @"الحديدة";
values9[3] = @"الحديدة";
NSDictionary *state9 = [NSDictionary dictionaryWithObjects:values9 forKeys:keys count:4]; 
[arr addObject:state9]; 

NSString *values10[4];
values10[0] = @"8181";
values10[1] = @"STATE";
values10[2] = @"حجة";
values10[3] = @"حجة";
NSDictionary *state10 = [NSDictionary dictionaryWithObjects:values10 forKeys:keys count:4]; 
[arr addObject:state10]; 

NSString *values11[4];
values11[0] = @"8021";
values11[1] = @"STATE";
values11[2] = @"تعز";
values11[3] = @"تعز";
NSDictionary *state11 = [NSDictionary dictionaryWithObjects:values11 forKeys:keys count:4]; 
[arr addObject:state11]; 

NSString *values12[4];
values12[0] = @"8596";
values12[1] = @"STATE";
values12[2] = @"شبوة";
values12[3] = @"شبوة";
NSDictionary *state12 = [NSDictionary dictionaryWithObjects:values12 forKeys:keys count:4]; 
[arr addObject:state12]; 

NSString *values13[4];
values13[0] = @"8736";
values13[1] = @"STATE";
values13[2] = @"المهرة";
values13[3] = @"المهرة";
NSDictionary *state13 = [NSDictionary dictionaryWithObjects:values13 forKeys:keys count:4]; 
[arr addObject:state13]; 

NSString *values14[4];
values14[0] = @"8144";
values14[1] = @"STATE";
values14[2] = @"المحويت";
values14[3] = @"المحويت";
NSDictionary *state14 = [NSDictionary dictionaryWithObjects:values14 forKeys:keys count:4]; 
[arr addObject:state14]; 

NSString *values15[4];
values15[0] = @"8096";
values15[1] = @"STATE";
values15[2] = @"صعدة";
values15[3] = @"صعدة";
NSDictionary *state15 = [NSDictionary dictionaryWithObjects:values15 forKeys:keys count:4]; 
[arr addObject:state15]; 

NSString *values16[4];
values16[0] = @"8699";
values16[1] = @"STATE";
values16[2] = @"عدن";
values16[3] = @"عدن";
NSDictionary *state16 = [NSDictionary dictionaryWithObjects:values16 forKeys:keys count:4]; 
[arr addObject:state16]; 

NSString *values17[4];
values17[0] = @"8560";
values17[1] = @"STATE";
values17[2] = @"الضالع";
values17[3] = @"الضالع";
NSDictionary *state17 = [NSDictionary dictionaryWithObjects:values17 forKeys:keys count:4]; 
[arr addObject:state17]; 

NSString *values18[4];
values18[0] = @"8264";
values18[1] = @"STATE";
values18[2] = @"أمانة العاصمة";
values18[3] = @"أمانة العاصمة";
NSDictionary *state18 = [NSDictionary dictionaryWithObjects:values18 forKeys:keys count:4]; 
[arr addObject:state18]; 

NSString *values19[4];
values19[0] = @"8733";
values19[1] = @"STATE";
values19[2] = @"صنعاء";
values19[3] = @"صنعاء";
NSDictionary *state19 = [NSDictionary dictionaryWithObjects:values19 forKeys:keys count:4]; 
[arr addObject:state19]; 

NSString *values20[4];
values20[0] = @"8525";
values20[1] = @"STATE";
values20[2] = @"البيضاء";
values20[3] = @"البيضاء";
NSDictionary *state20 = [NSDictionary dictionaryWithObjects:values20 forKeys:keys count:4]; 
[arr addObject:state20]; 

NSString *values21[4];
values21[0] = @"8981";
values21[1] = @"STATE";
values21[2] = @"سقطرى";
values21[3] = @"سقطرى";
NSDictionary *state21 = [NSDictionary dictionaryWithObjects:values21 forKeys:keys count:4]; 
[arr addObject:state21]; 

NSString *values22[4];
values22[0] = @"8581";
values22[1] = @"STATE";
values22[2] = @"مأرب";
values22[3] = @"مأرب";
NSDictionary *state22 = [NSDictionary dictionaryWithObjects:values22 forKeys:keys count:4]; 
[arr addObject:state22]; 
    return arr;
}



-(NSArray*)getGovernorates{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if(governoratesDict){
        NSString *keys[4];
        keys[0] = @"_id";
        keys[1] = @"drop_down_type";
        keys[2] = @"typeDesc";
        keys[3] = @"typeName";
        int i = 8324;
        for (NSString *str in governorates) {
            NSString *values[4];
            values[0] = [NSString stringWithFormat:@"%d", i];
            values[1] = @"STATE";
            values[2] = str;
            values[3] = str;
            NSDictionary *dict = [NSDictionary dictionaryWithObjects:values forKeys:keys count:4];
            [arr addObject:dict];
        }
    }
    return arr;
}

-(NSArray*)getDistricts{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSString *keys[4];
    keys[0] = @"_id";
    keys[1] = @"drop_down_type";
    keys[2] = @"typeDesc";
    keys[3] = @"typeName";
    
    if(districts){
        int i = 8373;
        for (NSString* str in districts) {
            NSString *values1[4];
            values1[0] = [NSString stringWithFormat:@"%d", i];
            values1[1] = @"STATE";
            values1[2] = str;
            values1[3] = str;
            NSDictionary *dist = [NSDictionary dictionaryWithObjects:values1 forKeys:keys count:4];
            [arr addObject:dist];
        }
    }
    return arr;
}

- (void)idTypeSelected:(NSNotification *)notification{
    NSLog(@"selected doc in pt4: %@", notification.userInfo);
    NSDictionary *dict = notification.userInfo;
    if([[dict objectForKey:@"drop_down_type"] isEqualToString:@"IDENTIFICATION"]){
        docType = [dict objectForKey:@"typeName"];
    }
    if([propertyFileName isEqualToString:@"SignupPT4"]){
        NSString *govt = [dict objectForKey:@"typeName"];
        if([governoratesDict objectForKey:govt]){
            NSLog(@"Selected governorate %@", govt);
            districts = [governoratesDict objectForKey:govt];
        }
    }
    if([[notification.userInfo objectForKey:@"drop_down_type"] isEqualToString:@"IDENTIFICATION"]){
        [[NSUserDefaults standardUserDefaults] setObject:[notification.userInfo objectForKey:@"typeName"]  forKey:@"SelectedDocType"];
    }
}

-(void)getGovernoratesFromWebService
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reachability  currentReachabilityStatus];
    if (netStatus == NotReachable) {
        NSLog(@"Alert : No internet..");
    }
    else
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        NSString *processor_Code = PROCESSOR_CODE_GET_GOVERNORATES;
        NSString *transactionType = TRANSACTION_CODE_GET_GOVERNORATES;
        if (transactionType) {
            [dict setObject:transactionType forKey:PARAMETER13];
        }
        if (processor_Code) {
            [dict setObject:processor_Code forKey:PARAMETER15];
        }
        WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
        NSMutableDictionary *webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getGovernoratesBundle:dict]];
        WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
        NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
        WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
        [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
    }
    
}

- (void)updateGovernorates:(NSNotification *)notification{
    NSString * jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"GovernoratesDict"];
    NSStringEncoding  encoding = NSUTF8StringEncoding;
    NSData * jsonData = [jsonString dataUsingEncoding:encoding];
    NSError * error=nil;
    NSMutableDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    governoratesDict = parsedData;
    governorates = [governoratesDict allKeys];
//    districts = [governoratesDict valueForKey:@""];
//    for (NSString *str in governorates) {
//        NSLog(@"str %@", [governoratesDict valueForKey:str]);
//    }
//    NSLog(@"governoratesDict %@", parsedData);
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
