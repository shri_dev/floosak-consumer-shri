//
//  MapsViewController.m
//  ConsumerClient
//
//  Created by test on 17/10/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import "MapsViewController.h"
#import "Constants.h"
#import "Localization.h"
#import "ValidationsClass.h"
#import <CoreLocation/CoreLocation.h>
#import "WebSericeUtils.h"
#import "WebServiceRequestFormation.h"
#import "WebServiceRunning.h"
#import "WebServiceConstants.h"
#import "WebServiceDataObject.h"
#import "BaseViewController.h"
#import "MyLocation.h"

#define METERS_PER_MILE 1609.344
#define KILOMETERS_PER_METER 0.001
#define METERS_PER_KILOMETER 1000

@interface MapsViewController ()
{
    NSMutableDictionary *webUtilsValues;
}

@end

@implementation MapsViewController
@synthesize mapView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary{
   
    NSLog(@"PropertyFileName:%@",propertyFileArray);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        propertyFileName = propertyFileArray;
        local_fromViewe = fromView;
        NSLog(@"MapViewController");
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    strMKMapUserZoomRadios = 803.947017;
    
    //PageHeader
    pageHeader = [[PageHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64 ) withHeaderTitle:@""  withLeftbarBtn1Image_IconName:BACK_BUTTON_IMAGE withLeftbarBtn2Image_IconName:nil withHeaderButtonImage:HEADER_IMAGE_NAME  withRightbarBtn1Image_IconName:nil withRightbarBtn2Image_IconName:nil withRightbarBtn3Image_IconName:nil withTag:0];
    pageHeader.delegate = self;
    
    [self.view addSubview:pageHeader];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(callService)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Search" forState:UIControlStateNormal];
    //button.titleLabel.textColor=[UIColor redColor];
    [button setTitleColor:[UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0] forState:UIControlStateNormal];
    button.frame = CGRectMake(80.0, pageHeader.frame.origin.y + 15, 160.0, pageHeader.frame.size.height - 20);
    [pageHeader addSubview:button];

    strChk = @"First Time";
    [self addMKMapView];
}

- (void)addMKMapView{
    
    mapView = [[MKMapView alloc]init];
    mapView.frame=CGRectMake(0,
                             pageHeader.frame.size.height,
                             pageHeader.frame.size.width,
                             ([UIScreen mainScreen].bounds.size.height - pageHeader.frame.size.height));
    mapView.delegate = self;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
#endif
    [self.locationManager startUpdatingLocation];
    
    mapView.showsUserLocation = YES;
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [self.view addSubview:mapView];
}


-(void)callService
{
     NSLog(@"Call Service");
    
    WebSericeUtils *webUtils = [[WebSericeUtils alloc] init];
    webUtilsValues = [[NSMutableDictionary alloc] initWithDictionary:[webUtils getWebServiceBundleObjectWithProccessorCode:PROCESSOR_CODE_GET_LOCATION withInputDataModel:nil]];
    [webUtilsValues setObject:strMKMapUserZoomCordinates forKey:PARAMETER17];
    [webUtilsValues setObject:[NSString stringWithFormat:@"%f",strMKMapUserZoomRadios] forKey:PARAMETER18];
    
   WebServiceRequestFormation *webServiceRequest = [[WebServiceRequestFormation alloc] init];
    NSString *webRequest  = [webServiceRequest sendRequest:webUtilsValues];
    
    WebServiceRunning *webServiceRun = [[WebServiceRunning alloc] init];
    [webServiceRun startWebServiceWithRequest:webRequest withReqDictionary:webUtilsValues withDelegate:self];
    
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    //MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 15.0, 15.0);
    //MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 15*KILOMETERS_PER_METER, 15*KILOMETERS_PER_METER);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    strMKMapUserZoomCordinates = [NSString stringWithFormat:@"%f|%f",userLocation.coordinate.latitude,userLocation.coordinate.longitude];
    if ([strChk isEqualToString:@"First Time"]) {
        [self callService];
        strChk = @"";
    }
}

// Zoom in and aoom out
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    
}

// get Zoom in and aoom out radious in kilometers
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    double z = log2(360 * ((self.mapView.frame.size.width/256) / self.mapView.region.span.longitudeDelta));
    strMKMapUserZoomRadios = z * METERS_PER_KILOMETER;
    
    MKMapRect mRect = self.mapView.visibleMapRect;
    MKMapPoint eastMapPoint = MKMapPointMake(MKMapRectGetMinX(mRect), MKMapRectGetMidY(mRect));
    MKMapPoint westMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), MKMapRectGetMidY(mRect));
    
    double currentDist = MKMetersBetweenMapPoints(eastMapPoint, westMapPoint);
    strMKMapUserZoomRadios = currentDist * KILOMETERS_PER_METER;
    NSLog(@"%f",z);
    NSLog(@"%f",currentDist);
}


-(void)processData:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processCode
{
    //Remove annotations
    for (id<MKAnnotation> annotation in self.mapView.annotations) {
        [self.mapView removeAnnotation:annotation];
    }
    
    //Add Annotations
    if (webServiceDataObject.PaymentDetails3.length>0) {
        NSData *jsonData = [webServiceDataObject.PaymentDetails3 dataUsingEncoding:NSUTF8StringEncoding];
        NSError *jsonError;
        local_dataArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];

        NSLog(@"JsonData..%@",local_dataArray);
        
       [self addAndRefreshAnnotations:local_dataArray];
    }
}


// get Annotations data and Refresh
- (void)addAndRefreshAnnotations:(NSArray *)responseDataArray {
    
    MKCoordinateRegion mapRegion = [self.mapView region];
    CLLocationCoordinate2D centerLocation = mapRegion.center;
    
    for (id<MKAnnotation> annotation in self.mapView.annotations) {
        [self.mapView removeAnnotation:annotation];
    }
    
            for (int i=0; i<responseDataArray.count; i++) {
    
                NSNumber * latitude = [[responseDataArray objectAtIndex:i] objectForKey:@"latitude"];
                NSNumber * longitude = [[responseDataArray objectAtIndex:i] objectForKey:@"longitude"];
                NSString * crimeDescription = [[responseDataArray objectAtIndex:i] objectForKey:@"address"];
                NSString * address = @"Obopay";
                
                
                CLLocationCoordinate2D coordinate;
                coordinate.latitude = latitude.doubleValue;
                coordinate.longitude = longitude.doubleValue;
                MyLocation *annotation = [[MyLocation alloc] initWithName:crimeDescription address:address coordinate:coordinate] ;
                [mapView addAnnotation:annotation];
            }
}

//add annotations
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    static NSString *identifier = @"MyLocation";
    if ([annotation isKindOfClass:[MyLocation class]]) {
        
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        } else {
            annotationView.annotation = annotation;
        }
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        annotationView.image=[UIImage imageNamed:@"arrest.png"];
        
        return annotationView;
    }
    
    return nil;
}

#pragma mark - Error methods
-(void)errorCodeHandlingInXML:(NSString *)errorCode andMessgae:(NSString *)message withProcessorCode:(NSString *)processorCode
{
    
}

- (void)errorCodeHandlingInWebServiceRunningWithErrorCode:(NSString *)errorCode
{
    
}

//Back Action
-(void)menuBtn_Action
{
    [self.navigationController popViewControllerAnimated:NO];
}

/*
-(void)processData1:(WebServiceDataObject *)webServiceDataObject withProcessCode:(NSString *)processCode
{
    for (id<MKAnnotation> annotation in self.mapView.annotations) {
        [self.mapView removeAnnotation:annotation];
    }
    
    if (webServiceDataObject.PaymentDetails3.length>0) {
        NSData *jsonData = [webServiceDataObject.PaymentDetails3 dataUsingEncoding:NSUTF8StringEncoding];
        NSError *jsonError;
        local_dataArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
        
        NSLog(@"JsonData..%@",local_dataArray);
      
                for (int i=0; i<local_dataArray.count; i++) {
        
                    NSString *strCordinates = [NSString stringWithFormat:@"%@,%@",[[local_dataArray objectAtIndex:i] objectForKey:@"latitude"],[[local_dataArray objectAtIndex:i] objectForKey:@"longitude"]];
        
                    [self addPinWithTitle:[[local_dataArray objectAtIndex:i] objectForKey:@"address"] AndCoordinate:strCordinates];
                }
    }
}

-(void)addPinWithTitle:(NSString *)title AndCoordinate:(NSString *)strCoordinate
{
    MKPointAnnotation *mapPin = [[MKPointAnnotation alloc] init];
    
    // clear out any white space
    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // convert string into actual latitude and longitude values
    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
    
    double latitude = [components[0] doubleValue];
    double longitude = [components[1] doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    mapPin.title = title;
    mapPin.coordinate = coordinate;
    
    [self.mapView addAnnotation:mapPin];
}
*/


@end
