//
//  ParentTemplate9.h
//  Consumer Client
//
//  Created by android on 9/9/15.
//  Copyright (c) 2015 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageHeaderView.h"
#import "FeaturesMenu.h"
#import "PageHeaderView.h"
#import "FeaturesMenu.h"
#import "ActivityIndicator.h"
#import "ChildTemplate2.h"
#import "ChildTemplate4.h"
#import "BaseViewController.h"
#import "PopUpTemplate1.h"
/**
 * This class used to handle functionality and ViewController of ParentTemplate9
 *
 * @author Integra
 *
 */
@interface ParentTemplate9 : BaseViewController<PageHeaderViewButtonDelegate,FeaturesMenuDelegate>
{
    int selectedSideMenuButtonIndex;
    FeaturesMenu *featureMenu;
    PageHeaderView *pageHeader;
    NSDictionary *templateDictionary;
    int tagVal;
    NSString *currentPropertyFile;
}
/**
 * declarations are used to set the UIConstraints Of ParentTemplate9.
 * Label,Value label,Border label and Buttons.
 */
@property(nonatomic,strong) NSString *propertyFileName;
@property(nonatomic,strong)UIView *parentSubview;
@property(nonatomic,strong)UILabel *featureNameLabel;

/**
 * This method is  used for Method Initialization of ParentTemplate9.
 */
// Method For initialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view  withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;

@end