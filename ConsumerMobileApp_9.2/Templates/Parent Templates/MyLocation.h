//
//  MyLocation.h
//  ConsumerClient
//
//  Created by android on 20/10/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyLocation : NSObject<MKAnnotation>

- (id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate;
- (MKMapItem*)mapItem;
@end
