//
//  ParentTemplate2.h
//  Consumer Client
//
//  Created by Integra Micro on 17/04/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PageHeaderView.h"
#import "TabbMenu.h"
#import "PopUpTemplate1.h"

@interface ParentTemplate2 : BaseViewController<PageHeaderViewButtonDelegate,TabbMenuDelegate>
{
    int selectedSideMenuButtonIndex;
    NSArray *propertiesArray;
    NSString *propertyFileName;
    int from;
}

@property (assign)NSInteger selected_index;
@property (strong, nonatomic) IBOutlet UIView *parentSubView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view
         withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode
            dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;

@end
