//
//  MapsViewController.h
//  ConsumerClient
//
//  Created by test on 17/10/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageHeaderView.h"
#import "OptionsMenu.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ActivityIndicator.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface MapsViewController : UIViewController<PageHeaderViewButtonDelegate,MKMapViewDelegate, CLLocationManagerDelegate,UIGestureRecognizerDelegate>
{
    NSString *propertyFileName;
    NSString *local_fromViewe;
    
    OptionsMenu *optionsMenu;
    PageHeaderView *pageHeader;
    UILabel *bgLbl;
    
    MKMapView *mapView;
    MKCoordinateRegion mapRegion;
    UITapGestureRecognizer *tapGestureRecognizer;
    double strMKMapUserZoomRadios;
    NSString *strMKMapUserZoomCordinates;
    UIActivityIndicatorView *activityView;
    NSArray *local_dataArray;
    NSString *strChk;
}

@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property(nonatomic, retain) CLLocationManager *locationManager;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;
- (void)refreshTapped:(NSArray *)responseData;
@end
