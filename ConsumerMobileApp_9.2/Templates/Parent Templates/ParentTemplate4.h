//
//  ParentTemplate4.h
//  Consumer Client
//
//  Created by Integra Micro on 05/05/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "PageHeaderView.h"
#import "OptionsMenu.h"
#import "XmlParserHandler.h"
#import "ActivityIndicator.h"
#import "Reachability.h"
#import "DatabaseManager.h"
#import "DatabaseConstatants.h"
#import "CustomTextField.h"

// Class Parent Template4.
/**
 * This class used to handle functionality and ViewController of ParentTemplate4
 *
 * @author Integra
 *
 */
@interface ParentTemplate4 : BaseViewController<PageHeaderViewButtonDelegate, OptionsMenuDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>
{
    NSString *propertyFileName;
    NSArray *borderColors;
    NSString *alertview_Type;
    
    NSString *nextTemplateProperty;
    NSString *nextTemplate;

    NSArray *genderDetailsArray;
    NSArray *idDetailsArray;
    
    NSMutableDictionary *selfRegisDataDict;
    
    NSString *validationType;
    NSString *actionType;
    NSString *processorCode;
    NSString *alertviewType;
    NSString *nextTemplatePropertyFileName;
    NSString *nextTemplateName;
    NSString *currentTemplatePropertyFileName;
    
    OptionsMenu *optionsMenu;
    PageHeaderView *pageHeader;
    
    UILabel *bgLbl;
    
    int label_Y_Position;
    int label_X_Position;
    
    int distance_Y;
    
    int filed_Y_Position;
    int filed_X_Position;
    
    int next_Y_Position;
    int borderLabelY_position;
    
    NSMutableArray *validationsArray;
    
    /**
     Declare a toolbar for pickerview
     */
    UIToolbar *numberToolbar;
    CustomTextField *activeField;
    
    CGPoint contentOffset;
    bool isScroll;
    
    int buttonsCount;
    int startY_Position;
    
    int numberofFields;
    
    NSString *local_fromViewe;
    
    float inputField_X_Position;
    float inputField_Y_Position;
    
    float scroll_Y_Position;
    
    DatabaseManager *dataManager;
    
    NSString *dropdownString;
    UIButton *btn;
    NSArray *dropDownDataArray;
            
    //SignUpDKYC
   UIImageView *uploadedDocIMgView;
    
    int userApplicationMode;
    NSString *phoneNumberStr;
    NSString *multiUserPhnStr;
    
    // For T1,T2 and T3
    NSString *appTypeStr;
    
    BOOL isDebugging;
    
    NSMutableDictionary *governoratesDict;
    NSArray *governorates;
    NSArray *districts;
    NSString *docType;
}
/**
 * declarations are used to set the UIConstraints Of ParentTemplate4.
 * Label,Value label,Border label, Dropdown and Buttons.
 */
// Parent Template4 Constaraints.
@property(strong,nonatomic) UIScrollView *parentScrollView;
@property(strong,nonatomic) UILabel *signupstepCountLabel;
@property(strong,nonatomic) UILabel *signupStep_ValueLabelBoarder;

@property(strong,nonatomic) UILabel *textfieldTitle_Label1;
@property(strong,nonatomic) CustomTextField *textfield1;
@property(nonatomic,strong) UIButton *dropDownButton1;

@property(strong,nonatomic) UILabel *textfieldTitle_Label2;
@property(strong,nonatomic) CustomTextField *textfield2;
@property(nonatomic,strong) UIButton *dropDownButton2;

@property(strong,nonatomic) UILabel *textfieldTitle_Label3;
@property(strong,nonatomic) CustomTextField *textfield3;
@property(nonatomic,strong) UIButton *dropDownButton3;

@property(strong,nonatomic) UILabel *textfieldTitle_Label4;
@property(strong,nonatomic) CustomTextField *textfield4;
@property(nonatomic,strong) UIButton *dropDownButton4;

@property(strong,nonatomic) UILabel *textfieldTitle_Label5;
@property(strong,nonatomic) CustomTextField *textfield5;
@property(nonatomic,strong) UIButton *dropDownButton5;

@property(strong,nonatomic) UILabel *textfieldTitle_Label6;
@property(strong,nonatomic) CustomTextField *textfield6;
@property(nonatomic,strong) UIButton *dropDownButton6;

@property(strong,nonatomic) UILabel *textfieldTitle_Label7;
@property(strong,nonatomic) CustomTextField *textfield7;
@property(nonatomic,strong) UIButton *dropDownButton7;

@property(strong,nonatomic) UILabel *textfieldTitle_Label8;
@property(strong,nonatomic) CustomTextField *textfield8;
@property(nonatomic,strong) UIButton *dropDownButton8;

@property(strong,nonatomic) UILabel *textfieldTitle_Label9;
@property(strong,nonatomic) CustomTextField *textfield9;
@property(nonatomic,strong) UIButton *dropDownButton9;

@property(strong,nonatomic) UILabel *textfieldTitle_LabelGovernorate;
@property(strong,nonatomic) CustomTextField *textfieldGovernorate;
@property(nonatomic,strong) UIButton *dropDownButtonGovernorate;

@property(strong,nonatomic) UILabel *textfieldTitle_Label10;
@property(strong,nonatomic) CustomTextField *textfield10;
@property(nonatomic,strong) UIButton *dropDownButton10;

//deva
@property(strong,nonatomic) UILabel *textfieldTitle_Label11;
@property(strong,nonatomic) CustomTextField *textfield11;
@property(nonatomic,strong) UIButton *dropDownButton11;


@property(strong,nonatomic) UILabel *textfieldTitle_Label12;
@property(strong,nonatomic) CustomTextField *textfield12;
@property(nonatomic,strong) UIButton *dropDownButton12;


@property(strong,nonatomic) UILabel *textfieldTitle_Label13;
@property(strong,nonatomic) CustomTextField *textfield13;
@property(nonatomic,strong) UIButton *dropDownButton13;

@property(strong,nonatomic) UILabel *textfieldTitle_Label14;
@property(strong,nonatomic) CustomTextField *textfield14;
@property(nonatomic,strong) UIButton *dropDownButton14;

@property(strong,nonatomic) UILabel *textfieldTitle_Label15;
@property(strong,nonatomic) CustomTextField *textfield15;
@property(nonatomic,strong) UIButton *dropDownButton15;


@property(nonatomic,strong)UIDatePicker *datePicker1;

@property(strong,nonatomic) UIButton *button1;
@property(strong,nonatomic) UIButton *button2;
@property(strong,nonatomic) UIButton *button3;

//KCK Code Change
@property(strong,nonatomic) UIImageView *bgImageView;
@property(strong,nonatomic) UIImageView *logoImageView;

/**
 * This method is  used for Method Initialization of ParentTemplate4.
 */
// Method For initialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray withProcessorCode:(NSString *)processorCode dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;

-(void)addGovernorateDropdown;

@end
