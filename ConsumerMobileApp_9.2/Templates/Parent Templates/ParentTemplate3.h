//
//  ParentTemplate3.h
//  Consumer Client
//
//  Created by Integra Micro on 05/05/15.
//  Copyright (c) 2015 Integra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PageHeaderView.h"
#import "OptionsMenu.h"
#import "DatabaseManager.h"
#import "DatabaseConstatants.h"


// Class Parenttemplate3.

/**
 * This class used to handle functionality and ViewController of ParentTemplate3
 *
 * @author Integra
 *
 */
@interface ParentTemplate3 : BaseViewController <PageHeaderViewButtonDelegate,UIScrollViewDelegate,OptionsMenuDelegate>
{
    NSString *propertyFileName;
    NSString *nextTemplate;
    NSString *nextTemplatePropertyFile;
    NSString *processCode;
    NSString *action_type;
    
    DatabaseManager *dataBaseManager;
    OptionsMenu *optionsMenu;
    UILabel *bgLbl;
    
    int nextY_Position;
    
    int labelY_Position;
    int labelX_Position;
    
    int distanceY;
    int height_Position;
    int gapY_position;
    int numberOfButtons;
    
    PageHeaderView *pageHeader;
    
    //Pagecontroller images
    UIPageControl *pageControl;
    UIScrollView *scroll;
    NSMutableArray *imagesArray;
    CGFloat position;
    int imageCount;
}

/**
 * declarations are used to set the UIConstraints Of ParentTemplate4.
 * Label,Value label,Border label,Image,TableView and Buttons.
 */
@property(strong,nonatomic) UILabel *headerLabel1;
@property(strong,nonatomic) UILabel *headerLabel2;
@property(strong,nonatomic) UIImageView *applicationLogoImage_Icon;

@property(strong,nonatomic) UIButton *pt3Button1;
@property(strong,nonatomic) UIButton *pt3Button2;
@property(strong,nonatomic) UIButton *pt3Button3;
@property(strong,nonatomic) UIButton *pt3Button4;

/*
 * This method is used to Declare parentTemplate3 Method For initialization.
 */
// Method For initialization.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withSelectedIndex:(int)selectedIndex fromView:(int)view withFromView:(NSString *)fromView withPropertyFile:(NSString *)propertyFileArray  withProcessorCode:(NSString *)processorCode
    dataArray:(NSArray *)dataArray dataDictionary:(NSDictionary *)dataDictionary;

@end
