//
//  PushNotifiactionViewController.h
//  Consumer Client
//
//  Created by test on 01/02/16.
//  Copyright (c) 2016 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import "ChildTemplate11.h"
#import "NotificationConstants.h"
#import "Localization.h"

/**
 * This class used to handle functionality and ViewController of PushNotifiactionViewController.
 *
 * @author Integra
 *
 */
@interface PushNotifiactionViewController : BaseViewController
{
    NSString *propertyFile;
    NSString *processorCodeStr;
    NSString *nextTemplate;
    NSString *nextTemplatePropertyFile;
    NSMutableArray *contentArray;
    NSMutableDictionary *contentDictionary;
}

/**
 * declarations are used to set the UIConstraints Of PushNotifiactionViewController.
 * Label.
 */
@property(nonatomic,strong) UILabel *headerNameLabel;
/**
 * This method is  used for Method Initialization of PushNotifiactionViewController.
 */
// Method For initializatio
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil dataDictionary:(NSDictionary *)dataDictionary;

@end
