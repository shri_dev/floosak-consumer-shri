//
//  TabbMenu.h
//  ConsumerMobileApp_9.2
//
//  Created by Karthick on 1/27/18.
//  Copyright © 2018 Soumya. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TabbMenuDelegate <NSObject>
/**
 * This method is  used for Custom delegate Method for Feature Menu.
 */
@required
-(void)tabbMenuButtonAction:(int)tag;
@end
/**
 * This class used to handle functionality and view of Feature menu.
 *
 * @author VirginCodes
 *TabbMenu
 */
@interface TabbMenu : UIView

/**
 * declarations are used to set the UIConstraints Of Feature menu.
 * Label  and TableView.
 */
@property(nonatomic,strong)  UIScrollView *mainScrollView;
@property(nonatomic, strong) NSArray *featuresArray;
@property(nonatomic, strong) NSArray *featureImagesArray;
@property(nonatomic, strong) NSArray *featureTemplates;
@property(nonatomic, strong) NSArray *featureTemplateProperties;
@property(nonatomic,strong)  NSString *propertyFileName;

@property (nonatomic, assign) NSObject <TabbMenuDelegate> *delegate;
/**
 * This method is  used for Method Initialization of Feature Menu.
 */
- (id)initWithFrame:(CGRect)frame withSelectedIndex:(NSInteger)index fromView:(int)fromView withSelectedCategory:(NSInteger)categoryIndex andSetSubTemplates:(BOOL)subTemplates withParentIndex :(NSInteger)pIndex;

-(void)scrollToPosition:(int)tag;
@end

