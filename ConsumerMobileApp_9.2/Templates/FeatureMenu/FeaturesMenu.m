//  FeaturesMenu.m
//  Consumer Client
//  Created by jagadeeshk on 15/05/15.
//  Copyright (c) 2015 Soumya. All rights reserved.


#import "FeaturesMenu.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import <QuartzCore/QuartzCore.h>
#import "WebServiceConstants.h"
#import "Template.h"
#import "Localization.h"
#import "UILabel+Extension.h"

@implementation FeaturesMenu
{
    int minNumber;    
}

@synthesize featuresArray,featureImagesArray,featureTemplates,featureTemplateProperties,mainScrollView,propertyFileName;

#pragma mark - Feature Menu frame initialization.
/**
 * This method is used to set implemention of feature menu.
 */
- (id)initWithFrame:(CGRect)frame withSelectedIndex:(NSInteger)index fromView:(int)fromView withSelectedCategory:(NSInteger)categoryIndex andSetSubTemplates:(BOOL)subTemplates withParentIndex :(NSInteger)pIndex
{
    
    self = [super initWithFrame:frame];
    if (self)
    {
        mainScrollView = [[UIScrollView alloc] init];
        mainScrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        mainScrollView.backgroundColor = [UIColor clearColor];
        mainScrollView.contentOffset = CGPointMake(0, 0);
        mainScrollView.delegate = self;
        [mainScrollView setShowsVerticalScrollIndicator:NO];
        propertyFileName=@"Features_List";
        NSLog(@"PropertyFileName:%@",propertyFileName);
        //FeatureMenu BackGround Color.
        NSArray *viewbackGroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
       [mainScrollView setBackgroundColor:[UIColor colorWithRed:[[viewbackGroundColor objectAtIndex:0] floatValue] green:[[viewbackGroundColor objectAtIndex:1] floatValue] blue:[[viewbackGroundColor objectAtIndex:2] floatValue] alpha:1.0f]];
        [self addSubview:mainScrollView];
        
        // feature menu Array.
        if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
        {
            featuresArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:KEY_TEMPLATES];
            featureImagesArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:IMAGES];
        }
        else
        if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
        {
            if (subTemplates) {
                int index1 = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"topLevelNavigation"];
                featuresArray = [[Template getNextFeatureTemplateWithIndex:index1] objectForKey:KEY_CATEGORY_FEATURES];
                featureImagesArray = [[Template getNextFeatureTemplateWithIndex:index1] objectForKey:IMAGES];
            }
            else
            {
                featuresArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:CATEGORIES_NAMES_STRING];
                featureImagesArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:CATEGORIES_IMAGES];
            }
        }

        float distance;
        float y_pos;
        
        if ([[UIScreen mainScreen] bounds].size.height > 480)
        {
            distance = 100;
            y_pos = 20;
        }
        else
        {
            distance = 85;
            y_pos = 12;
        }
        
        float x_pos = 10;
        
        NSArray *iconBtncolorsArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"feature_list_view_default_color", @"GeneralSettings",[NSBundle mainBundle], nil)];
        
        NSArray *selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"feature_list_view_selected_color", @"GeneralSettings",[NSBundle mainBundle], nil)];

        for (int i = 1; i <= featuresArray.count; i++)
        {
            UIButton *featureButton = [UIButton buttonWithType:UIButtonTypeCustom];
            if (SCREEN_HEIGHT>480){
                [featureButton setFrame:CGRectMake(x_pos, y_pos, 60, 60)];
            }
            else if(SCREEN_HEIGHT==480){
                [featureButton setFrame:CGRectMake(x_pos+5, y_pos, 50, 50)];
            }
            
            // Feature Images
            if ([NSLocalizedStringFromTableInBundle(@"featurelist_template_list_item_image_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                     [featureButton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[featureImagesArray objectAtIndex:i-1]]] forState:UIControlStateNormal];
            
            featureButton.tag = i;

            if (i == index)
            {
                featureButton.backgroundColor = [UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
            }
            else
            {
                featureButton.backgroundColor = [UIColor colorWithRed:[[iconBtncolorsArray objectAtIndex:0] floatValue] green:[[iconBtncolorsArray objectAtIndex:1] floatValue] blue:[[iconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
            }
            
            [featureButton addTarget:self action:@selector(sideButn_Action:) forControlEvents:UIControlEventTouchUpInside];
            featureButton.layer.cornerRadius = 25;
            featureButton.clipsToBounds = YES;
            [mainScrollView addSubview:featureButton];
            UILabel *featureLabel;
             if ([NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
             {
                 featureLabel =[[UILabel alloc] init];
                 
                 if (SCREEN_HEIGHT>480)
                     featureLabel.frame=CGRectMake(0, featureButton.frame.origin.y+featureButton.frame.size.height, self.frame.size.width,40);
                 else if(SCREEN_HEIGHT==480)
                     featureLabel.frame=CGRectMake(0, featureButton.frame.origin.y+featureButton.frame.size.height, self.frame.size.width,40);
 
                 featureLabel.text = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:[featuresArray objectAtIndex:i-1]]];
                 featureLabel.numberOfLines = 3;
                 featureLabel.textAlignment = NSTextAlignmentCenter;
                 [featureLabel alignToTop];
                 
              if([NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_font_override", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
                 {
                     //  Properties for label TextColor.
                     NSArray *label_TextColor;
                     
                     if (NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
                         label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                     
                     else if (NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
                         label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                     
                     else
                         label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
                     
                     if (label_TextColor)
                         featureLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                      // Properties for label Textstyle.
                      
                      NSString *textStyle;
                      if(NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
                      textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
                      else if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
                      textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
                      else
                      textStyle = application_default_text_style;
                      
                      // Properties for label Font size.
                      
                      NSString *fontSize;
                      if(NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
                      fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
                      else if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
                      fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
                      else
                      fontSize = application_default_text_size;

                     if ([textStyle isEqualToString:TEXT_STYLE_0])
                           featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                     
                     else if ([textStyle isEqualToString:TEXT_STYLE_1])
                            featureLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                     
                     else if ([textStyle isEqualToString:TEXT_STYLE_2])
                            featureLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                     
                     else
                            featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                 }
                 else{
                     //Default Properties for label textColor
                     NSArray *label_TextColor ;
                     if (NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
                         label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
                     else
                         label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
                     if (label_TextColor)
                         featureLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
                     
                     //Default Properties for label textStyle
                     
                     NSString *textStyle;
                     if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
                         textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
                     else
                         textStyle = application_default_text_style;
                     
                     //Default Properties for label fontSize
                     
                     NSString *fontSize;
                     if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
                         fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
                     else
                         fontSize = application_default_text_size;
                     
                     if ([textStyle isEqualToString:TEXT_STYLE_0])
                         featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                     
                     else if ([textStyle isEqualToString:TEXT_STYLE_1])
                         featureLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
                     
                     else if ([textStyle isEqualToString:TEXT_STYLE_2])
                         featureLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
                     
                     else
                         featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
                     
                 }
                 featureLabel.tag = i;
                 [mainScrollView addSubview:featureLabel];
        }
            
            UILabel *featureBgLabel =[[UILabel alloc] init];
            
            if (SCREEN_HEIGHT>480)
                featureBgLabel.frame=CGRectMake(10, featureLabel.frame.size.height+featureLabel.frame.origin.y, 60, 1);
            else if(SCREEN_HEIGHT==480)
                featureBgLabel.frame=CGRectMake(10, featureLabel.frame.size.height+featureLabel.frame.origin.y, 60, 1);
          
            NSArray *featureBgcolorsArray = [ValidationsClass colorWithHexString:FEATURELIST_BORDER_COLOR];
            
        if (featureBgcolorsArray)
            featureBgLabel.backgroundColor = [UIColor colorWithRed:[[featureBgcolorsArray objectAtIndex:0] floatValue] green:[[featureBgcolorsArray objectAtIndex:1] floatValue] blue:[[featureBgcolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
           
            [mainScrollView addSubview:featureBgLabel];
            
        y_pos =  featureBgLabel.frame.origin.y + featureBgLabel.frame.size.height+5;
        mainScrollView.contentSize = CGSizeMake(self.frame.size.width, y_pos);
        }
    }
    return self;
}


#pragma mark - SET DELEGETE METHOD.

/**
 * This method is  used for Custom delegate Method for Feature Menu.
 * @param type -Based on Profile Features will Be shown.
 * Method is used for get user selected feature.
 */
-(void)sideButn_Action:(UIButton *)button
{
    if (self.delegate != nil)
    {
        [self.delegate featuresMenuButtonAction:(int)button.tag];
    }
}


@end
