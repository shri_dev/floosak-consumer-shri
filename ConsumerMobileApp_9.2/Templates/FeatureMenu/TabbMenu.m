//
//  TabbMenu.m
//  ConsumerMobileApp_9.2
//
//  Created by Karthick on 1/27/18.
//  Copyright © 2018 Soumya. All rights reserved.
//

#import "TabbMenu.h"
#import "Constants.h"
#import "ValidationsClass.h"
#import <QuartzCore/QuartzCore.h>
#import "WebServiceConstants.h"
#import "Template.h"
#import "Localization.h"
#import "UILabel+Extension.h"

//@interface TabbMenu ()
@implementation TabbMenu

{
  int minNumber;
  UIButton *featureButton;
}
@synthesize featuresArray,featureImagesArray,featureTemplates,featureTemplateProperties,mainScrollView,propertyFileName;

#pragma mark - Feature Menu frame initialization.
/**
 * This method is used to set implemention of feature menu.
 */
- (id)initWithFrame:(CGRect)frame withSelectedIndex:(NSInteger)index fromView:(int)fromView withSelectedCategory:(NSInteger)categoryIndex andSetSubTemplates:(BOOL)subTemplates withParentIndex :(NSInteger)pIndex
{
  
  self = [super initWithFrame:frame];
  if (self)
  {
    mainScrollView = [[UIScrollView alloc]init];
    mainScrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    mainScrollView.backgroundColor = [UIColor colorWithRed:229/255.0 green:229/255.0f blue:229/255.0 alpha:1.0f];
    mainScrollView.contentOffset =CGPointMake(0, 0);
    [mainScrollView setShowsVerticalScrollIndicator:NO];
    propertyFileName=@"Features_List";
    NSLog(@"PropertyFileName:%@",propertyFileName);
    // FeatureMenu BackGround Color.
    //NSArray *viewbackGroundColor=[ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_back_ground_color",propertyFileName,[NSBundle mainBundle], nil)];
   // [mainScrollView setBackgroundColor:[UIColor colorWithRed:[[viewbackGroundColor objectAtIndex:0] floatValue] green:[[viewbackGroundColor objectAtIndex:1] floatValue] blue:[[viewbackGroundColor objectAtIndex:2] floatValue] alpha:1.0f]];
    
    [self addSubview:mainScrollView];
    
    // feature menu Array.
    if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"0"])
    {
      featuresArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:KEY_TEMPLATES];
      featureImagesArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:IMAGES];
      NSLog(@"featuresArray %@",featuresArray);
      NSLog(@"Features_List %@", featureImagesArray);
    }
    else
      if ([NSLocalizedStringFromTableInBundle(@"application_feature_list_view", @"GeneralSettings",[NSBundle mainBundle], nil) isEqualToString:@"1"])
      {
        if (subTemplates) {
          int index1 = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"topLevelNavigation"];
          featuresArray = [[Template getNextFeatureTemplateWithIndex:index1] objectForKey:KEY_CATEGORY_FEATURES];
          featureImagesArray = [[Template getNextFeatureTemplateWithIndex:index1] objectForKey:IMAGES];
        }
        else
        {
          featuresArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:CATEGORIES_NAMES_STRING];
          featureImagesArray = [[Template getNextFeatureTemplateWithIndex:(int)index] objectForKey:CATEGORIES_IMAGES];
        }
      }
    
    float distance;
    float y_pos;
    
    if ([[UIScreen mainScreen] bounds].size.height > 480)
    {
      distance = 100;
      y_pos = 15;
    }
    else
    {
      distance = 85;
      y_pos = 15;
    }
    
    float x_pos = 10;
    
    NSArray *iconBtncolorsArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"feature_list_view_default_color", @"GeneralSettings",[NSBundle mainBundle], nil)];
    
    NSArray *selectedIconBtncolorsArray = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"feature_list_view_selected_color", @"GeneralSettings",[NSBundle mainBundle], nil)];
    
    for (int i = 1; i <= featuresArray.count; i++)
    {
      featureButton = [UIButton buttonWithType:UIButtonTypeCustom];
      
      if (SCREEN_HEIGHT>480){
        //        [featureButton setFrame:CGRectMake(y_pos,x_pos , 60, 60)];
        [featureButton setFrame:CGRectMake(x_pos+12,y_pos , 60, 60)];
        
      }
      else if(SCREEN_HEIGHT==480){
        [featureButton setFrame:CGRectMake(x_pos+17, y_pos, 50, 50)];
      }
      //            if (SCREEN_HEIGHT>480){
      //                [featureButton setFrame:CGRectMake(x_pos, y_pos, 60, 60)];
      //            }
      //            else if(SCREEN_HEIGHT==480){
      //                [featureButton setFrame:CGRectMake(x_pos+5, y_pos, 50, 50)];
      //            }
      
      // Feature Images
      if ([NSLocalizedStringFromTableInBundle(@"featurelist_template_list_item_image_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        UIImage *btnimage = [[UIImage imageNamed:[NSString stringWithFormat:@"feature-%@-icon.png",[featureImagesArray objectAtIndex:i-1]]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [featureButton setImage:btnimage forState:UIControlStateNormal];
        [featureButton setImageEdgeInsets: UIEdgeInsetsMake(10, 10, 10, 10)];
        [featureButton setTintColor:[UIColor blackColor]];
      }
      featureButton.contentMode = UIViewContentModeScaleAspectFill;
      
      featureButton.tag = i;
      
      if (i == index)
      {
        featureButton.backgroundColor = [UIColor colorWithRed:255/255.0 green:144/255.0 blue:0 alpha:1.0f];
        //featureButton.backgroundColor = [UIColor colorWithRed:[[selectedIconBtncolorsArray objectAtIndex:0] floatValue] green:[[selectedIconBtncolorsArray objectAtIndex:1] floatValue] blue:[[selectedIconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
      }
      else
      {
        featureButton.backgroundColor = [UIColor clearColor];
        //featureButton.backgroundColor = [UIColor colorWithRed:[[iconBtncolorsArray objectAtIndex:0] floatValue] green:[[iconBtncolorsArray objectAtIndex:1] floatValue] blue:[[iconBtncolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
      }
      
      [featureButton addTarget:self action:@selector(sideButn_Action:) forControlEvents:UIControlEventTouchUpInside];
      featureButton.layer.cornerRadius = featureButton.frame.size.height/2;
      featureButton.clipsToBounds = YES;
      featureButton.layer.borderWidth = 1.0;
      featureButton.layer.cornerRadius = featureButton.bounds.size.height/2;
      featureButton.layer.borderColor = [[UIColor blackColor] CGColor];
      featureButton.clipsToBounds = YES;
      [mainScrollView addSubview:featureButton];
      UILabel *featureLabel;
      if ([NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_visibility", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
      {
        featureLabel =[[UILabel alloc] init];
        
        if (SCREEN_HEIGHT>480)
          featureLabel.frame=CGRectMake(x_pos, featureButton.frame.origin.y+featureButton.frame.size.height, featureButton.frame.size.width+25,40);
        else if(SCREEN_HEIGHT==480)
          featureLabel.frame=CGRectMake(x_pos, featureButton.frame.origin.y+featureButton.frame.size.height, featureButton.frame.size.width+25,40);
        
        featureLabel.text = [NSString stringWithFormat:@"%@",[Localization languageSelectedStringForKey:[featuresArray objectAtIndex:i-1]]];
        featureLabel.numberOfLines = 3;
        featureLabel.textAlignment = NSTextAlignmentCenter;
        [featureLabel alignToTop];
        
        if([NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_font_override", propertyFileName,[NSBundle mainBundle], nil) caseInsensitiveCompare:TRUE_TEXT] == NSOrderedSame)
        {
          //  Properties for label TextColor.
          NSArray *label_TextColor;
          
          if (NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
            label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else if (NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
            label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
          
          else
            label_TextColor = [ValidationsClass colorWithHexString:application_default_text_color];
          
          if (label_TextColor)
            featureLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
          // Properties for label Textstyle.
          
          NSString *textStyle;
          if(NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
          else if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
          else
            textStyle = application_default_text_style;
          
          // Properties for label Font size.
          
          NSString *fontSize;
          if(NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_list_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
          else if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
          else
            fontSize = application_default_text_size;
          
          if ([textStyle isEqualToString:TEXT_STYLE_0])
            featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_1])
            featureLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_2])
            featureLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
          
          else
            featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
        }
        else{
          //Default Properties for label textColor
          NSArray *label_TextColor ;
          if (NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil))
            label_TextColor = [ValidationsClass colorWithHexString:NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_color",propertyFileName,[NSBundle mainBundle], nil)];
          else
            label_TextColor=[ValidationsClass colorWithHexString:application_default_text_color];
          if (label_TextColor)
            featureLabel.textColor = [UIColor colorWithRed:[[label_TextColor objectAtIndex:0] floatValue] green:[[label_TextColor objectAtIndex:1] floatValue] blue:[[label_TextColor objectAtIndex:2] floatValue] alpha:1.0f];
          
          //Default Properties for label textStyle
          
          NSString *textStyle;
          if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil))
            textStyle = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_style",propertyFileName,[NSBundle mainBundle], nil);
          else
            textStyle = application_default_text_style;
          
          //Default Properties for label fontSize
          
          NSString *fontSize;
          if(NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil))
            fontSize = NSLocalizedStringFromTableInBundle(@"featureslist_template_item_text_size",propertyFileName,[NSBundle mainBundle], nil);
          else
            fontSize = application_default_text_size;
          
          if ([textStyle isEqualToString:TEXT_STYLE_0])
            featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_1])
            featureLabel.font=[UIFont boldSystemFontOfSize:[fontSize floatValue]];
          
          else if ([textStyle isEqualToString:TEXT_STYLE_2])
            featureLabel.font=[UIFont italicSystemFontOfSize:[fontSize floatValue]];
          
          else
            featureLabel.font=[UIFont systemFontOfSize:[fontSize floatValue]];
          
        }
        featureLabel.tag = i;
        [mainScrollView addSubview:featureLabel];
      }
      
      UILabel *featureBgLabel =[[UILabel alloc] init];
      
      if (SCREEN_HEIGHT>480)
        featureBgLabel.frame=CGRectMake(x_pos, featureButton.frame.origin.y+featureButton.frame.size.height, 60, 1);
      else if(SCREEN_HEIGHT==480)
        featureBgLabel.frame=CGRectMake(x_pos, featureButton.frame.origin.y+featureButton.frame.size.height, 60, 1);
      
      NSArray *featureBgcolorsArray = [ValidationsClass colorWithHexString:FEATURELIST_BORDER_COLOR];
      
      if (featureBgcolorsArray)
        featureBgLabel.backgroundColor = [UIColor colorWithRed:[[featureBgcolorsArray objectAtIndex:0] floatValue] green:[[featureBgcolorsArray objectAtIndex:1] floatValue] blue:[[featureBgcolorsArray objectAtIndex:2] floatValue] alpha:1.0f];
      
      featureBgLabel.backgroundColor = [UIColor clearColor];
      [mainScrollView addSubview:featureBgLabel];
      
      //      y_pos =  featureBgLabel.frame.origin.y + featureBgLabel.frame.size.height+5;
      x_pos =  featureLabel.frame.origin.x + featureLabel.frame.size.width+5;
      
      mainScrollView.contentSize = CGSizeMake(x_pos, 80);
      //      mainScrollView.contentSize = CGSizeMake(self.frame.size.width, y_pos);
    }
  }
  
  return self;
}


#pragma mark - SET DELEGETE METHOD.

/**
 * This method is  used for Custom delegate Method for Feature Menu.
 * @param type -Based on Profile Features will Be shown.
 * Method is used for get user selected feature.
 */
-(void)sideButn_Action:(UIButton *)button
{
  if (self.delegate != nil)
  {
    [self.delegate tabbMenuButtonAction:(int)button.tag];
  }
}

-(void)scrollToPosition:(int)tag {
    NSLog(@"Scrolling to position %d", tag);
    NSArray *arr = mainScrollView.subviews;
    NSMutableArray *arr2 = [[NSMutableArray alloc] init];
    for (UIView *view in arr){
        if([view isKindOfClass:[UIButton class]]){
            [arr2 addObject:view];
        }
    }
    if(tag == 6){
        UIButton *button = (UIButton*)[arr2 objectAtIndex:tag-1];
        [mainScrollView scrollRectToVisible:CGRectMake(button.frame.origin.x+50, button.frame.origin.y, button.frame.size.width, button.frame.size.height) animated:NO];
    }else{
        UIButton *button = (UIButton*)[arr2 objectAtIndex:tag];
        [mainScrollView scrollRectToVisible:button.frame animated:NO];
    }
    return;
}

@end


